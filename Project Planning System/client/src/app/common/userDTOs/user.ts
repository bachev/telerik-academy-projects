export class User {
  public id: string;
  public email: string;
  public firstName: string;
  public lastName: string;
  public position: string;
  public role: string;
  public dateCreated: string;
  public workingHoursLeft: number;
  public directManagerName: string;
}
