export class EmployeeCreateDTO {
    firstName: string;
    lastName: string;
    position: string;
    managerId: number;
    skillIds: number[];
  }
