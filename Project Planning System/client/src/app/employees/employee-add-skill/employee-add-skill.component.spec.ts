import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeAddSkillComponent } from './employee-add-skill.component';

describe('EmployeeAddSkillComponent', () => {
  let component: EmployeeAddSkillComponent;
  let fixture: ComponentFixture<EmployeeAddSkillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeAddSkillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeAddSkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
