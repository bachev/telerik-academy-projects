export class EmployeeDTO {
    public id: number;
    public firstName: string;
    public lastName: string;
    public position: string;
    public directManager: string;
    public workingHoursLeft: number;
}
