import { CreateUserComponent } from './../../users/create-user/create-user.component';
import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { EmployeesService } from 'src/app/core/services/employees.service';
import { EmployeeDTO } from '../models/employee.dto';
import { AuthService } from 'src/app/core/services/auth.service';
import { UsersService } from 'src/app/core/services/user.service';
import { User } from 'src/app/common/userDTOs/user';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatCheckbox } from '@angular/material/checkbox';
import { EmployeeAddSkillComponent } from '../employee-add-skill/employee-add-skill.component';

@Component({
  selector: 'app-list-employees',
  templateUrl: './list-employees.component.html',
  styleUrls: ['./list-employees.component.css']
})
export class ListEmployeesComponent implements OnInit {
  public displayedColumns: string[] = ['firstName', 'lastName', 'position', 'workingHoursLeft'/* , 'info' */];
  public employees: EmployeeDTO[];
  public users: User[];
  public loggedUserId: number;
  public loggedUser: User;
  public selectedEmployee: EmployeeDTO;
  public selectedUser: User;
  public filteredEmployees: EmployeeDTO[];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('checkBox') public checkBox: MatCheckbox;
  @ViewChild(MatSort, {static: true}) sort: EmployeeDTO[];

  constructor(
    private readonly employeesService: EmployeesService,
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
    public router: Router,
    public dialog: MatDialog,

  ) {}

  ngOnInit() {

    this.employeesService.getAllEmployees()
    .subscribe((data: EmployeeDTO[]) => {
      this.employees = data;
      this.filteredEmployees = data;
    });

    this.usersService.allUsers()
    .subscribe((data: User[]) =>
    this.users = data);

    this.authService.loggedUser$.subscribe((user) => {
      this.loggedUserId = +user.id;
      this.loggedUser = user;
    });
  }

  public openDialog() {
    this.dialog.open(CreateUserComponent);
  }

  public change(checkBox: MatCheckbox) {
    if (checkBox.checked) {
      this.usersService.getUserSubordinates(this.loggedUserId)
      .subscribe((data: EmployeeDTO[]) => this.filteredEmployees = data);
    } else if (!checkBox.checked){
      this.employeesService.getAllEmployees()
      .subscribe((data: EmployeeDTO[]) => this.filteredEmployees = data);
    }
  }

  public onEmployeeSelect(employee: EmployeeDTO): void {
    this.selectedEmployee = employee;
    this.router.navigate(['/employees', employee.id]);
  }

  public onUserSelect(user: User): void {
    this.selectedUser = user;
    this.router.navigate(['/users', user.id]);
  }

  public applyFilterFirstName(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if (!filterValue) {
      this.filteredEmployees = this.employees;
    } else {
      this.filteredEmployees = this.employees.filter(employee => employee.firstName.toLowerCase().includes(filterValue.toLowerCase()));
    }
  }

  public applyFilterLastName(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if (!filterValue) {
      this.filteredEmployees = this.employees;
    } else {
      this.filteredEmployees = this.employees.filter(employee => employee.lastName.toLowerCase().includes(filterValue.toLowerCase()));
    }
  }

  public applyFilterPosition(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if (!filterValue) {
      this.filteredEmployees = this.employees;
    } else {
      this.filteredEmployees = this.employees.filter(employee => employee.position.toLowerCase().includes(filterValue.toLowerCase()));
    }
  }

  public employeePage(employee: EmployeeDTO): void {
    this.selectedEmployee = employee;
    this.router.navigate(['/employees', employee.id]);
  }

  public userPage(user: User): void {
    this.selectedUser = user;
    this.router.navigate(['/users', user.id]);
  }

/*   public openAddSkillDialog() {
    this.dialog.open(EmployeeAddSkillComponent);
  } */
}

