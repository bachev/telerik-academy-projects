import { ProjectDTO } from 'src/app/projects/models/project.dto';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeesService } from 'src/app/core/services/employees.service';
import { Skill } from '../../skills/models/skill.dto';
import { EmployeeProjectDTO } from '../models/employee-project.dto';

@Component({
  selector: 'app-employee-list-items',
  templateUrl: './employee-list-items.component.html',
  styleUrls: ['./employee-list-items.component.css'],
})
export class EmployeeListItemsComponent implements OnInit {
  public employeeId: number;
  public projects: ProjectDTO[];
  public skills: Skill[];
  public selectedProject: EmployeeProjectDTO;
  displayedColumnsProject: string[] = ['projectId', 'projectName', 'skillName', 'dailyTimeInput'];
  displayedColumnsSkill: string[] = ['id', 'name'];
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly employeeService: EmployeesService,
  ) { }

  ngOnInit() {
    this.employeeId = +this.route.snapshot.paramMap.get('id');

    this.employeeService.getEmployeeProjects(this.employeeId).subscribe((data) => {
      this.projects = data;
    });

    this.employeeService.getEmployeeSkills(this.employeeId).subscribe((data) => {
      this.skills = data;
    });
  }
  public projectPage(project: EmployeeProjectDTO): void {
    this.selectedProject = project;
    this.router.navigate(['/projects', project.projectId]);
  }
}


