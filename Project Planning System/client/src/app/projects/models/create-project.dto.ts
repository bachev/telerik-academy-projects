import { StartProjectDTO } from './start-project.dto';
import { CreateProjectSkillDTO } from './create-project-skill.dto';
import { CreateProjectEmployeeDTO } from './create-project-employee.dto';

export class CreateProjectDTO {
    public data: StartProjectDTO;
    public skills: CreateProjectSkillDTO[];
    public employees: CreateProjectEmployeeDTO[];
}
