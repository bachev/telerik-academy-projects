export class ProjectDTO {
    public id: number;
    public name: string;
    public description: string;
    public reporter: string;
    public status: string;
    public targetDays: number;
    public remainingDays: number;
    public managementTotalHours: number;
    public managementHoursPassed: number;
    public managementDailyHours: number;
  }
