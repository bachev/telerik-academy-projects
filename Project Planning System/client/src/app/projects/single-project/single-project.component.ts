import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-single-project',
  templateUrl: './single-project.component.html',
  styleUrls: ['./single-project.component.css']
})
export class SingleProjectComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public getUrl(): string {
    return 'url(\'https://2015.igem.org/wiki/images/2/26/Amoy-MenuBar-bg.jpg\')';
  }

}
