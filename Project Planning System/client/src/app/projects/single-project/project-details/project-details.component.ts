import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { state, style, trigger } from '@angular/animations';
import { User } from '../../../common/userDTOs/user';
import { AuthService } from '../../../core/services/auth.service';
import { ProjectsService } from '../../../core/services/projects.service';
import { Subscription } from 'rxjs';
import { ProjectSkillDTO } from '../../models/project-skill.dto';
import { ProjectEmployeeDTO } from '../../models/project-employee.dto';
import { Skill } from '../../../skills/models/skill.dto';
import { CreateProjectSkillDTO } from '../../models/create-project-skill.dto';
import { NotificatorService } from '../../../core/services/notificator.service';
import { SkillsService } from '../../../core/services/skills.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DeleteProjectSkillDTO } from '../../models/delete-project-skill.dto';
import { UpdateProjectSkillDTO } from '../../models/update-project-skill.dto';
import { EmployeeDTO } from '../../../employees/models/employee.dto';
import { MatOptionSelectionChange } from '@angular/material/core';
import { CreateProjectEmployeeDTO } from '../../models/create-project-employee.dto';
import { UpdateProjectEmployeeDTO } from '../../models/update-project-employee.dto';
import { DeleteProjectEmployeeDTO } from '../../models/delete-project-employee.dto';
import { ProjectDTO } from '../../models/project.dto';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
    ]),
  ],
})
export class ProjectDetailsComponent implements OnInit, OnDestroy {
  private loggedUserSubscription: Subscription;
  public loggedUser: User;
  public loggedUserName: string;
  public projectId: number;
  public project: ProjectDTO;
  public skills: Skill[];
  public addSkillForm: FormGroup;
  public addEmployeeForm: FormGroup;
  public editSkillForm: FormGroup;
  public editEmployeeForm: FormGroup;
  public skillToUpdate: ProjectSkillDTO;
  public employeeToUpdate: ProjectEmployeeDTO;
  public projectSkills: ProjectSkillDTO[];
  public projectEmployees: ProjectEmployeeDTO[];
  public selectedSkill: Skill;
  public employeesForSkill: EmployeeDTO[];
  public addSkillFormShown = true;
  public addEmployeeFormShown = false;
  public skillInEditMode = false;
  public employeeInEditMode = false;
  public panelOpenState = false;
  public displayedColumnsSkill: string[] = ['skillName', 'totalHours', 'hoursWorked'];
  public displayedColumnsEmployee: string[] = ['employeeName', 'skillName', 'dailyTimeInput'];

  constructor(
    private readonly authService: AuthService,
    private readonly projectsService: ProjectsService,
    private readonly skillsService: SkillsService,
    private readonly notificator: NotificatorService,
    private readonly route: ActivatedRoute,
    private readonly formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUser$.subscribe((user) => {
      this.loggedUser = user;
      this.loggedUserName = `${user.firstName} ${user.lastName}`;
    });
    this.projectId = this.route.snapshot.params.id;

    this.projectsService.getProjectById(this.projectId).subscribe((foundProject: ProjectDTO) => this.project = foundProject);

    this.addSkillForm = this.formBuilder.group({
      skill: ['', [Validators.required]],
      skillTime: ['', [Validators.required, Validators.pattern('^[0-9]+$')]]
    });
    this.addEmployeeForm = this.formBuilder.group({
      selectedSkill: ['', [Validators.required]],
      employee: ['', [Validators.required]],
      dailyInput: ['', [Validators.required, Validators.min(1), Validators.max(8), Validators.pattern('^[1-8]+$')]]
    });
    this.projectsService.getProjectSkills(this.projectId).subscribe((data) => {
      this.projectSkills = data;
    });
    this.projectsService.getProjectEmployees(this.projectId).subscribe((data) => {
      this.projectEmployees = data;
    });
    this.skillsService.listAllSkills().subscribe((allSkills: Skill[]) => this.skills = allSkills);
  }

  ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  public toggleAddForms(): void {
    this.addSkillFormShown = !this.addSkillFormShown;
    this.addEmployeeFormShown = !this.addEmployeeFormShown;
  }

  public addSkillToProject(): void {
    const skill: Skill = this.addSkillForm.get('skill').value;
    const skillTime: number = this.addSkillForm.get('skillTime').value;

    if (skill && (skillTime || skillTime === 0)) {
      const newProjectSkill = new CreateProjectSkillDTO();
      newProjectSkill.skillId = skill.id;
      newProjectSkill.totalHours = skillTime;

      this.projectsService.addSkillToProject(this.projectId, newProjectSkill).subscribe(
        () => {
          this.ngOnInit();
          this.notificator.success(`${skill.name} - ${skillTime} hours added to project!`);
        },
        (error) => this.notificator.error(JSON.stringify(error.error.error))
      );
    }
  }

  public showEditSkillForm(skill: ProjectSkillDTO): void {
    this.skillInEditMode = !this.skillInEditMode;
    this.skillToUpdate = skill;
    this.editSkillForm = this.formBuilder.group({
      updatedSkillTime: [skill.totalHours, [Validators.required, Validators.min(1), Validators.pattern('^[0-9]+$')]]
    });
  }

  public updateProjectSkillTime(): void {
    const projectSkillId: number = this.skillToUpdate.id;
    const skillTime: number = this.editSkillForm.get('updatedSkillTime').value;

    if (skillTime) {
    const projectSkill = new UpdateProjectSkillDTO();
    projectSkill.projectSkillId = projectSkillId;
    projectSkill.totalHours = skillTime;

    this.projectsService.updateProjectSkillTime(this.projectId, projectSkill).subscribe(
      () => {
        this.ngOnInit();
        this.skillInEditMode = !this.skillInEditMode;
        this.notificator.success(`Required hours for ${this.skillToUpdate.skillName} updated!`);
      },
      (error) => this.notificator.error(JSON.stringify(error.error.error))
      );
    }
  }

  public removeSkillFromProject(skill: ProjectSkillDTO): void {
    const projectSkill = new DeleteProjectSkillDTO();
    projectSkill.projectSkillId = skill.id;

    this.projectsService.removeSkillFromProject(this.projectId, projectSkill).subscribe(
      () => {
        this.ngOnInit();
        this.notificator.success(`${skill.skillName} activity removed from project!`);
      }
    );
  }

  public getEmployeesForSelectedSkill(event: MatOptionSelectionChange, projectSkill: ProjectSkillDTO): void {
    if (event.isUserInput) {
      this.employeesForSkill = [];
      this.selectedSkill = this.skills.find(skill => skill.name === projectSkill.skillName);
      this.skillsService.getAllSkillEmployees(this.selectedSkill.id).subscribe((availableEmployees: EmployeeDTO[]) => {
        availableEmployees.forEach(employee => {
          if (employee.workingHoursLeft > 0) {
            this.employeesForSkill.push(employee);
          }
        });
      });
    }
  }

  public addEmployeeToProject(): void {
    const employee: EmployeeDTO = this.addEmployeeForm.get('employee').value;
    const dailyInput: number = this.addEmployeeForm.get('dailyInput').value;

    if (employee && dailyInput) {
      const projectEmployee = new CreateProjectEmployeeDTO();
      projectEmployee.skillId = this.selectedSkill.id;
      projectEmployee.employeeId = employee.id;
      projectEmployee.dailyTimeInput = dailyInput;

      this.projectsService.addEmployeeToProject(this.projectId, projectEmployee).subscribe(
        () => {
          this.ngOnInit();
          this.notificator.success(`${employee.firstName} ${employee.lastName} added to project with ${dailyInput}
          hours ${this.selectedSkill.name} daily!`);
        },
        (error) => this.notificator.error(JSON.stringify(error.error.error))
      );
    }
  }

  public showEditEmployeeForm(employee: ProjectEmployeeDTO): void {
    this.employeeInEditMode = !this.employeeInEditMode;
    this.employeeToUpdate = employee;
    this.editEmployeeForm = this.formBuilder.group({
      updatedDailyInput: [employee.dailyTimeInput, [Validators.required, Validators.pattern('^[1-8]')]]
    });
  }

  public updateProjectEmployeeTime(): void {
    const projectEmployeeId: number = this.employeeToUpdate.id;
    const dailyInput: number = this.editEmployeeForm.get('updatedDailyInput').value;

    if (dailyInput) {
      const projectEmployee = new UpdateProjectEmployeeDTO();
      projectEmployee.projectEmployeeId = projectEmployeeId;
      projectEmployee.dailyTimeInput = dailyInput;

      this.projectsService.updateProjectEmployeeTime(this.projectId, projectEmployee).subscribe(
        () => {
          this.ngOnInit();
          this.employeeInEditMode = !this.employeeInEditMode;
          this.notificator.success(`${this.employeeToUpdate.employeeName}'s daily input updated!`);
        },
        (error) => this.notificator.error(JSON.stringify(error.error.error))
      );
    }
  }

  public removeEmployeeFromProject(employee: ProjectEmployeeDTO): void {
    const projectEmployee = new DeleteProjectEmployeeDTO();
    projectEmployee.projectEmployeeId = employee.id;

    this.projectsService.removeEmployeeFromProject(this.projectId, projectEmployee).subscribe(
      () => {
        this.ngOnInit();
        this.notificator.success(`${employee.employeeName} removed from project!`);
      }
    );
  }
}
