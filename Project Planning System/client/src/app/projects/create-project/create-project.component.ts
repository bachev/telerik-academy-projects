import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Skill } from '../../skills/models/skill.dto';
import { SkillsService } from '../../core/services/skills.service';
import { StartProjectDTO } from '../models/start-project.dto';
import { CreateProjectSkillDTO } from '../models/create-project-skill.dto';
import { NotificatorService } from '../../core/services/notificator.service';
import { EmployeeDTO } from '../../employees/models/employee.dto';
import { EmployeesService } from '../../core/services/employees.service';
import { MatOptionSelectionChange } from '@angular/material/core';
import { CreateProjectEmployeeDTO } from '../models/create-project-employee.dto';
import { ProjectEmployeeDisplayDTO } from '../models/project-employee-display.dto';
import { ProjectSkillDisplayDTO } from '../models/project-skill-display.dto';
import { CreateProjectDTO } from '../models/create-project.dto';
import { ProjectsService } from '../../core/services/projects.service';
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { User } from '../../common/userDTOs/user';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css']
})
export class CreateProjectComponent implements OnInit, OnDestroy {
  public firstFormGroup: FormGroup;
  public secondFormGroup: FormGroup;
  public thirdFormGroup: FormGroup;
  private loggedUserSubscription: Subscription;
  public loggedUser: User;
  public skills: Skill[];
  public initialProjectData = new StartProjectDTO();
  public projectSkills: CreateProjectSkillDTO[] = [];
  public projectSkillsToDisplay: ProjectSkillDisplayDTO[] = [];
  public isThirdStepOptional = false;
  public selectedSkills: Skill[] = [];
  public availableEmployees: EmployeeDTO[] = [];
  public filteredEmployees: EmployeeDTO[];
  public projectEmployees: CreateProjectEmployeeDTO[] = [];
  public projectEmployeesToDisplay: ProjectEmployeeDisplayDTO[] = [];
  public projectCompletionDays: number;

  public constructor(
    private readonly formBuilder: FormBuilder,
    private readonly skillsService: SkillsService,
    private readonly employeesService: EmployeesService,
    private readonly projectsService: ProjectsService,
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
  ) { }

  ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUser$.subscribe((user: User) => this.loggedUser = user);

    this.firstFormGroup = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(120)]],
      targetDays: ['', [Validators.required, Validators.min(1), Validators.pattern('^[0-9]+$')]],
      managementTotalHours: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      managementDailyHours: ['', [Validators.required, Validators.pattern('^[0-8]+$')]]
    });
    this.secondFormGroup = this.formBuilder.group({
      skill: ['', [Validators.required]],
      skillTotalHours: ['', [Validators.required, Validators.pattern('^[0-9]+$')]]
    });
    this.thirdFormGroup = this.formBuilder.group({
      selectedSkill: ['', [Validators.required]],
      employee: ['', [Validators.required]],
      employeeDailyHours: ['', [Validators.required, Validators.min(1), Validators.max(8), Validators.pattern('^[1-8]+$')]]
    });
  }

  ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  public startProject(): void {
    const name: string = this.firstFormGroup.get('name').value;
    const description: string = this.firstFormGroup.get('description').value;
    const targetDays: number = this.firstFormGroup.get('targetDays').value;
    const managementTotalHours: number = this.firstFormGroup.get('managementTotalHours').value;
    const managementDailyHours: number = this.firstFormGroup.get('managementDailyHours').value;

    this.initialProjectData.name = name;
    this.initialProjectData.description = description;
    this.initialProjectData.targetDays = targetDays;
    this.initialProjectData.managementTotalHours = managementTotalHours;
    this.initialProjectData.managementDailyHours = managementDailyHours;

    this.calculateProjectCompletionDays();

    this.skillsService.listAllSkills().subscribe((allSkills: Skill[]) => this.skills = allSkills);
  }

  public addSkillsToProject(): void {
    const skill: Skill = this.secondFormGroup.get('skill').value;
    const skillHours: number = this.secondFormGroup.get('skillTotalHours').value;

    if (skill && (skillHours || skillHours === 0)) {
      const projectSkill = new CreateProjectSkillDTO();
      projectSkill.skillId = skill.id;
      projectSkill.totalHours = skillHours;

      this.projectSkills.push(projectSkill);
      this.selectedSkills.push(skill);

      this.skills = this.skills.filter(singleSkill => singleSkill.id !== skill.id);

      const projectSkillToDisplay = new ProjectSkillDisplayDTO();
      projectSkillToDisplay.skillName = skill.name;
      projectSkillToDisplay.totalHours = skillHours;

      this.projectSkillsToDisplay.push(projectSkillToDisplay);

      this.notificator.success(`${skill.name} - ${skillHours} hours added to project!`);

      this.calculateProjectCompletionDays();

      this.skillsService.getAllSkillEmployees(projectSkill.skillId).subscribe(
        (employees: EmployeeDTO[]) => employees.forEach(employee => {
          if (employee.workingHoursLeft > 0 &&
            this.availableEmployees.every(availableEmployee => availableEmployee.id !== employee.id)) {
            this.availableEmployees.push(employee);
          }
        })
      );
    }
  }

  public makeThirdStepOptional(): void {
    if (this.projectSkillsToDisplay.every(projectSkill => projectSkill.totalHours === 0)) {
      this.isThirdStepOptional = true;
    } else {
      this.isThirdStepOptional = false;
    }
  }

  public filterEmployeesBySkill(event: MatOptionSelectionChange, skill: Skill): void {
    if (event.isUserInput) {
      this.filteredEmployees = [];
      this.availableEmployees.forEach(employee => {
        this.employeesService.getEmployeeSkills(employee.id).subscribe((employeeSkills: Skill[]) => {
          if (employeeSkills.some(employeeSkill => employeeSkill.id === skill.id)) {
            this.filteredEmployees.push(employee);
          }
        });
      });
    }
  }

  public addEmployeeToProject(): void {
    const skill: Skill = this.thirdFormGroup.get('selectedSkill').value;
    const employee: EmployeeDTO = this.thirdFormGroup.get('employee').value;
    const dailyInput: number = this.thirdFormGroup.get('employeeDailyHours').value;

    if (skill && employee && dailyInput && !isNaN(dailyInput)) {
      const projectEmployee = new CreateProjectEmployeeDTO();
      projectEmployee.skillId = skill.id;
      projectEmployee.employeeId = employee.id;
      projectEmployee.dailyTimeInput = dailyInput;

      this.projectEmployees.push(projectEmployee);

      const projectEmployeeToDisplay = new ProjectEmployeeDisplayDTO();
      projectEmployeeToDisplay.skillName = skill.name;
      projectEmployeeToDisplay.employeeName = `${employee.firstName} ${employee.lastName}`;
      projectEmployeeToDisplay.dailyTimeInput = dailyInput;

      this.projectEmployeesToDisplay.push(projectEmployeeToDisplay);

      this.notificator.success(`${employee.firstName} ${employee.lastName} added to project with ${dailyInput} hours ${skill.name} daily!`);

      this.calculateProjectCompletionDays();
    }
  }

  public createProject(): void {
    const newProject = new CreateProjectDTO();
    newProject.data = this.initialProjectData;
    newProject.skills = this.projectSkills;
    newProject.employees = this.projectEmployees;

    this.projectsService.createProject(newProject).subscribe(
      () => {
        this.authService.getNewToken(+this.loggedUser.id);
        this.router.navigate(['projects']);
        this.notificator.success(`Project: ${this.initialProjectData.name} created successfully!`);
      },
      (error) => {
        this.authService.getNewToken(+this.loggedUser.id);
        this.router.navigate(['projects']);
        this.notificator.error(JSON.stringify(error.error.error));
      }
    );
  }

  private calculateProjectCompletionDays(): void {
    this.projectCompletionDays = 0;

    if (this.initialProjectData.managementTotalHours > 0 && this.initialProjectData.managementDailyHours > 0) {
      this.projectCompletionDays = Math.ceil(this.initialProjectData.managementTotalHours / this.initialProjectData.managementDailyHours);
    } else if (this.initialProjectData.managementTotalHours > 0 && this.initialProjectData.managementDailyHours === 0) {
      this.projectCompletionDays = null;

      return;
    }
    const completionDaysForSkills: number[] = [];

    const activeProjectSkills: CreateProjectSkillDTO[] = this.projectSkills.filter(projectSkill => projectSkill.totalHours > 0);

    activeProjectSkills.forEach(projectSkill => {
      const dailyHoursForSkill: number = this.projectEmployees.reduce((acc, projectEmployee) => {
        return projectEmployee.skillId === projectSkill.skillId ? acc + projectEmployee.dailyTimeInput : acc;
      }, 0);

      const totalDaysForSkill: number = dailyHoursForSkill > 0 ? Math.ceil(projectSkill.totalHours / dailyHoursForSkill) : null;

      completionDaysForSkills.push(totalDaysForSkill);
    });

    if (completionDaysForSkills.includes(null)) {
      this.projectCompletionDays = null;

      return;
    }
    completionDaysForSkills.sort((a, b) => b - a);

    if (completionDaysForSkills[0] > this.projectCompletionDays) {
      this.projectCompletionDays = completionDaysForSkills[0];
    }
  }
}

