import { AgreeModalComponent } from './components/agree-modal/agree-modal.component';
import { SliderComponent } from './components/slider/slider.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FlexLayoutModule } from '@angular/flex-layout';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    SliderComponent,
    AgreeModalComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    NgxSpinnerModule,
    RouterModule,
    FlexLayoutModule
  ],
  exports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    CommonModule,
    MaterialModule,
    HeaderComponent,
    FooterComponent,
    SliderComponent,
    NgxSpinnerModule,
    FlexLayoutModule,
  ],
})
export class SharedModule { }
