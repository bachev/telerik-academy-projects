import { Router } from '@angular/router';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { User } from 'src/app/common/userDTOs/user';
import { AuthService } from 'src/app/core/services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  private loggedInSubscription: Subscription;
  public loggedUser: User;
  @Input() loggedIn: boolean;
  @Input() public user: User;
  @Output() toggleMenu = new EventEmitter<void>();
  @Output() logout = new EventEmitter<void>();


  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificator: NotificatorService,
  ) { }

  public ngOnInit() {
   this.authService.loggedUser$.subscribe((user) => {
     this.loggedUser = user;
   });
  }
  public triggerLogout() {
    this.logout.emit();
    this.notificator.success('Logout Successful!');
  }

  public gotoHome() {
    this.router.navigate(['home']);
  }


}
