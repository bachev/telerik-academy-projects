import { CreateUserComponent } from './create-user/create-user.component';
import { EditUserProfileComponent } from './edit-user-modal/edit-user-modal.component';
import { NgModule } from '@angular/core';
import { UserListItemsComponent } from './user-list-items/user-list-items.component';
import { SingleUserViewComponent } from './single-user-view/single-user-view.component';
import { UsersComponent } from './users/users.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { UsersService } from '../core/services/user.service';

@NgModule({
  declarations: [
    UserListItemsComponent,
    SingleUserViewComponent,
    UsersComponent,
    EditUserProfileComponent,
    CreateUserComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    FormsModule,
  ],
  providers: [ UsersService ],
  exports: [
    UserListItemsComponent,
    SingleUserViewComponent,
    RouterModule,
  ]
})
export class UsersModule { }
