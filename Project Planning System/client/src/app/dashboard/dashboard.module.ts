import { DashboardItemsComponent } from './dashboard-user-items/dashboard-user-items.component';
import { DashboardUserInfoComponent } from './dashboard-user-info/dashboard-user-info.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { UsersModule } from '../users/users.module';
import { UsersService } from '../core/services/user.service';



@NgModule({
  declarations: [
    DashboardComponent,
    DashboardUserInfoComponent,
    DashboardItemsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    UsersModule,
    RouterModule
  ],
  providers: [
    UsersService
  ],
  exports: [
    RouterModule,
  ],

})
export class DashboardModule { }
