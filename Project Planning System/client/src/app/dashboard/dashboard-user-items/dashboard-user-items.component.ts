import { EmployeeDTO } from 'src/app/employees/models/employee.dto';
import { ProjectDTO } from 'src/app/projects/models/project.dto';
import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../core/services/user.service';
import { User } from '../../common/userDTOs/user';
import { AuthService } from '../../core/services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dashboard-user-items',
  templateUrl: './dashboard-user-items.component.html',
  styleUrls: ['./dashboard-user-items.component.css'],
})
export class DashboardItemsComponent implements OnInit {
  public userId: number;
  public user: User;
  public projects: ProjectDTO[];
  public subordinates: EmployeeDTO[];
  public selectedProject: ProjectDTO;
  public selectedEmployee: EmployeeDTO;
  public displayedColumnsProject: string[] = ['id', 'name', 'targetDays', 'remainingDays'];
  public displayedColumnsEmployee: string[] = ['firstName', 'lastName', 'position'];

  constructor(

    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly usersService: UsersService,
  ) { }

  ngOnInit() {
    this.authService.loggedUser$.subscribe((user) => {
        this.userId = +user.id;
    });

    this.usersService.singleUser(this.userId).subscribe((data) => {
        this.user = data;
    });

    this.usersService.getInProgressProjects(this.userId).subscribe((data) => {
        this.projects = data;
    });

    this.usersService.getUserSubordinates(this.userId).subscribe((data) => {
        this.subordinates = data;
    });
  }


  public projectPage(project: ProjectDTO): void {
    this.selectedProject = project;
    this.router.navigate(['/projects', project.id]);
  }

  public employeePage(employee: EmployeeDTO): void {
    this.selectedEmployee = employee;
    this.router.navigate(['/employees', employee.id]);
  }
}


