import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../core/services/auth.service';
import { NotificatorService } from '../core/services/notificator.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginModel = { email: '', password: '' };
  public registerForm: FormGroup;


  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly fb: FormBuilder,
  ) {
    this.registerForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
    });

  }

  public ngOnInit() {
  }

  public login(): void {
    this.authService.login(this.loginModel)
      .subscribe(
        () => {
          this.notificator.success(`Login successful!`);
          this.router.navigate(['/dashboard']);
        },
        () => this.notificator.error(`Invalid email/password!`),
      );
  }
}
