import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from '../../config/config';
import { EmployeeDTO } from '../../employees/models/employee.dto';
import { Observable } from 'rxjs';
import { Skill } from '../../skills/models/skill.dto';
import { ProjectDTO } from 'src/app/projects/models/project.dto';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  constructor(
    private readonly http: HttpClient,
    ) { }

    public getAllEmployees(): Observable<EmployeeDTO[]> {
        return this.http.get<EmployeeDTO[]>(`${CONFIG.SERVER_ADDR}/employees`);
    }

    public getEmployeeById(id: number): Observable<EmployeeDTO> {
      return this.http.get<EmployeeDTO>(`${CONFIG.SERVER_ADDR}/employees/${id}`);
    }

    public getEmployeeProjects(id: number): Observable<ProjectDTO[]> {
      return this.http.get<ProjectDTO[]>(`${CONFIG.SERVER_ADDR}/employees/${id}/projects`);
    }

    public getEmployeeSkills(id: number): Observable<Skill[]> {
      return this.http.get<Skill[]>(`${CONFIG.SERVER_ADDR}/employees/${id}/skills`);
    }
}
