import { AddNewSkillDTO } from './../../common/userDTOs/employee-add-skill-dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from 'src/app/config/config';


@Injectable({
    providedIn: 'root'
})
export class AdminService {
    constructor(
        private readonly httpClient: HttpClient,
    ) { }



    public createEmployee(body: any): Observable<any> {
        return this.httpClient.post<any>(`${CONFIG.SERVER_ADDR}/admin/employees`, body);
    }

    public createManager(body: any): Observable<any> {
        return this.httpClient.post<any>(`${CONFIG.SERVER_ADDR}/admin/users`, body);
    }

    public deleteEmployee(id: number): Observable<any> {
        return this.httpClient.delete<any>(`${CONFIG.SERVER_ADDR}/admin/employees/${id}`);
    }

    public deleteUser(userId: number): Observable<any> {
        return this.httpClient.delete<any>(`${CONFIG.SERVER_ADDR}/admin/users/${userId}`);
    }

    public addSkill(id: number, body: AddNewSkillDTO): Observable<any> {
        return this.httpClient.post<any>(`${CONFIG.SERVER_ADDR}/admin/employees/${id}`, body);
    }

}

