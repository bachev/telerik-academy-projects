import { UserUpdateDTO } from './../../common/userDTOs/user-update-dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/common/userDTOs/user';

@Injectable({
    providedIn: 'root'
})
export class UsersService {
    constructor(
        private readonly httpClient: HttpClient,
    ) { }

    public allUsers(): Observable<User[]> {
        return this.httpClient.get<User[]>('http://localhost:3000/users');
    }

    public singleUser(userId: number): Observable<User> {
        return this.httpClient.get<User>(`http://localhost:3000/users/${userId}`);
    }

    public getToken(userId: number): Observable<{ token: string }> {
        return this.httpClient.get<{ token: string }>(`http://localhost:3000/users/${userId}/token`);
    }

    public getUserSubordinates(userId: number): Observable<any> {
        return this.httpClient.get<any>(`http://localhost:3000/users/${userId}/employees`);
    }

    public updateUser(userId: number, body: UserUpdateDTO): Observable<User> {
        return this.httpClient.put<User>(`http://localhost:3000/users/${userId}`, body);
    }

    public getProjects(userId: number): Observable<any> {
        return this.httpClient.get<any>(`http://localhost:3000/users/${userId}/projects`);
    }

    public getInProgressProjects(userId: number): Observable<any> {
        return this.httpClient.get<any>(`http://localhost:3000/users/${userId}/projects/inProgress`);
    }
}

