import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { StorageService } from './services/storage.service';
import { AuthService } from './services/auth.service';
import { NotificatorService } from './services/notificator.service';
import { AuthGuard } from '../common/guards/auth.guard';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
      countDuplicates: true,
    }),
  ],
  providers: [
    AuthService,
    StorageService,
    NotificatorService,
    AuthGuard,

  ],
  exports: [
    HttpClientModule,
  ],
})
export class CoreModule { }
