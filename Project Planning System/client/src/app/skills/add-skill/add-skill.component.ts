import { Component, OnInit } from '@angular/core';
import { SkillsService } from 'src/app/core/services/skills.service';
import { ActivatedRoute } from '@angular/router';
import { SkillCreateDTO } from '../models/skill-create.dto';

@Component({
  selector: 'app-add-skill',
  templateUrl: './add-skill.component.html',
  styleUrls: ['./add-skill.component.css']
})
export class AddSkillComponent implements OnInit {

  public skillName: string;
  public skillCreateBody: SkillCreateDTO;
  constructor(
    private readonly service: SkillsService,
  ) { }

  ngOnInit(): void {
  }

  public createSkill(): void {
    this.skillCreateBody = {
      name: this.skillName
    };

    this.service.createSkill(this.skillCreateBody)
      .subscribe({
        next: data => window.location.reload(),
        error: e => console.log(e),
      });
  }

}

