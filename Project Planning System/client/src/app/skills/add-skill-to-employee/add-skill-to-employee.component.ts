import { EmployeeDTO } from '../../employees/models/employee.dto';
import { AddNewSkillDTO } from '../../common/userDTOs/employee-add-skill-dto';
import { EmployeesService } from 'src/app/core/services/employees.service';
import {
  Component,
  OnInit,
} from '@angular/core';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { AdminService } from 'src/app/core/services/admin.service';
import { Skill } from 'src/app/skills/models/skill.dto';
import { SkillsService } from 'src/app/core/services/skills.service';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-add-skill-to-employee',
  templateUrl: './add-skill-to-employee.component.html',
  styleUrls: ['./add-skill-to-employee.component.css']
})
export class AddSkillToEmployeeComponent implements OnInit {

  public skills: Skill[];
  public skillId: number;
  public employeeId: number;
  public employees: EmployeeDTO[];
  public skillControl = new FormControl(this.skillId);
  public employeeControl = new FormControl(this.employeeId);

  constructor(
    private readonly notificator: NotificatorService,
    private readonly adminService: AdminService,
    private readonly skillsService: SkillsService,
    private readonly employeeService: EmployeesService,

  ) { }
  ngOnInit(): void {
    this.employeeService.getAllEmployees().subscribe((data) => {
      this.employees = data;
    });

    this.skillsService.listAllSkills().subscribe((data) => {
      this.skills = data;
    });
  }
  public addSkill(employee: number): void {
    const addSkill: AddNewSkillDTO = {
      skillId: this.skillId
    };
    this.adminService.addSkill(employee, addSkill).subscribe(() => {
      this.notificator.success('Skill added!');
    });
  }
}
