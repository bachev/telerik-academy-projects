import { AddSkillToEmployeeComponent } from './add-skill-to-employee/add-skill-to-employee.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuard } from '../common/guards/role.guard';
import { ListSkillsComponent } from './list-skills/list-skills.component';
import { AddSkillComponent } from './add-skill/add-skill.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes  = [
/*   {
    path: 'skills',
    component: ListSkillsComponent,
    canActivate: [RoleGuard],
    data: {role: 'Admin'}
  },
  {
    path: 'skills/create',
    component: AddSkillComponent,
    canActivate: [RoleGuard],
    data: {role: 'Admin'}
  }, */
];

@NgModule({
  declarations: [
    ListSkillsComponent,
    AddSkillComponent,
    AddSkillToEmployeeComponent],
  imports: [
    CommonModule,
    SharedModule,
    // RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ]
})
export class SkillsModule { }
