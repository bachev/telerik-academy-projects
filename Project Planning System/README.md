﻿## How to setup and run the project:

**1. Create a local copy of the project on your computer**

Open *Git Bash* shell in a folder of your choice and run the following command:
```git clone https://gitlab.com/bachev/telerik-academy-projects.git```

**2. Setup the database**
-   Install MySQL and MySQL Workbench
-   Using MySQL Workbench create a new schema and name it ***ppsdb***

**3. Create the following *.env* and *ormconfig.json* files in the *api* folder (where _package.json_ and the rest of the config files are):**
*.env*:

    PORT=3000
    DB_TYPE=mysql
    DB_HOST=localhost
    DB_PORT=3306
    DB_USERNAME=root
    DB_PASSWORD=1234
    DB_DATABASE_NAME=ppsdb
    JWT_SECRET = ppsystem
    JWT_EXPIRE_TIME = 3600


*ormconfig.json*:

     {
     "type": "mysql",
     "host": "localhost",
     "port": 3306,
     "username": "root",
     "password": "1234",
     "database": "ppsdb",
     "entities": [
     "src/database/entities/**/*.ts"
     ],
     "migrations": [
     "src/database/migration/**/*.ts"
     ],
     "cli": {
     "entitiesDir": "src/database/entities",
     "migrationsDir": "src/database/migration"
     }
    }

**4.  Install all dependencies**

Open the project with *Visual Studio Code* and run ```npm install``` both in *api* and *client* folders (where *package.json* and the rest of the config files are)

**5. Run the application**
-   Server side - navigate to the *api* folder and run:
     -   `npm run seed` - to create one admin and one basic user
    -   `npm run start:dev` - to start the server in watch mode
-   Client side - navigate to the *client* folder and run:
    -   `ng s -o` - to launch the application and open it in the browser
    -   `npm run test` - to run the unit tests

**6. Login**

as administrator: 
- email - `admin@admin.com` 
- password - `Admin`

as basic user:
- email - `manager@manager.com` 
- password - `Manager`

