import { createParamDecorator, ExecutionContext } from '@nestjs/common';

// tslint:disable-next-line: variable-name
export const Token = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();

    return request.headers.authorization;
  },
);
