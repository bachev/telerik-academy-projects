import { UsersModule } from './users/users.module';
import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { ProjectsModule } from './projects/projects.module';
import { EmployeesModule } from './employees/employees.module';
import { SkillsModule } from './skills/skills.module';
import { AuthModule } from './auth/auth.module';
import { CoreModule } from './common/core.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    DatabaseModule,
    ProjectsModule,
    UsersModule,
    EmployeesModule,
    SkillsModule,
    AuthModule,
    CoreModule,
    ScheduleModule.forRoot(),
  ],
})
export class AppModule { }
