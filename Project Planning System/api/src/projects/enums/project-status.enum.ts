export enum ProjectStatus {
    ONGOING = 'In Progress',
    CLOSED = 'Ended',
}
