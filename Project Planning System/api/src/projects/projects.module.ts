import { Module } from '@nestjs/common';
import { ProjectsController } from './projects.controller';
import { ProjectsService } from './projects.service';
import { TypeOrmModule } from '@nestjs/typeorm/dist/typeorm.module';
import { Project } from '../database/entities/project.entity';
import { User } from '../database/entities/user.entity';
import { ProjectSkill } from '../database/entities/projectSkill.entity';
import { Skill } from '../database/entities/skill.entity';
import { ProjectSkillsService } from './project-skills.service';
import { ProjectEmployeesService } from './project-employees.service';
import { ProjectEmployee } from '../database/entities/projectEmployee.entity';
import { Employee } from '../database/entities/employee.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Project, ProjectSkill, User, Skill, ProjectEmployee, Employee]),
  ],
  controllers: [ProjectsController],
  providers: [ProjectsService, ProjectSkillsService, ProjectEmployeesService],
})
export class ProjectsModule {}
