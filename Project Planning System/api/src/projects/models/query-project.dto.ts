import { IsOptional } from 'class-validator';

export class QueryProjectDTO {
  @IsOptional()
    public status?: string;

  @IsOptional()
    public name?: string;

  @IsOptional()
    public description?: string;
}
