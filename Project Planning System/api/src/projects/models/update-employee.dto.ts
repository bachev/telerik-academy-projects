import { Min } from 'class-validator';

export class UpdateProjectEmployeeTimeDTO {
  @Min(1)
    public projectEmployeeId: number;

  @Min(1)
    public dailyTimeInput: number;
}
