import { Length, IsOptional, Min } from 'class-validator';

export class ProjectUpdateDTO {

  @IsOptional()
  @Length(2, 50)
  public name?: string;

  @IsOptional()
  @Length(10, 120)
  public description?: string;

  @IsOptional()
  @Min(1)
  public managementTotalHours?: number;
}
