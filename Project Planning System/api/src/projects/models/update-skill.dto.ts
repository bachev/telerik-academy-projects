import { IsPositive, Min } from 'class-validator';

export class UpdateProjectSkillTimeDTO {
  @Min(1)
    public projectSkillId: number;

  @IsPositive()
    public totalHours: number;
}
