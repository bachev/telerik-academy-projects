import { Length, Min } from 'class-validator';

export class ProjectCreateDTO {
  @Length(2, 50)
  public name: string;

  @Length(10, 120)
  public description: string;

  @Min(1)
  public targetDays: number;

  @Min(0)
  public managementTotalHours: number;

  @Min(0)
  public managementDailyHours: number;
}
