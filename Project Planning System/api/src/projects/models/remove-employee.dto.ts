import { Min } from 'class-validator';

export class RemoveProjectEmployeeDTO {
  @Min(1)
    public projectEmployeeId: number;
}
