import { Expose } from 'class-transformer';

export class ReturnProjectSkillDTO {
  @Expose()
    public id: number;

  @Expose()
    public skillName: string;

  @Expose()
    public totalHours: number;

  @Expose()
    public hoursWorked:number;
}
