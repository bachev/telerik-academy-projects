import { Min } from 'class-validator';

export class AddSkillToProjectDTO {
  @Min(1)
  public skillId: number;

  @Min(0)
  public totalHours: number;
}
