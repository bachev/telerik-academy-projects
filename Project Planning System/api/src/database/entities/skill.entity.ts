/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, OneToMany } from 'typeorm';
import { Employee } from './employee.entity';
import { ProjectSkill } from './projectSkill.entity';
import { ProjectEmployee } from './projectEmployee.entity';

@Entity('skills')
export class Skill {
  @PrimaryGeneratedColumn('increment')
    public id: number;

  @Column({ type: 'nvarchar', unique: true })
    public name: string;

  @Column({ type: 'boolean', default: false })
    public isDeleted: boolean;

  @ManyToMany(type => Employee, employee => employee.skills)
    public employees: Promise<Employee[]>;

  @OneToMany(type => ProjectSkill, projectSkill => projectSkill.skill)
    public projectSkills: Promise<ProjectSkill[]>;

  @OneToMany(type => ProjectEmployee, projectEmployee => projectEmployee.employee)
    public projectEmployees: Promise<ProjectEmployee[]>;
}
