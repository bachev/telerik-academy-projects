/* eslint-disable @typescript-eslint/no-unused-vars */
import { PrimaryGeneratedColumn, Column, ManyToOne, Entity } from 'typeorm';
import { Skill } from './skill.entity';
import { Project } from './project.entity';

@Entity('projects_skills')
export class ProjectSkill {
  @PrimaryGeneratedColumn('increment')
    public id: number;

  @Column('nvarchar')
    public skillName: string;

  @Column('int')
    public totalHours: number;

  @Column({ type: 'int', default: 0 })
    public hoursWorked:number;

  @Column({ type: 'boolean', default: false })
    public isDeleted: boolean;

  @ManyToOne(type => Skill, skill => skill.projectSkills)
    public skill: Promise<Skill>;

  @ManyToOne(type => Project, project => project.projectSkills)
    public project: Promise<Project>;
}
