/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Employee } from './employee.entity';
import { Project } from './project.entity';
import { Skill } from './skill.entity';

@Entity('projects_employees')
export class ProjectEmployee {
  @PrimaryGeneratedColumn('increment')
public id: number;

  @Column('nvarchar')
public employeeName: string;

  @Column('nvarchar')
public skillName: string;

  @Column('nvarchar')
public projectName: string;

  @Column('int')
public projectId: number;

  @Column('int')
public dailyTimeInput: number;

  @Column({ type: 'boolean', default: false })
public isDeleted: boolean;

  @ManyToOne(type => Employee, employee => employee.projectEmployees)
public employee: Promise<Employee>;

  @ManyToOne(type => Skill, skill => skill.projectEmployees)
public skill: Promise<Skill>;

  @ManyToOne(type => Project, project => project.projectEmployees)
public project: Promise<Project>;
}
