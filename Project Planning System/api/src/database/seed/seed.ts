import { createConnection, Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { UserRole } from '../../users/enum/user-roles.enum';
import * as bcrypt from 'bcrypt';

const seedAdmin = async (connection: any) => {
  const userRepository: Repository<User> = connection.manager.getRepository(User);

  const admin = await userRepository.findOne({
    where: {
      email: 'admin@admin.com',
    },
  });

  if (admin) {
    console.log('The DB already has an admin!');
    return;
  }

  const newAdmin: User = userRepository.create({
    firstName: 'Admin',
    lastName: 'Admin',
    email: 'admin@admin.com',
    password: await bcrypt.hash('Admin', 10),
    position: 'Admin',
    role: UserRole.Admin,
    directManagerName: 'self-managed',
  });

  await userRepository.save(newAdmin);
  console.log('Seeded admin successfully!');
};

const seedManager = async (connection: any) => {
  const userRepository: Repository<User> = connection.manager.getRepository(User);

  const manager = await userRepository.findOne({
    where: {
      email: 'manager@manager.com',
    },
  });

  if (manager) {
    console.log('The DB already has a manager!');
    return;
  }

  const newManager: User = userRepository.create({
    firstName: 'Manager',
    lastName: 'Manager',
    email: 'manager@manager.com',
    password: await bcrypt.hash('Manager', 10),
    position: 'Manager',
    role: UserRole.Basic,
    directManagerName: 'self-managed',
  });

  await userRepository.save(newManager);
  console.log('Seeded manager successfully!');
};

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  await seedAdmin(connection);
  await seedManager(connection);

  await connection.close();
  console.log('Seed completed!');
};

seed().catch(console.error);
