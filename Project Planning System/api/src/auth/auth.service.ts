import { ProjectPlanningSystemError } from '../common/exceptions/pps.error';
import { UserLoginDTO } from '../users/models/user-login.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { User } from '../database/entities/user.entity';
import { ReturnUserDTO } from '../users/models/user-return.dto';
import { plainToClass } from 'class-transformer';

@Injectable()
export class AuthService {
  private readonly tokensBlacklist: string[] = [];

  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly jwtService: JwtService,
  ) {}

  public async login(loginUser: UserLoginDTO): Promise<{ token: string }> {
    const user: ReturnUserDTO = await this.validateUser(loginUser);

    if (user === null) {
      throw new ProjectPlanningSystemError('Wrong email or password!', 401);
    }
    const payload: ReturnUserDTO = { ...user };

    return { token: await this.jwtService.signAsync(payload) };
  }

  public blacklistToken(token: string): void {
    this.tokensBlacklist.push(token);
  }

  public isTokenBlacklisted(token: string): boolean {
    return this.tokensBlacklist.includes(token);
  }

  private async validateUser(user: UserLoginDTO): Promise<ReturnUserDTO> {
    let userValidated: User;
    if (user.email) {
      userValidated = await this.userRepository.findOne({ email: user.email, isDeleted: false });
    } else {
      throw new ProjectPlanningSystemError('Please provide an email!', 400);
    }
    if (userValidated === undefined) {
      return null;
    }
    if (!(await bcrypt.compare(user.password, userValidated.password))) {
      return null;
    }
    return plainToClass(ReturnUserDTO, userValidated, { excludeExtraneousValues: true });
  }
}
