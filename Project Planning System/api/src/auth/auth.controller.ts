import { Controller, Post, Body, Delete, HttpCode, HttpStatus, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserLoginDTO } from '../users/models/user-login.dto';
import { Token } from '../common/decorators/token.decorator';
import { AuthenticationGuard } from '../common/guards/auth.guard';

@Controller('session')
export class AuthController {

  constructor(
    private readonly authService: AuthService,
  ) {}

  @Post()
  async login(@Body() user: UserLoginDTO): Promise<{ token: string }> {
    return await this.authService.login(user);
  }

  @Delete()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthenticationGuard)
  public async logout(@Token() token: string) {
    this.authService.blacklistToken(token);

    return {
      msg: 'Successful logout!',
    };
  }
}
