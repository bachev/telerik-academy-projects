import { ProjectReturnDTO } from './../projects/models/project-return.dto';
import { UpdateUserDTO } from './models/user-update.dto';
import { ReturnUserDTO } from './models/user-return.dto';
import { ProjectPlanningSystemError } from './../common/exceptions/pps.error';
import { CreateUserDTO } from './models/user-create.dto';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { User } from '../database/entities/user.entity';
import * as bcrypt from 'bcrypt';
import { Project } from '../database/entities/project.entity';
import { ReturnEmployeeDTO } from '../employees/models/return-employee.dto';
import { QueryUserDTO } from './models/query-user.dto';
import { ProjectStatus } from '../projects/enums/project-status.enum';
import { UserRole } from './enum/user-roles.enum';
import { Employee } from '../database/entities/employee.entity';
import { ChangeUserManagerDTO } from './models/change-user-manager.dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    private readonly jwtService: JwtService,
  ) {}

  public async getAllUsers(query: QueryUserDTO): Promise<ReturnUserDTO[]> {
    let foundUsers: User[] = await this.usersRepository.find({
      where: { isDeleted: false }, order: { firstName: 'ASC' },
    });
    if (query.firstName) {
      foundUsers = foundUsers.filter(employee =>
         employee.firstName.toLowerCase().includes(query.firstName.toLowerCase()));
    }
    if (query.lastName) {
      foundUsers = foundUsers.filter(employee =>
         employee.lastName.toLowerCase().includes(query.lastName.toLowerCase()));
    }
    if (query.position) {
      foundUsers = foundUsers.filter(employee =>
         employee.position.toLowerCase().includes(query.position.toLowerCase()));
    }

    return plainToClass(ReturnUserDTO, foundUsers, { excludeExtraneousValues: true });
  }

  public async getSingleUser(userId: number): Promise<ReturnUserDTO> {
    const foundUser: User = await this.findUserById(userId);

    return plainToClass(ReturnUserDTO, foundUser, { excludeExtraneousValues: true });
  }

  public async getUserToken(userId: number): Promise<{ token: string }> {
    const foundUser: User = await this.findUserById(userId);

    const returnUserDTO = plainToClass(ReturnUserDTO, foundUser, { excludeExtraneousValues: true });

    const payload: ReturnUserDTO = { ...returnUserDTO };

    return { token: await this.jwtService.signAsync(payload) };
  }

  public async getAllUserProjects(userId: number): Promise<ProjectReturnDTO[]> {
    const foundUser: User = await this.findUserById(userId);

    const currentProjects: Project[] = (await foundUser.projects)
    .filter(project => !project.isDeleted);

    return plainToClass(ProjectReturnDTO, currentProjects, { excludeExtraneousValues: true });
  }

  public async getUserInProgressProjects(userId: number): Promise<ProjectReturnDTO[]> {
    const foundUser: User = await this.findUserById(userId);

    const currentProjects: Project[] = (await foundUser.projects).filter(project =>
      !project.isDeleted && project.status === ProjectStatus.ONGOING);

    return plainToClass(ProjectReturnDTO, currentProjects, { excludeExtraneousValues: true });
  }

  public async getUserSubordinates(userId: number): Promise<ReturnEmployeeDTO[]> {
    const foundUser: User = await this.findUserById(userId);

    const subordinates: Employee[] = (await foundUser.subordinates).filter(
      employee => !employee.isDeleted);

    return plainToClass(ReturnEmployeeDTO, subordinates, { excludeExtraneousValues: true });
  }

  public async getUserDependentManagers(userId: number): Promise<ReturnUserDTO[]> {
    const foundUser: User = await this.findUserById(userId);

    const dependentManagers: User[] = (await foundUser.dependentManagers).filter(
      manager => !manager.isDeleted);

    return plainToClass(ReturnUserDTO, dependentManagers, { excludeExtraneousValues: true });
  }

  public async createUser(user: CreateUserDTO): Promise<ReturnUserDTO> {
    const existingEmail = await this.usersRepository.findOne({ email: user.email });

    if (existingEmail) {
      throw new ProjectPlanningSystemError(`User with ${existingEmail.email} already exists!`, 400);
    }
    const directManager: User = user.directManagerId === null ? null :
    await this.findUserById(user.directManagerId);
    const salt = await bcrypt.genSalt();

    const newUser: User = this.usersRepository.create(user);

    newUser.password = await bcrypt.hash(user.password, salt);
    newUser.directManagerName = directManager === null ? 'self-managed' :
    `${directManager.firstName} ${directManager.lastName}`;

    newUser.directManager = Promise.resolve(directManager);
    newUser.dependentManagers = Promise.resolve([]);
    newUser.projects = Promise.resolve([]);
    newUser.subordinates = Promise.resolve([]);

    if (user.isAdmin) {
      newUser.role = UserRole.Admin;
    }
    await this.usersRepository.save(newUser);

    return plainToClass(ReturnUserDTO, newUser, { excludeExtraneousValues: true });
  }

  public async updateUser(
    body: UpdateUserDTO, userId: number,
    user: ReturnUserDTO): Promise<ReturnUserDTO> {
    const foundUser: User = await this.findUserById(userId);

    if (userId !== user.id) {
      throw new ProjectPlanningSystemError(
        `You are not authorized to edit user with ID: ${userId}!`, 401);
    }
    const existingEmail = await this.usersRepository.findOne({ email: body.email });

    if (existingEmail) {
      throw new ProjectPlanningSystemError(`Email: ${existingEmail.email} already exists!`, 400);
    }
    if (body.email) {
      foundUser.email = body.email;
    }
    if (body.password) {
      foundUser.password = await bcrypt.hash(body.password, 10);
    }
    await this.usersRepository.save(foundUser);

    return plainToClass(ReturnUserDTO, foundUser, { excludeExtraneousValues: true });
  }

  public async changeUserManager(
    userId: number, manager: ChangeUserManagerDTO): Promise<ReturnUserDTO> {
    const foundUser: User = await this.findUserById(userId);
    const newManager: User = manager.directManagerId === null ? null :
    await this.findUserById(manager.directManagerId);

    foundUser.directManager = Promise.resolve(newManager);
    foundUser.directManagerName = newManager === null ? 'self-managed' :
    `${newManager.firstName} ${newManager.lastName}`;

    await this.usersRepository.save(foundUser);

    return plainToClass(ReturnUserDTO, foundUser, { excludeExtraneousValues: true });
  }

  public async deleteUser(userId: number): Promise<void> {
    const foundUser: User = await this.findUserById(userId);

    await this.usersRepository.update(foundUser.id, { isDeleted: true });
  }

  public async findUserById(userId: number): Promise<User> {
    const foundUser: User = await this.usersRepository.findOne(
      userId, { where: { isDeleted: false } });

    if (!foundUser) {
      throw new ProjectPlanningSystemError(`User with id ${userId} does not exist!`, 404);
    }

    return foundUser;
  }
}
