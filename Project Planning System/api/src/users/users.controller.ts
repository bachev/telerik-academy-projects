import { ProjectReturnDTO } from './../projects/models/project-return.dto';
import { ReturnUserDTO } from './models/user-return.dto';
import { Controller, Get, Put, Body, Param, ParseIntPipe, UseGuards, Query } from '@nestjs/common';
import { UsersService } from './users.service';
import { UpdateUserDTO } from './models/user-update.dto';
import { ReturnEmployeeDTO } from '../employees/models/return-employee.dto';
import { AuthenticationGuard } from '../common/guards/auth.guard';
import { User } from '../common/decorators/user.decorator';
import { QueryUserDTO } from './models/query-user.dto';

@Controller('users')
@UseGuards(AuthenticationGuard)
export class UsersController {

  constructor(
    private readonly usersService: UsersService,
  ) { }

  @Get()
  public async getAllUsers(@Query() query: QueryUserDTO): Promise<ReturnUserDTO[]> {
    return await this.usersService.getAllUsers(query);
  }

  @Get(':userId')
  public async getUserById(
    @Param('userId', ParseIntPipe) id: number,
  ): Promise<ReturnUserDTO> {
    return this.usersService.getSingleUser(id);
  }

  @Get(':userId/token')
  public async getUserToken(@Param('userId', ParseIntPipe) id: number): Promise<{ token: string }> {
    return this.usersService.getUserToken(id);
  }

  @Get(':userId/projects')
  public async getAllUserProjects(
    @Param('userId', ParseIntPipe) id: number): Promise <ProjectReturnDTO[]> {
    return await this.usersService.getAllUserProjects(id);
  }

  @Get(':userId/projects/inProgress')
  public async getUserInProgressProjects(
    @Param('userId', ParseIntPipe) id: number): Promise <ProjectReturnDTO[]> {
    return await this.usersService.getUserInProgressProjects(id);
  }

  @Get(':userId/employees')
  public async getUserSubordinates(
    @Param('userId', ParseIntPipe) id: number): Promise<ReturnEmployeeDTO[]> {
    return await this.usersService.getUserSubordinates(id);
  }

  @Get(':userId/managers')
  public async getUserDependentManagers(
    @Param('userId', ParseIntPipe) id: number): Promise<ReturnUserDTO[]> {
    return await this.usersService.getUserDependentManagers(id);
  }

  @Put(':userId')
  public async updateUser(
      @Param('userId', ParseIntPipe) id: number,
      @Body() body: UpdateUserDTO,
      @User() user: ReturnUserDTO): Promise<ReturnUserDTO> {
    return await this.usersService.updateUser(body, id, user);
  }
}
