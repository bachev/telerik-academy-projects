import { IsEmail, Matches, Length } from 'class-validator';

export class UserLoginDTO {
  @IsEmail()
  public email: string;

  @Length(4, 20)
  @Matches(/^[a-z0-9]+$/i, {
    message:
        'Password must contain only numbers and letters!',
  })
public password: string;
}
