import { Allow } from 'class-validator';

export class ChangeUserManagerDTO {
  @Allow()
  public directManagerId: number;
}
