import { IsOptional } from 'class-validator';

export class QueryUserDTO {
  @IsOptional()
    public firstName?: string;

  @IsOptional()
    public lastName?: string;

  @IsOptional()
    public position?: string;
}
