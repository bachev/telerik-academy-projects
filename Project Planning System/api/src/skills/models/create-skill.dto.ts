import { Length } from 'class-validator';

export class CreateSkillDTO {
  @Length(2, 40)
  public name: string;
}
