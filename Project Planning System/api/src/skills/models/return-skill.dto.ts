import { Expose } from 'class-transformer';

export class ReturnSkillDTO {
  @Expose()
  public id: number;

  @Expose()
  public name: string;
}
