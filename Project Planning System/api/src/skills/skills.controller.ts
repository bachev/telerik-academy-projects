import { Controller, Get, Param, ParseIntPipe, UseGuards } from '@nestjs/common';
import { SkillsService } from './skills.service';
import { ReturnSkillDTO } from './models/return-skill.dto';
import { ReturnEmployeeDTO } from '../employees/models/return-employee.dto';
import { AuthenticationGuard } from '../common/guards/auth.guard';

@Controller('skills')
@UseGuards(AuthenticationGuard)
export class SkillsController {

  public constructor(private readonly skillsService: SkillsService) {}

  @Get()
  public async getAllSkills(): Promise<ReturnSkillDTO[]> {
    return await this.skillsService.getAllSkills();
  }

  @Get(':skillId')
  public async getSkillById(@Param('skillId', ParseIntPipe) id: number): Promise<ReturnSkillDTO> {
    return await this.skillsService.getSkillById(id);
  }

  @Get(':skillId/employees')
  public async getSkillEmployees(
    @Param('skillId', ParseIntPipe) id: number): Promise<ReturnEmployeeDTO[]> {
    return await this.skillsService.getSkillEmployees(id);
  }
}
