import { Controller, Get, Query, Param, ParseIntPipe, UseGuards } from '@nestjs/common';
import { EmployeesService } from './employees.service';
import { QueryEmployeeDTO } from './models/query-employee.dto';
import { ReturnEmployeeDTO } from './models/return-employee.dto';
import { ReturnSkillDTO } from '../skills/models/return-skill.dto';
import { EmployeeCurrentProjectDTO } from './models/employee-current-project.dto';
import { AuthenticationGuard } from '../common/guards/auth.guard';

@Controller('employees')
@UseGuards(AuthenticationGuard)
export class EmployeesController {

  public constructor(private readonly employeesService: EmployeesService) { }

  @Get()
  public async getEmployees(@Query() query: QueryEmployeeDTO): Promise<ReturnEmployeeDTO[]> {
    return await this.employeesService.getEmployees(query);
  }

  @Get(':employeeId')
  public async getEmployeeById(
      @Param('employeeId', ParseIntPipe) id: number): Promise<ReturnEmployeeDTO> {
    return await this.employeesService.getEmployeeById(id);
  }

  @Get(':employeeId/skills')
  public async getEmployeeSkills(
    @Param('employeeId', ParseIntPipe) id: number): Promise<ReturnSkillDTO[]> {
    return await this.employeesService.getEmployeeSkills(id);
  }

  @Get(':employeeId/projects')
  public async getEmployeeProjects(
    @Param('employeeId', ParseIntPipe) id: number): Promise<EmployeeCurrentProjectDTO[]> {
    return await this.employeesService.getEmployeeProjects(id);
  }
}
