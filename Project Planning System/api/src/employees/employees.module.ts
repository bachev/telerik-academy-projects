import { Module } from '@nestjs/common';
import { EmployeesService } from './employees.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from '../database/entities/employee.entity';
import { EmployeesController } from './employees.controller';
import { User } from '../database/entities/user.entity';
import { Skill } from '../database/entities/skill.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Employee, User, Skill])],
  providers: [EmployeesService],
  controllers: [EmployeesController],
})
export class EmployeesModule { }
