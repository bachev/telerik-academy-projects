import { Min, ArrayNotEmpty, ArrayUnique, Length } from 'class-validator';

export class CreateEmployeeDTO {
  @Length(2, 10)
  public firstName: string;

  @Length(2, 20)
  public lastName: string;

  @Length(2, 40)
  public position: string;

  @Min(1)
  public managerId: number;

  @ArrayUnique()
  @ArrayNotEmpty()
  public skillIds: number[];
}
