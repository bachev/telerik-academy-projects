import { Expose } from 'class-transformer';

export class ReturnEmployeeDTO {
  @Expose()
    public id: number;

  @Expose()
    public firstName: string;

  @Expose()
    public lastName: string;

  @Expose()
    public position: string;

  @Expose()
    public directManager: string;

  @Expose()
    public workingHoursLeft: number;
}
