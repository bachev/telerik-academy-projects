import { Min } from 'class-validator';

export class AddNewSkillDTO {
  @Min(1)
    public skillId: number;
}
