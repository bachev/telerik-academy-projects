import { ExceptionFilter, Catch, ArgumentsHost } from "@nestjs/common";
import { ForumSystemError } from "../exceptions/forum-system.error";
import { Response } from 'express';

@Catch(ForumSystemError)
export class ForumSystemErrorFilter implements ExceptionFilter {
    public catch(exception: ForumSystemError, host: ArgumentsHost) {
        const context = host.switchToHttp();
        const response = context.getResponse<Response>();

        response.status(exception.code).json({
            status: exception.code,
            error: exception.message,
        });
    }
}