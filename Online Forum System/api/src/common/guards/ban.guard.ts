import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { AuthService } from "../../auth/auth.service";

@Injectable()
export class BanGuard implements CanActivate {

    public constructor(private readonly authService: AuthService) { }

    public async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const user = request.user;

        return await this.authService.isUserBanned(user);
    }
}