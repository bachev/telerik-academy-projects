import { AuthGuard } from "@nestjs/passport";
import { CanActivate, Injectable, ExecutionContext } from "@nestjs/common";
import { AuthService } from "../../auth/auth.service";
import { Request } from "express";

@Injectable()
export class AuthenticationGuard extends AuthGuard('jwt') implements CanActivate {

    public constructor(private readonly authService: AuthService) {
        super();
    }
    public async canActivate(context: ExecutionContext): Promise<boolean> {
        if (!(await super.canActivate(context))) {
            return false;
        }
        const request: Request = context.switchToHttp().getRequest();
        const token: string = request.headers.authorization;

        if (this.authService.isTokenBlacklisted(token)) {
            return false;
        } else {
            return true;
        }
    }
}