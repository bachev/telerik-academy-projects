import { CanActivate, Injectable, ExecutionContext } from "@nestjs/common";
import { UserRole } from "../../users/enums/user-role.enum";

@Injectable()
export class AdminGuard implements CanActivate {

    public canActivate(context: ExecutionContext): boolean {
        const request = context.switchToHttp().getRequest();
        const user = request.user;

        return user && user.role === UserRole.Admin;
    }
}