import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { User } from '../database/entities/user.entity';
import { Repository } from 'typeorm';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { LoginUserDTO } from '../users/models/login-user.dto';
import { ReturnUserDTO } from '../users/models/return-user.dto';

@Injectable()
export class AuthService {
  private readonly tokensBlacklist: string[] = [];

  public constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly jwtService: JwtService,
  ) {}

  public async loginUser(loginUser: LoginUserDTO): Promise<{ token: string }> {
    const user: ReturnUserDTO = await this.validateUser(loginUser);

    if (user === undefined || user === null) {
      throw new ForumSystemError('Wrong username/email or password!', 401);
    }
    const payload: ReturnUserDTO = { ...user };

    return { token: await this.jwtService.signAsync(payload) };
  }

  public blacklistToken(token: string): void {
    this.tokensBlacklist.push(token);
  }

  public isTokenBlacklisted(token: string): boolean {
    return this.tokensBlacklist.includes(token);
  }

  public async isUserBanned(user: ReturnUserDTO): Promise<boolean> {
    const userWithBanstatus = await this.userRepository.findOne(user.id);
    if (!(await userWithBanstatus.banstatus) || !(await userWithBanstatus.banstatus).isBanned) {
      return true;
    } else {
      return false;
    }
  }

  private async validateUser(user: LoginUserDTO): Promise<ReturnUserDTO> {
    let userValidated: User;
    if (user.email) {
      userValidated = await this.userRepository.findOne({ email: user.email, isDeleted: false });
    } else if (user.username) {
      userValidated = await this.userRepository.findOne({ username: user.username, isDeleted: false });
    } else {
      throw new ForumSystemError('Please provide username or email!', 400);
    }
    if (userValidated === undefined) {
      return null;
    }
    if (!(await bcrypt.compare(user.password, userValidated.password))) {
      return null;
    }

    return userValidated;
  }
}
