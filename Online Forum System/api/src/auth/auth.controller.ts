import { Controller, Post, Body, Delete, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginUserDTO } from '../users/models/login-user.dto';
import { Token } from '../common/decorators/token.decorator';
import { ResponseMessageDTO } from '../common/models/response-message.dto';
import { AuthenticationGuard } from '../common/guards/auth.guard';

@Controller('session')
export class AuthController {
  public constructor(private readonly authService: AuthService) {}

  @Post()
  public async login(@Body() user: LoginUserDTO): Promise<{ token: string }> {
    return await this.authService.loginUser(user);
  }

  @Delete()
  @UseGuards(AuthenticationGuard)
  public logout(@Token() token: string): ResponseMessageDTO {
    this.authService.blacklistToken(token);

    return { msg: 'Logout successful!' };
  }
}
