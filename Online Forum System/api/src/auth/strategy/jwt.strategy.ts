import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ReturnUserDTO } from '../../users/models/return-user.dto';
import { UsersService } from '../../users/users.service';
import { ForumSystemError } from '../../common/exceptions/forum-system.error';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly usersService: UsersService, configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get('JWT_SECRET'),
      ignoreExpiration: false,
    });
  }

  public async validate(payload: ReturnUserDTO): Promise<ReturnUserDTO> {
    const user = await this.usersService.getUserByUsername(payload.username);

    if (!user) {
      throw new ForumSystemError('Please login to continue!', 401);
    }

    return user;
  }
}
