/* eslint-disable @typescript-eslint/camelcase */
import { PostsService } from "./posts.service";
import { Test, TestingModule } from "@nestjs/testing";
import { Post } from "../database/entities/post.entity";
import { getRepositoryToken } from "@nestjs/typeorm";
import { User } from "../database/entities/user.entity";
import { Vote } from "../database/entities/vote.entity";
import { QueryPostDTO } from "./models/query-post.dto";
import { ReturnPostDTO } from "./models/return-post.dto";
import { ForumSystemError } from "../common/exceptions/forum-system.error";
import { UserRole } from "../users/enums/user-role.enum";
import { CreatePostDTO } from "./models/create-post.dto";
import { ReturnUserDTO } from "../users/models/return-user.dto";
import { UpdatePostDTO } from "./models/update-post.dto";
import { ResponseMessageDTO } from "../common/models/response-message.dto";

describe('PostsService', () => {
    let service: PostsService;
    let postRepository: any;
    let userRepository: any;
    let voteRepository: any;

    beforeEach(async () => {
        postRepository = {
            find: jest.fn(),
            findOne: jest.fn(),
            create: jest.fn(),
            save: jest.fn(),
        };

        userRepository = {
            findOne: jest.fn(),
        };

        voteRepository = {
            findOne: jest.fn(),
            create: jest.fn(),
            save: jest.fn(),
            find: jest.fn(),
        };

        const module: TestingModule = await Test.createTestingModule({
            providers: [
                PostsService,
                { provide: getRepositoryToken(Post), useValue: postRepository },
                { provide: getRepositoryToken(User), useValue: userRepository },
                { provide: getRepositoryToken(Vote), useValue: voteRepository },
            ],
        }).compile();

        service = module.get<PostsService>(PostsService);
    });

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(service).toBeDefined();
    });

    describe('getPosts() should', () => {
        it('call postRepository find() once with the correct filtering object', async () => {
            // Arrange
            const fakeQuery = new QueryPostDTO();
            const expectedFilteringObject = { where: { isDeleted: false }, order: { addedOn: "DESC" } };
            const spy = jest.spyOn(postRepository, 'find');

            // Act
            await service.getPosts(fakeQuery);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(expectedFilteringObject);
        });

        it('return a filtered collection of posts if query parameters are passed', async () => {
            // Arrange
            const fakePostsCollection = [
                { title: 'title', content: 'content' },
                { title: 'post', content: 'test' },
            ];
            const fakeQueryParam = { title: 'i', content: 'n' };
            const expectedResult = [{ title: 'title', content: 'content' }];

            jest.spyOn(postRepository, 'find').mockReturnValue(Promise.resolve(fakePostsCollection));

            // Act
            const actualResult = await service.getPosts(fakeQueryParam);

            // Assert
            expect(actualResult).toEqual(expectedResult);
        });

        it('return a transformed array of ReturnPostDTO objects', async () => {
            // Arrange
            const fakeQuery = new QueryPostDTO();
            const fakePostEntity: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const expectedResult: ReturnPostDTO = {
                title: 'title',
                id: 1,
                content: 'content',
                addedOn: 'date',
                isFlagged: false,
                isLocked: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
            };

            jest.spyOn(postRepository, 'find').mockReturnValue(Promise.resolve([fakePostEntity]));

            // Act
            const actualResult: ReturnPostDTO[] = await service.getPosts(fakeQuery);

            // Assert
            expect(actualResult.length).toEqual(1);
            expect(actualResult[0]).toBeInstanceOf(ReturnPostDTO);
            expect(actualResult[0]).toEqual(expectedResult);
        });
    });

    describe('getPostById() should', () => {
        it('call postRepository findOne() once with the passed postId and the correct filtering object', async () => {
            //Arrange
            const fakePost = new Post();
            const fakePostId = 1;
            const expectedFilteringObject = { where: { isDeleted: false } };
            const spy = jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));

            // Act
            await service.getPostById(fakePostId);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakePostId, expectedFilteringObject);
        });

        it('throw if the searched post is not found', async () => {
            // Arrange
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(undefined));

            // Act & Assert
            expect(service.getPostById(fakePostId)).rejects.toThrowError(ForumSystemError);
        });

        it('return a transformed ReturnPostDTO object', async () => {
            // Arrange
            const fakePostId = 1;
            const fakePostEntity: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const expectedResult: ReturnPostDTO = {
                title: 'title',
                id: 1,
                content: 'content',
                addedOn: 'date',
                isFlagged: false,
                isLocked: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null
            };

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePostEntity));

            // Act
            const actualResult: ReturnPostDTO = await service.getPostById(fakePostId);

            // Assert

            expect(actualResult).toBeInstanceOf(ReturnPostDTO);
            expect(actualResult).toEqual(expectedResult);
        });
    });

    describe('createPost() should', () => {
        it('call userRepository findOne() once with the passed userId and the correct filtering object', async () => {
            // Arrange
            const fakePost = new Post();
            const fakeUser = new User();
            const fakeReturnUserDTO = {
                username: 'username',
                id: 1,
                email: 'email',
                role: UserRole.Basic,
                joinedOn: 'date',
                avatar: 'avatar',
                isBanned: false
            };
            const fakeCreatePostDTO = new CreatePostDTO();
            const expectedFilteringObject = { where: { isDeleted: false } };
            const spy = jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeUser));

            jest.spyOn(postRepository, 'create').mockReturnValue(fakePost);
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act
            await service.createPost(fakeCreatePostDTO, fakeReturnUserDTO);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeReturnUserDTO.id, expectedFilteringObject);
        });

        it('call postRepository create() once with the passed parameters', async () => {
            // Arrange
            const fakePost = new Post();
            const fakeUser = new User();
            const fakeReturnUserDTO = new ReturnUserDTO();
            const fakeCreatePostDTO = { title: 'title', content: 'content' };
            const spy = jest.spyOn(postRepository, 'create').mockReturnValue(fakePost);

            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeUser));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act
            await service.createPost(fakeCreatePostDTO, fakeReturnUserDTO);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeCreatePostDTO);
        });

        it('call postRepository save() once with the correct parameters', async () => {
            // Arrange
            const fakePost = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeUser = new User();
            const fakeReturnUserDTO = new ReturnUserDTO();
            const fakeCreatePostDTO = new CreatePostDTO();
            const spy = jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeUser));
            jest.spyOn(postRepository, 'create').mockReturnValue(fakePost);

            // Act
            await service.createPost(fakeCreatePostDTO, fakeReturnUserDTO);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakePost);
        });

        it('return a transformed ReturnPostDTO object', async () => {
            // Arrange
            const fakePostEntity: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const expectedResult: ReturnPostDTO = {
                title: 'title',
                id: 1,
                content: 'content',
                addedOn: 'date',
                isFlagged: false,
                isLocked: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: undefined
            };
            const fakeUser = new User();
            const fakeReturnUserDTO = new ReturnUserDTO();
            const fakeCreatePostDTO = new CreatePostDTO();

            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePostEntity));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeUser));
            jest.spyOn(postRepository, 'create').mockReturnValue(fakePostEntity);

            // Act
            const actualResult: ReturnPostDTO = await service.createPost(fakeCreatePostDTO, fakeReturnUserDTO);

            // Assert
            expect(actualResult).toBeInstanceOf(ReturnPostDTO);
            expect(actualResult).toEqual(expectedResult);
        });
    });

    describe('updatePost() should', () => {
        it('call postRepository findOne() once with the passed postId and the correct filtering object', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const expectedFilteringObject = { where: { isDeleted: false } };
            const fakeUpdate = new UpdatePostDTO();
            const spy = jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));

            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act
            await service.updatePost(fakePostId, fakeUpdate, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakePostId, expectedFilteringObject);
        });

        it('throw if the searched post is not found', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const fakeUpdate = new UpdatePostDTO();

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(undefined));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act & Assert
            expect(service.updatePost(fakePostId, fakeUpdate, fakeAuthUser)).rejects.toThrowError(ForumSystemError);
        });

        it('throw if the post to be updated is locked and the updating user is not an admin', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: true,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const fakeUpdate = new UpdatePostDTO();

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act & Assert
            expect(service.updatePost(fakePostId, fakeUpdate, fakeAuthUser)).rejects.toThrowError(ForumSystemError);
        });

        it('not throw if the post to be updated is locked but the updating user is an admin', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Admin,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: true,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const fakeUpdate = new UpdatePostDTO();

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act & Assert
            expect(service.updatePost(fakePostId, fakeUpdate, fakeAuthUser)).resolves.not.toThrow();
        });

        it('throw if the updating user is not the author of the post and is not an admin', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 2,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const fakeUpdate = new UpdatePostDTO();

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act & Assert
            expect(service.updatePost(fakePostId, fakeUpdate, fakeAuthUser)).rejects.toThrowError(ForumSystemError);
        });

        it('not throw if the updating user is not the author of the post but is an admin', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 2,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Admin,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const fakeUpdate = new UpdatePostDTO();

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act & Assert
            expect(service.updatePost(fakePostId, fakeUpdate, fakeAuthUser)).resolves.not.toThrow();
        });

        it('update the post with the passed parameters', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const expectedResult: ReturnPostDTO = {
                title: 'new',
                id: 1,
                content: 'test',
                addedOn: 'date',
                isFlagged: false,
                isLocked: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: 1,
            };
            const fakePostId = 1;
            const fakeUpdate: UpdatePostDTO = { title: 'new', content: 'test', };

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act
            const actualResult: ReturnPostDTO = await service.updatePost(fakePostId, fakeUpdate, fakeAuthUser);

            // Assert
            expect(actualResult).toEqual(expectedResult);
        });

        it('call postRepository save() once with the correct post', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const fakeUpdate = new UpdatePostDTO();
            const spy = jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));

            // Act
            await service.updatePost(fakePostId, fakeUpdate, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakePost);
        });

        it('return a transformed ReturnPostDTO object', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePostEntity: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const expectedResult: ReturnPostDTO = {
                title: 'title',
                id: 1,
                content: 'content',
                addedOn: 'date',
                isFlagged: false,
                isLocked: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: 1
            };
            const fakePostId = 1;
            const fakeUpdate = new UpdatePostDTO();

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePostEntity));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePostEntity));

            // Act
            const actualResult: ReturnPostDTO = await service.updatePost(fakePostId, fakeUpdate, fakeAuthUser);

            // Assert
            expect(actualResult).toBeInstanceOf(ReturnPostDTO);
            expect(actualResult).toEqual(expectedResult);
        });
    });

    describe('deletePost() should', () => {
        it('call postRepository findOne() once with the passed postId and the correct filtering object', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const expectedFilteringObject = { where: { isDeleted: false } };
            const spy = jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));

            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act
            await service.deletePost(fakePostId, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakePostId, expectedFilteringObject);
        });

        it('throw if the searched post is not found', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(undefined));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act & Assert
            expect(service.deletePost(fakePostId, fakeAuthUser)).rejects.toThrowError(ForumSystemError);
        });

        it('throw if the post to be updated is locked and the updating user is not an admin', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: true,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act & Assert
            expect(service.deletePost(fakePostId, fakeAuthUser)).rejects.toThrowError(ForumSystemError);
        });

        it('not throw if the post to be updated is locked but the updating user is an admin', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Admin,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: true,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act & Assert
            expect(service.deletePost(fakePostId, fakeAuthUser)).resolves.not.toThrow();
        });

        it('throw if the updating user is not the author of the post and is not an admin', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 2,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act & Assert
            expect(service.deletePost(fakePostId, fakeAuthUser)).rejects.toThrowError(ForumSystemError);
        });

        it('not throw if the updating user is not the author of the post but is an admin', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 2,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Admin,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act & Assert
            expect(service.deletePost(fakePostId, fakeAuthUser)).resolves.not.toThrow();
        });

        it('set the isDeleted property of the found post to true', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act
            await service.deletePost(fakePostId, fakeAuthUser);

            // Assert
            expect(fakePost.isDeleted).toEqual(true);
        });

        it('call postRepository save() once with the correct post', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const spy = jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));

            // Act
            await service.deletePost(fakePostId, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakePost);
        });

        it('return the correct success message', async () => {
            // Arrange
            const fakePostAuthor: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: fakePostAuthor.id,
                user: Promise.resolve(fakePostAuthor),
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const expectedResult: ResponseMessageDTO = { msg: 'Post deleted!' };

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act
            const actualResult = await service.deletePost(fakePostId, fakeAuthUser);

            // Assert
            expect(actualResult).toEqual(expectedResult);
        });
    });

    describe('flagPost() should', () => {
        it('call postRepository findOne() once with the passed postId and the correct filtering object', async () => {
            // Arrange
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const expectedFilteringObject = { where: { isDeleted: false } };
            const spy = jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));

            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act
            await service.flagPost(fakePostId);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakePostId, expectedFilteringObject);
        });

        it('throw if the searched post is not found', async () => {
            // Arrange
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(undefined));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act & Assert
            expect(service.flagPost(fakePostId)).rejects.toThrowError(ForumSystemError);
        });

        it('throw if the post to be flagged is locked', async () => {
            // Arrange
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: true,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act & Assert
            expect(service.flagPost(fakePostId)).rejects.toThrowError(ForumSystemError);
        });

        it('set the isFlagged property of the found post to true', async () => {
            // Arrange
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act
            await service.flagPost(fakePostId);

            // Assert
            expect(fakePost.isFlagged).toEqual(true);
        });

        it('call postRepository save() once with the correct post', async () => {
            // Arrange
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const spy = jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));

            // Act
            await service.flagPost(fakePostId);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakePost);
        });

        it('return a transformed ReturnPostDTO object', async () => {
            // Arrange
            const fakePostEntity: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const expectedResult: ReturnPostDTO = {
                title: 'title',
                id: 1,
                content: 'content',
                addedOn: 'date',
                isFlagged: true,
                isLocked: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePostEntity));
            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePostEntity));

            // Act
            const actualResult: ReturnPostDTO = await service.flagPost(fakePostId);

            // Assert
            expect(actualResult).toBeInstanceOf(ReturnPostDTO);
            expect(actualResult).toEqual(expectedResult);
        });
    });

    describe('likePost() should', () => {
        it('call postRepository findOne() once with the passed postId and the correct filtering object', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: false,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;
            const expectedFilteringObject = { where: { isDeleted: false } };
            const spy = jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));

            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeVote]));

            // Act
            await service.likePost(fakePostId, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakePostId, expectedFilteringObject);
        });
        it('throw if the post to be liked is not found', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: false,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(undefined));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));

            // Act & Assert
            expect(service.likePost(fakePostId, fakeAuthUser)).rejects.toThrowError(ForumSystemError);
        });

        it('throw if the post to be liked is locked and the updating user is not an admin', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: true,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: false,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));

            // Act & Assert
            expect(service.likePost(fakePostId, fakeAuthUser)).rejects.toThrowError(ForumSystemError);
        });

        it('not throw if the post to be liked is locked but the updating user is an admin', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Admin,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: true,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: false,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));

            // Act & Assert
            expect(service.likePost(fakePostId, fakeAuthUser)).resolves.not.toThrow();
        });

        it('call userRepository findOne() once with the passed userId', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: false,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;
            const fakeUserId = { id: fakeAuthUserEntity.id };
            const spy = jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeVote]));


            // Act
            await service.likePost(fakePostId, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeUserId);
        });

        it('call voteRepository findOne() once with the correct filtering object', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: false,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;
            const expectedFilteringObject = { where: { post: fakePost, user: fakeAuthUserEntity } };
            const spy = jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));

            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeVote]));

            // Act
            await service.likePost(fakePostId, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(expectedFilteringObject);
        });

        it('set the isLiked property of the found vote to true', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: false,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;

            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeVote]));

            // Act
            await service.likePost(fakePostId, fakeAuthUser);

            // Assert
            expect(fakeVote.isLiked).toEqual(true);
        });

        it('call voteRepository save() once with the correct parameter', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: false,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;
            const spy = jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));

            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeVote]));

            // Act
            await service.likePost(fakePostId, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeVote);
        });

        it('call voteRepository create() once with the correct parameter if the searched vote is not found', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeNewVote: Vote = {
                id: 1,
                isLiked: false,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;
            const expectedParam = { isLiked: true };
            const spy = jest.spyOn(voteRepository, 'create').mockReturnValue({ isLiked: true });

            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(undefined));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeNewVote));
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeNewVote]));

            // Act
            await service.likePost(fakePostId, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(expectedParam);
        });

        it('call voteRepository save() once with the correct parameter if the searched vote is not found', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeNewVote: Vote = {
                id: 1,
                isLiked: false,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const expectedParam = {
                isLiked: true,
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
            };
            const fakePostId = 1;
            const spy = jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeNewVote));

            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(undefined));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(voteRepository, 'create').mockReturnValue({ isLiked: true });
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeNewVote]));

            // Act
            await service.likePost(fakePostId, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(expectedParam);
        });

        it('return a transformed ReturnPostDTO object', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePostEntity: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: false,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePostEntity),
                comment: null,
            };
            const expectedResult: ReturnPostDTO = {
                title: 'title',
                id: 1,
                content: 'content',
                addedOn: 'date',
                isFlagged: false,
                isLocked: false,
                likesCount: 1,
                dislikesCount: 1,
                authorId: null
            };
            const fakePostId = 1;

            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePostEntity));
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeVote]));

            // Act
            const actualResult: ReturnPostDTO = await service.likePost(fakePostId, fakeAuthUser);

            // Assert
            expect(actualResult).toBeInstanceOf(ReturnPostDTO);
            expect(actualResult).toEqual(expectedResult);
        });
    });

    describe('unlikePost() should', () => {
        it('call postRepository findOne() once with the passed postId and the correct filtering object', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: true,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;
            const expectedFilteringObject = { where: { isDeleted: false } };
            const spy = jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));

            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeVote]));

            // Act
            await service.unlikePost(fakePostId, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakePostId, expectedFilteringObject);
        });
        it('throw if the post to be liked is not found', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: true,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(undefined));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));

            // Act & Assert
            expect(service.unlikePost(fakePostId, fakeAuthUser)).rejects.toThrowError(ForumSystemError);
        });

        it('throw if the post to be liked is locked and the updating user is not an admin', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: true,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: true,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));

            // Act & Assert
            expect(service.unlikePost(fakePostId, fakeAuthUser)).rejects.toThrowError(ForumSystemError);
        });

        it('not throw if the post to be liked is locked but the updating user is an admin', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Admin,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: true,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: true,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));

            // Act & Assert
            expect(service.unlikePost(fakePostId, fakeAuthUser)).resolves.not.toThrow();
        });

        it('call userRepository findOne() once with the passed userId', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: true,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;
            const fakeUserId = { id: fakeAuthUserEntity.id };
            const spy = jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeVote]));


            // Act
            await service.unlikePost(fakePostId, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeUserId);
        });

        it('call voteRepository findOne() once with the correct filtering object', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: true,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;
            const expectedFilteringObject = { where: { post: fakePost, user: fakeAuthUserEntity } };
            const spy = jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));

            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeVote]));

            // Act
            await service.unlikePost(fakePostId, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(expectedFilteringObject);
        });

        it('set the isLiked property of the found vote to false', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: true,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;

            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeVote]));

            // Act
            await service.unlikePost(fakePostId, fakeAuthUser);

            // Assert
            expect(fakeVote.isLiked).toEqual(false);
        });

        it('call voteRepository save() once with the correct parameter', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: true,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;
            const spy = jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));

            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeVote]));

            // Act
            await service.unlikePost(fakePostId, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeVote);
        });

        it('call voteRepository create() once with the correct parameter if the searched vote is not found', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeNewVote: Vote = {
                id: 1,
                isLiked: true,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const fakePostId = 1;
            const expectedParam = { isLiked: false };
            const spy = jest.spyOn(voteRepository, 'create').mockReturnValue({ isLiked: true });

            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(undefined));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeNewVote));
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeNewVote]));

            // Act
            await service.unlikePost(fakePostId, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(expectedParam);
        });

        it('call voteRepository save() once with the correct parameter if the searched vote is not found', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeNewVote: Vote = {
                id: 1,
                isLiked: true,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
                comment: null,
            };
            const expectedParam = {
                isLiked: true,
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePost),
            };
            const fakePostId = 1;
            const spy = jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeNewVote));

            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(undefined));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(voteRepository, 'create').mockReturnValue({ isLiked: true });
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeNewVote]));

            // Act
            await service.unlikePost(fakePostId, fakeAuthUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(expectedParam);
        });

        it('return a transformed ReturnPostDTO object', async () => {
            // Arrange
            const fakeAuthUserEntity: User = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                password: 'password',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false,
                isDeleted: false,
                posts: null,
                votes: null,
                comments: null,
                banstatus: null,
                friends_requests: null,
                friends_received: null,
            };
            const fakeAuthUser: ReturnUserDTO = {
                id: 1,
                joinedOn: 'date',
                username: 'username',
                email: 'email',
                role: UserRole.Basic,
                avatar: 'avatar',
                isBanned: false
            };
            const fakePostEntity: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakeVote: Vote = {
                id: 1,
                isLiked: true,
                addedOn: 'date',
                user: Promise.resolve(fakeAuthUserEntity),
                post: Promise.resolve(fakePostEntity),
                comment: null,
            };
            const expectedResult: ReturnPostDTO = {
                title: 'title',
                id: 1,
                content: 'content',
                addedOn: 'date',
                isFlagged: false,
                isLocked: false,
                likesCount: 1,
                dislikesCount: 1,
                authorId: null
            };
            const fakePostId = 1;

            jest.spyOn(voteRepository, 'save').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(voteRepository, 'findOne').mockReturnValue(Promise.resolve(fakeVote));
            jest.spyOn(userRepository, 'findOne').mockReturnValue(Promise.resolve(fakeAuthUserEntity));
            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePostEntity));
            jest.spyOn(voteRepository, 'find').mockReturnValue(Promise.resolve([fakeVote]));

            // Act
            const actualResult: ReturnPostDTO = await service.unlikePost(fakePostId, fakeAuthUser);

            // Assert
            expect(actualResult).toBeInstanceOf(ReturnPostDTO);
            expect(actualResult).toEqual(expectedResult);
        });
    });

    describe('togglePostLock() should', () => {
        it('call postRepository findOne() once with the passed postId and the correct filtering object', async () => {
            //Arrange
            const fakePost = new Post();
            const fakePostId = 1;
            const fakeLockPostDTO = { isLocked: true };
            const expectedFilteringObject = { where: { isDeleted: false } };
            const spy = jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));

            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act
            await service.togglePostLock(fakePostId, fakeLockPostDTO);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakePostId, expectedFilteringObject);
        });

        it('throw if the searched post is not found', async () => {
            // Arrange
            const fakePostId = 1;
            const fakeLockPostDTO = { isLocked: true };

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(undefined));

            // Act & Assert
            expect(service.togglePostLock(fakePostId, fakeLockPostDTO)).rejects.toThrowError(ForumSystemError);
        });

        it('set the isLocked property of the found post to true when false', async () => {
            // Arrange
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const fakeLockPostDTO = { isLocked: true };

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act
            await service.togglePostLock(fakePostId, fakeLockPostDTO)

            // Assert
            expect(fakePost.isLocked).toEqual(true);
        });

        it('set the isLocked property of the found post to false when true', async () => {
            // Arrange
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: true,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const fakeLockPostDTO = { isLocked: false };

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            // Act
            await service.togglePostLock(fakePostId, fakeLockPostDTO)

            // Assert
            expect(fakePost.isLocked).toEqual(false);
        });

        it('call postRepository save() once with the passed post', async () => {
            //Arrange
            const fakePost: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const fakePostId = 1;
            const fakeLockPostDTO = { isLocked: true };
            const spy = jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePost));

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePost));


            // Act
            await service.togglePostLock(fakePostId, fakeLockPostDTO);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakePost);
        });

        it('return a transformed ReturnPostDTO object', async () => {
            // Arrange
            const fakePostEntity: Post = {
                id: 1,
                addedOn: 'date',
                title: 'title',
                content: 'content',
                isFlagged: false,
                isLocked: false,
                isDeleted: false,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null,
                user: null,
                comments: null,
                votes: null,
            };
            const expectedResult: ReturnPostDTO = {
                title: 'title',
                id: 1,
                content: 'content',
                addedOn: 'date',
                isFlagged: false,
                isLocked: true,
                likesCount: 0,
                dislikesCount: 0,
                authorId: null
            };
            const fakePostId = 1;
            const fakeLockPostDTO = { isLocked: true };

            jest.spyOn(postRepository, 'findOne').mockReturnValue(Promise.resolve(fakePostEntity));
            jest.spyOn(postRepository, 'save').mockReturnValue(Promise.resolve(fakePostEntity));

            // Act
            const actualResult: ReturnPostDTO = await service.togglePostLock(fakePostId, fakeLockPostDTO);

            // Assert

            expect(actualResult).toBeInstanceOf(ReturnPostDTO);
            expect(actualResult).toEqual(expectedResult);
        });
    });
});