import { PostsController } from "./posts.controller";
import { TestingModule, Test } from "@nestjs/testing";
import { PostsService } from "./posts.service";
import { AuthService } from "../auth/auth.service";
import { CreatePostDTO } from "./models/create-post.dto";
import { ReturnUserDTO } from "../users/models/return-user.dto";
import { QueryPostDTO } from "./models/query-post.dto";
import { UpdatePostDTO } from "./models/update-post.dto";
import { UserRole } from "../users/enums/user-role.enum";

describe('PostsController', () => {
    let controller: PostsController;
    let postsService: any;
    let authService: any;

    beforeEach(async () => {
        authService = {
            isTokenBlacklisted: jest.fn(),
            isUserBanned: jest.fn(),
        };

        postsService = {
            getPosts: jest.fn(),
            getPostById: jest.fn(),
            createPost: jest.fn(),
            updatePost: jest.fn(),
            deletePost: jest.fn(),
            flagPost: jest.fn(),
            likePost: jest.fn(),
            unlikePost: jest.fn(),
        };

        const module: TestingModule = await Test.createTestingModule({
            controllers: [PostsController],
            providers: [
                { provide: PostsService, useValue: postsService },
                { provide: AuthService, useValue: authService }
            ],
        }).compile();

        controller = module.get<PostsController>(PostsController);
    });

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(controller).toBeDefined();
    });

    describe('getPosts() should', () => {
        it('call postsService getPosts() once with the passed query parameters', async () => {
            // Arrange
            const fakeQuery = { title: 'p' };
            const spy = jest.spyOn(postsService, 'getPosts');

            // Act
            await controller.getPosts(fakeQuery);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeQuery);
        });

        it('return the result from postsService getPosts()', async () => {
            // Arrange
            const fakeQuery = new QueryPostDTO();
            const controlResult = { title: 'post' };

            jest.spyOn(postsService, 'getPosts').mockReturnValue(Promise.resolve(controlResult));

            // Act
            const actualResult = await controller.getPosts(fakeQuery);

            // Assert
            expect(actualResult).toEqual(controlResult);
        });
    });

    describe('getPostById() should', () => {
        it('call postsService getPostById() once with the passed id', async () => {
            // Arrange
            const fakeId = 1;
            const spy = jest.spyOn(postsService, 'getPostById');

            // Act
            await controller.getPostById(fakeId);

            //Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeId);
        });

        it('return the result from postsService getPostById()', async () => {
            // Arrange
            const fakeId = 1;
            const controlResult = { title: 'post' };

            jest.spyOn(postsService, 'getPostById').mockReturnValue(Promise.resolve(controlResult));

            // Act
            const actualResult = await controller.getPostById(fakeId);

            // Assert
            expect(actualResult).toEqual(controlResult);
        });
    });

    describe('createPost() should', () => {
        it('call postsService createPost() once with the passed parameters', async () => {
            // Arrange
            const fakeCreatePostDTO = { title: 'title', content: 'content' };
            const fakeUser = { username: 'username', id: 1, email: 'email', role: UserRole.Basic, joinedOn: 'date', avatar: 'avatar' };
            const spy = jest.spyOn(postsService, 'createPost');

            // Act
            await controller.createPost(fakeCreatePostDTO, fakeUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeCreatePostDTO, fakeUser);
        });

        it('return the result from postsService createPost()', async () => {
            // Arrange
            const fakeCreatePostDTO = new CreatePostDTO();
            const fakeUser = new ReturnUserDTO();
            const controlResult = { title: 'post' };

            jest.spyOn(postsService, 'createPost').mockReturnValue(Promise.resolve(controlResult));

            // Act
            const actualResult = await controller.createPost(fakeCreatePostDTO, fakeUser);

            // Assert
            expect(actualResult).toEqual(controlResult);
        });
    });

    describe('updatePost() should', () => {
        it('call postsService updatePost() once with the passed parameters', async () => {
            // Arrange
            const fakeId = 1;
            const fakeUpdatePostDTO = { title: 'title' };
            const fakeUser = { username: 'username', id: 1, email: 'email', role: UserRole.Basic, joinedOn: 'date', avatar: 'avatar' };
            const spy = jest.spyOn(postsService, 'updatePost');

            // Act
            await controller.updatePost(fakeId, fakeUpdatePostDTO, fakeUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeId, fakeUpdatePostDTO, fakeUser);
        });

        it('return the result from postsService createPost()', async () => {
            // Arrange
            const fakeId = 1;
            const fakeUpdatePostDTO = new UpdatePostDTO();
            const fakeUser = new ReturnUserDTO();
            const controlResult = { title: 'post' };

            jest.spyOn(postsService, 'updatePost').mockReturnValue(Promise.resolve(controlResult));

            // Act
            const actualResult = await controller.updatePost(fakeId, fakeUpdatePostDTO, fakeUser);

            // Assert
            expect(actualResult).toEqual(controlResult);
        });
    });

    describe('deletePost() should', () => {
        it('call postsService deletePost() once with the passed parameters', async () => {
            // Arrange
            const fakeId = 1;
            const fakeUser = { username: 'username', id: 1, email: 'email', role: UserRole.Basic, joinedOn: 'date', avatar: 'avatar' };
            const spy = jest.spyOn(postsService, 'deletePost');

            // Act
            await controller.deletePost(fakeId, fakeUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeId, fakeUser);
        });

        it('return the result from postsService deletePost()', async () => {
            // Arrange
            const fakeId = 1;
            const fakeUser = new ReturnUserDTO();
            const controlResult = { msg: 'Post deleted!' };

            jest.spyOn(postsService, 'deletePost').mockReturnValue(Promise.resolve(controlResult));

            // Act
            const actualResult = await controller.deletePost(fakeId, fakeUser);

            // Assert
            expect(actualResult).toEqual(controlResult);
        });
    });

    describe('flagPost() should', () => {
        it('call postsService flagPost() once with the passed id', async () => {
            // Arrange
            const fakeId = 1;
            const spy = jest.spyOn(postsService, 'flagPost');

            // Act
            await controller.flagPost(fakeId);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeId);
        });

        it('return the result from postsService flagPost()', async () => {
            // Arrange
            const fakeId = 1;
            const controlResult = { title: 'post' };

            jest.spyOn(postsService, 'flagPost').mockReturnValue(Promise.resolve(controlResult));

            // Act
            const actualResult = await controller.flagPost(fakeId);

            // Assert
            expect(actualResult).toEqual(controlResult);
        });
    });

    describe('likePost() should', () => {
        it('call postsService likePost() once with the passed parameters', async () => {
            // Arrange
            const fakeId = 1;
            const fakeUser = { username: 'username', id: 1, email: 'email', role: UserRole.Basic, joinedOn: 'date', avatar: 'avatar' };
            const spy = jest.spyOn(postsService, 'likePost');

            // Act
            await controller.likePost(fakeId, fakeUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeId, fakeUser);
        });

        it('return the result from postsService likePost()', async () => {
            // Arrange
            const fakeId = 1;
            const fakeUser = new ReturnUserDTO();
            const controlResult = { title: 'post' };

            jest.spyOn(postsService, 'likePost').mockReturnValue(Promise.resolve(controlResult));

            // Act
            const actualResult = await controller.likePost(fakeId, fakeUser);

            // Assert
            expect(actualResult).toEqual(controlResult);
        });
    });

    describe('unlikePost() should', () => {
        it('call postsService unlikePost() once with the passed parameters', async () => {
            // Arrange
            const fakeId = 1;
            const fakeUser = { username: 'username', id: 1, email: 'email', role: UserRole.Basic, joinedOn: 'date', avatar: 'avatar' };
            const spy = jest.spyOn(postsService, 'unlikePost');

            // Act
            await controller.unlikePost(fakeId, fakeUser);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeId, fakeUser);
        });

        it('return the result from postsService unlikePost()', async () => {
            // Arrange
            const fakeId = 1;
            const fakeUser = new ReturnUserDTO();
            const controlResult = { title: 'post' };

            jest.spyOn(postsService, 'unlikePost').mockReturnValue(Promise.resolve(controlResult));

            // Act
            const actualResult = await controller.unlikePost(fakeId, fakeUser);

            // Assert
            expect(actualResult).toEqual(controlResult);
        });
    });
});