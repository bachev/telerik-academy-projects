import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { ResponseMessageDTO } from '../common/models/response-message.dto';
import { Post } from '../database/entities/post.entity';
import { User } from '../database/entities/user.entity';
import { Vote } from '../database/entities/vote.entity';
import { UserRole } from '../users/enums/user-role.enum';
import { ReturnUserDTO } from '../users/models/return-user.dto';
import { CreatePostDTO } from './models/create-post.dto';
import { LockPostDTO } from './models/lock-post.dto';
import { QueryPostDTO } from './models/query-post.dto';
import { ReturnPostDTO } from './models/return-post.dto';
import { UpdatePostDTO } from './models/update-post.dto';

@Injectable()
export class PostsService {
  public constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Post) private readonly postRepository: Repository<Post>,
    @InjectRepository(Vote) private readonly voteRepository: Repository<Vote>,
  ) { }

  async getPosts(query: QueryPostDTO): Promise<ReturnPostDTO[]> {
    let posts = await this.postRepository.find({ where: { isDeleted: false }, order: { addedOn: "DESC" } });

    if (query.title) {
      posts = posts.filter(post => post.title.toLowerCase().includes(query.title.toLowerCase()));
    }
    if (query.content) {
      posts = posts.filter(post => post.content.toLowerCase().includes(query.content.toLowerCase()));
    }

    return plainToClass(ReturnPostDTO, posts, { excludeExtraneousValues: true });
  }

  public async getPostById(postId: number): Promise<ReturnPostDTO> {
    const post: Post = await this.findPostById(postId);

    return plainToClass(ReturnPostDTO, post, { excludeExtraneousValues: true });
  }

  public async createPost(post: CreatePostDTO, author: ReturnUserDTO): Promise<ReturnPostDTO> {
    const user: User = await this.userRepository.findOne(author.id, { where: { isDeleted: false } });

    const newPost: Post = this.postRepository.create(post);
    newPost.authorId = author.id;
    newPost.user = Promise.resolve(user);
    newPost.comments = Promise.resolve([]);

    await this.postRepository.save(newPost);

    return plainToClass(ReturnPostDTO, newPost, { excludeExtraneousValues: true });
  }

  public async updatePost(postId: number, update: UpdatePostDTO, user: ReturnUserDTO): Promise<ReturnPostDTO> {
    let post: Post = await this.findPostById(postId);

    if (post.isLocked && user.role !== UserRole.Admin) {
      throw new ForumSystemError('This post is locked!', 403);
    }
    if ((await post.user).id !== user.id && user.role !== UserRole.Admin) {
      throw new ForumSystemError('You are not authorized to edit this post!', 401);
    }
    post = { ...post, ...update };

    await this.postRepository.save(post);

    return plainToClass(ReturnPostDTO, post, { excludeExtraneousValues: true });
  }

  public async deletePost(postId: number, user: ReturnUserDTO): Promise<ResponseMessageDTO> {
    const post: Post = await this.findPostById(postId);

    if (post.isLocked && user.role !== UserRole.Admin) {
      throw new ForumSystemError('This post is locked!', 403);
    }
    if ((await post.user).id !== user.id && user.role !== UserRole.Admin) {
      throw new ForumSystemError('You are not authorized to delete this post!', 401);
    }
    post.isDeleted = true;

    await this.postRepository.save(post);

    return { msg: 'Post deleted!' };
  }

  public async flagPost(postId: number): Promise<ReturnPostDTO> {
    const post: Post = await this.findPostById(postId);

    if (post.isLocked) {
      throw new ForumSystemError('This post is locked!', 403);
    }
    post.isFlagged = true;

    await this.postRepository.save(post);

    return plainToClass(ReturnPostDTO, post, { excludeExtraneousValues: true });
  }

  public async likePost(postId: number, user: ReturnUserDTO): Promise<ReturnPostDTO> {
    const foundPost: Post = await this.findPostById(postId);

    if (foundPost.isLocked && user.role !== UserRole.Admin) {
      throw new ForumSystemError('This post is locked!', 403);
    }
    const foundUser: User = await this.userRepository.findOne({ id: user.id });
    const foundVote: Vote = await this.voteRepository.findOne({ where: { post: foundPost, user: foundUser } });
    if (foundVote === undefined) {
      const newVote: Vote = this.voteRepository.create({ isLiked: true });
      newVote.user = Promise.resolve(foundUser);
      newVote.post = Promise.resolve(foundPost);

      await this.voteRepository.save(newVote);
    } else {
      foundVote.isLiked = true;

      await this.voteRepository.save(foundVote);
    }
    await this.updatePostVotesCount(foundPost);

    return plainToClass(ReturnPostDTO, foundPost, { excludeExtraneousValues: true });
  }

  public async unlikePost(postId: number, user: ReturnUserDTO): Promise<ReturnPostDTO> {
    const foundPost: Post = await this.findPostById(postId);

    if (foundPost.isLocked && user.role !== UserRole.Admin) {
      throw new ForumSystemError('This post is locked!', 403);
    }
    const foundUser: User = await this.userRepository.findOne({ id: user.id });
    const foundVote: Vote = await this.voteRepository.findOne({ where: { post: foundPost, user: foundUser } });
    if (foundVote === undefined) {
      const newVote: Vote = this.voteRepository.create({ isLiked: false });
      newVote.user = Promise.resolve(foundUser);
      newVote.post = Promise.resolve(foundPost);

      await this.voteRepository.save(newVote);
    } else {
      foundVote.isLiked = false;

      await this.voteRepository.save(foundVote);
    }
    await this.updatePostVotesCount(foundPost);

    return plainToClass(ReturnPostDTO, foundPost, { excludeExtraneousValues: true });
  }

  public async togglePostLock(postId: number, lock: LockPostDTO): Promise<ReturnPostDTO> {
    const post: Post = await this.findPostById(postId);

    post.isLocked = lock.isLocked;

    await this.postRepository.save(post);

    return plainToClass(ReturnPostDTO, post, { excludeExtraneousValues: true });
  }

  private async updatePostVotesCount(post: Post): Promise<void> {
    const postLikes: Vote[] = await this.voteRepository.find({ where: { post, isLiked: true } });
    const postDislikes: Vote[] = await this.voteRepository.find({ where: { post, isLiked: false } });

    post.likesCount = postLikes.length;
    post.dislikesCount = postDislikes.length;

    await this.postRepository.save(post);
  }

  private async findPostById(postId: number): Promise<Post> {
    const post: Post = await this.postRepository.findOne(postId, { where: { isDeleted: false } });

    if (post === undefined || post === null) {
      throw new ForumSystemError(`Post with ID: ${postId} does not exist!`, 404);
    }

    return post;
  }
}
