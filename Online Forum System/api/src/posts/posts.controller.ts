import { Controller, Body, Query, Get, Param, Post, Put, Delete, ParseIntPipe, UseGuards } from '@nestjs/common';
import { PostsService } from './posts.service';
import { QueryPostDTO } from './models/query-post.dto';
import { ReturnPostDTO } from './models/return-post.dto';
import { CreatePostDTO } from './models/create-post.dto';
import { UpdatePostDTO } from './models/update-post.dto';
import { ResponseMessageDTO } from '../common/models/response-message.dto';
import { AuthenticationGuard } from '../common/guards/auth.guard';
import { User } from '../common/decorators/user.decorator';
import { ReturnUserDTO } from '../users/models/return-user.dto';
import { BanGuard } from '../common/guards/ban.guard';

@Controller('posts')
@UseGuards(AuthenticationGuard)
export class PostsController {
  public constructor(private readonly postsService: PostsService) {}

  @Get()
  public async getPosts(@Query() query: QueryPostDTO): Promise<ReturnPostDTO[]> {
    return await this.postsService.getPosts(query);
  }

  @Get(':postId')
  public async getPostById(@Param('postId', ParseIntPipe) id: number): Promise<ReturnPostDTO> {
    return await this.postsService.getPostById(id);
  }

  @Post()
  @UseGuards(BanGuard)
  public async createPost(@Body() body: CreatePostDTO, @User() user: ReturnUserDTO): Promise<ReturnPostDTO> {
    return await this.postsService.createPost(body, user);
  }

  @Put(':postId')
  @UseGuards(BanGuard)
  public async updatePost(
    @Param('postId', ParseIntPipe) id: number,
    @Body() body: UpdatePostDTO,
    @User() user: ReturnUserDTO,
  ): Promise<ReturnPostDTO> {
    return await this.postsService.updatePost(id, body, user);
  }

  @Delete(':postId')
  @UseGuards(BanGuard)
  public async deletePost(
    @Param('postId', ParseIntPipe) id: number,
    @User() user: ReturnUserDTO,
  ): Promise<ResponseMessageDTO> {
    return await this.postsService.deletePost(id, user);
  }

  @Post(':postId')
  @UseGuards(BanGuard)
  public async flagPost(@Param('postId', ParseIntPipe) id: number): Promise<ReturnPostDTO> {
    return await this.postsService.flagPost(id);
  }

  @Post(':postId/votes')
  @UseGuards(BanGuard)
  public async likePost(
    @Param('postId', ParseIntPipe) id: number,
    @User() user: ReturnUserDTO,
  ): Promise<ReturnPostDTO> {
    return await this.postsService.likePost(id, user);
  }

  @Delete(':postId/votes')
  @UseGuards(BanGuard)
  public async unlikePost(
    @Param('postId', ParseIntPipe) id: number,
    @User() user: ReturnUserDTO,
  ): Promise<ReturnPostDTO> {
    return await this.postsService.unlikePost(id, user);
  }
}
