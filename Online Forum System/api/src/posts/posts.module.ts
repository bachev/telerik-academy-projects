import { Module } from '@nestjs/common';
import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Post } from '../database/entities/post.entity';
import { User } from '../database/entities/user.entity';
import { Vote } from '../database/entities/vote.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Post, User, Vote])],
  controllers: [PostsController],
  providers: [PostsService],
})
export class PostsModule {}
