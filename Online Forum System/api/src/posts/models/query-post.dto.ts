import { IsOptional } from 'class-validator';

export class QueryPostDTO {
    @IsOptional()
    public title?: string;

    @IsOptional()
    public content?: string;
}