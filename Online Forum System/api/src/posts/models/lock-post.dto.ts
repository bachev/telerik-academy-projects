import { IsBoolean } from 'class-validator';

export class LockPostDTO {
    @IsBoolean()
    public isLocked: boolean;
}