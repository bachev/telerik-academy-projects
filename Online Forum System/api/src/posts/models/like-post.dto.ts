import { IsBoolean } from 'class-validator';

export class LikePostDTO {
    @IsBoolean()
    public isLiked: boolean;
}