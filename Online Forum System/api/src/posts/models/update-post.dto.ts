import { IsOptional, Length, MinLength } from 'class-validator';

export class UpdatePostDTO {
    @IsOptional()
    @Length(2, 50)
    public title?: string;

    @IsOptional()
    @MinLength(10)
    public content?: string;
}