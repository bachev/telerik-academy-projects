import { Length, MinLength } from 'class-validator';

export class CreatePostDTO {
    @Length(2, 50)
    public title: string;

    @MinLength(10)
    public content: string;
}