import { Expose } from 'class-transformer';

export class ReturnPostDTO {
    @Expose()
    public title: string;

    @Expose()
    public id: number;

    @Expose()
    public content: string;

    @Expose()
    public addedOn: string;

    @Expose()
    public likesCount: number;

    @Expose()
    public dislikesCount: number;

    @Expose()
    public authorId: number;

    @Expose()
    public isFlagged: boolean;

    @Expose()
    public isLocked: boolean;
}