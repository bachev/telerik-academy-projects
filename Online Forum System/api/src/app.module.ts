import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { PostsModule } from './posts/posts.module';
import { CommentsModule } from './comments/comments.module';
import { DatabaseModule } from './database/database.module';
import { AuthModule } from './auth/auth.module';
import { CoreModule } from './common/core.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [UsersModule, PostsModule, DatabaseModule, CommentsModule, UsersModule, AuthModule, CoreModule, ScheduleModule.forRoot()],
  controllers: [],
  providers: [],
})
export class AppModule {}
