import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { Vote } from '../database/entities/vote.entity';
import { Comment } from '../database/entities/comment.entity';
import { Post } from '../database/entities/post.entity';
import { CommentsService } from './comments.service';
import { QueryCommentDTO } from './models/query-comment.dts';
import { ReturnCommentDTO } from './models/return-comment.dto';
import { CreateCommentDTO } from './models/create-comment.dto';
import { ReturnUserDTO } from '../users/models/return-user.dto';
import { UserRole } from '../users/enums/user-role.enum';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { UpdateCommentDTO } from './models/update-comment.dto';
describe('CommentsService should', () => {
  let service: CommentsService;
  let postRepository: any;
  let commentRepository: any;
  let userRepository: any;
  let voteRepository: any;
  let fakeCommentEntity: Comment;
  let fakePostEntity: Post;
  let fakeUserEntity: User;
  beforeEach(async () => {
    postRepository = {
      find: jest.fn(),
      findOne: jest.fn(),
      findOneOrFail: jest.fn(),
      create: jest.fn(),
      save: jest.fn(),
    };

    commentRepository = {
      find: jest.fn(),
      findOne: jest.fn(),
      findOneOrFail: jest.fn(),
      create: jest.fn(),
      save: jest.fn(),
    };

    userRepository = {
      find: jest.fn(),
      findOne: jest.fn(),
      findOneOrFail: jest.fn(),
      create: jest.fn(),
      save: jest.fn(),
    };

    voteRepository = {
      find: jest.fn(),
      findOne: jest.fn(),
      findOneOrFail: jest.fn(),
      create: jest.fn(),
      save: jest.fn(),
    };

    fakeUserEntity = {
      id: 1,
      joinedOn: 'date',
      username: 'username',
      email: 'email',
      password: 'password',
      role: UserRole.Basic,
      avatar: 'avatar',
      isDeleted: false,
      posts: Promise.resolve([fakePostEntity]),
      votes: null,
      comments: Promise.resolve([fakeCommentEntity]),
      banstatus: null,
      friends_requests: null,
      friends_received: null,
    };

    fakeCommentEntity = {
      id: 1,
      addedOn: 'date',
      title: 'title',
      content: 'content',
      isDeleted: false,
      votes: null,
      user: Promise.resolve(fakeUserEntity),
      post: Promise.resolve(fakePostEntity),
    };

    fakePostEntity = {
      id: 1,
      addedOn: 'date',
      title: 'title',
      content: 'content',
      isFlagged: false,
      isLocked: false,
      isDeleted: false,
      user: Promise.resolve(fakeUserEntity),
      comments: Promise.resolve([fakeCommentEntity]),
      votes: null,
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CommentsService,
        { provide: getRepositoryToken(Post), useValue: postRepository },
        { provide: getRepositoryToken(Comment), useValue: commentRepository },
        { provide: getRepositoryToken(User), useValue: userRepository },
        { provide: getRepositoryToken(Vote), useValue: voteRepository },
      ],
    }).compile();

    service = module.get<CommentsService>(CommentsService);

    jest.clearAllMocks();
  });

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(service).toBeDefined();
  });

  describe('getPostComments() should', () => {
    it('call postsRepository findOneOrFail with the correct value', async () => {
      const fakePostId = 1;
      const spy = jest.spyOn(postRepository, 'findOneOrFail').mockReturnValue(fakePostEntity);
      const expectedFilteringObject = {
        where: { id: fakePostId, isDeleted: false },
      };

      await service.getPostComments(fakePostId);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(expectedFilteringObject);
    });
    it('return comments as ReturnCommentDTO objects', async () => {
      const fakePostId = 1;
      const spy = jest.spyOn(postRepository, 'findOneOrFail').mockReturnValue(fakePostEntity);

      const expectedResult: ReturnCommentDTO[] = [
        {
          id: 1,
          addedOn: 'date',
          title: 'title',
          content: 'content',
        },
      ];

      const actualResult: ReturnCommentDTO[] = await service.getPostComments(fakePostId);

      expect(actualResult).toEqual(expectedResult);
    });
    it(`return an empty array if search query doesn't return results`, async () => {
      const fakePostId = 1;
      const spy = jest.spyOn(postRepository, 'findOneOrFail').mockReturnValue(fakePostEntity);
      const mockQuery: QueryCommentDTO = {
        content: 'does not exist',
      };

      const actualResult = await service.getPostComments(fakePostId, mockQuery);
      const expectedResult = [];

      expect(actualResult).toEqual(expectedResult);
    });
  });
  describe('getUserComments() should', () => {
    it('call userRepository findOneOrFail with the correct value', async () => {
      const fakeUserId = 1;
      const expectedFilteringObject = {
        where: { id: fakeUserId, isDeleted: false },
      };
      const spy = jest
        .spyOn(userRepository, 'findOneOrFail')
        .mockReturnValue({ id: fakeUserId, comments: Promise.resolve(fakeCommentEntity) });

      await service.getUserComments(fakeUserId);

      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(expectedFilteringObject);
    });
    it('return comments as ReturnCommentDTO objects', async () => {
      const fakeUserId = 1;
      const spy = jest
        .spyOn(userRepository, 'findOneOrFail')
        .mockReturnValue({ id: fakeUserId, comments: Promise.resolve(fakeCommentEntity) });

      const expectedResult: ReturnCommentDTO = {
        id: 1,
        addedOn: 'date',
        title: 'title',
        content: 'content',
      };

      const actualResult = await service.getUserComments(fakeUserId);

      expect(actualResult).toEqual(expectedResult);
    });
    it('return an empty array if the user has no comments', async () => {
      const fakeUserId = 1;
      const spy = jest
        .spyOn(userRepository, 'findOneOrFail')
        .mockReturnValue({ id: fakeUserId, comments: Promise.resolve([]) });

      const emptyCommentArray: ReturnCommentDTO[] = [];

      const actualResult = await service.getUserComments(fakeUserId);

      expect(actualResult).toEqual(emptyCommentArray);
    });
  });
  describe('createComment() should', () => {
    it('call the commentRepository save() method once', async () => {
      const fakePostId = 1;
      const fakeComment: CreateCommentDTO = {
        content: 'contest',
        title: 'title',
      };
      const fakeReturnUserDTO = {
        username: 'username',
        id: 1,
        email: 'email',
        role: UserRole.Basic,
        joinedOn: 'date',
        avatar: 'avatar',
      };

      const spy = jest.spyOn(commentRepository, 'save').mockReturnValue(fakeComment);
      jest.spyOn(userRepository, 'findOneOrFail').mockReturnValue(new User());
      jest.spyOn(postRepository, 'findOneOrFail').mockReturnValue(new Post());
      jest.spyOn(commentRepository, 'create').mockReturnValue(fakeComment);

      await service.createComment(fakePostId, fakeComment, fakeReturnUserDTO);

      expect(spy).toBeCalledTimes(1);
    });
    it('call the userRepository findOneOrFail() method once', async () => {
      const fakePostId = 1;
      const fakeComment: CreateCommentDTO = {
        content: 'contest',
        title: 'title',
      };
      const fakeReturnUserDTO = {
        username: 'username',
        id: 1,
        email: 'email',
        role: UserRole.Basic,
        joinedOn: 'date',
        avatar: 'avatar',
      };

      const spy = jest.spyOn(userRepository, 'findOneOrFail').mockReturnValue(new User());
      jest.spyOn(postRepository, 'findOneOrFail').mockReturnValue(new Post());
      jest.spyOn(commentRepository, 'create').mockReturnValue(fakeComment);
      jest.spyOn(commentRepository, 'save').mockReturnValue(fakeComment);

      await service.createComment(fakePostId, fakeComment, fakeReturnUserDTO);

      expect(spy).toBeCalledTimes(1);
    });
    it('call the postRepository findOneOrFail() method once', async () => {
      const fakePostId = 1;
      const fakeComment: CreateCommentDTO = {
        content: 'contest',
        title: 'title',
      };
      const fakeReturnUserDTO = {
        username: 'username',
        id: 1,
        email: 'email',
        role: UserRole.Basic,
        joinedOn: 'date',
        avatar: 'avatar',
      };

      const spy = jest.spyOn(postRepository, 'findOneOrFail').mockReturnValue(new Post());
      jest.spyOn(userRepository, 'findOneOrFail').mockReturnValue(new User());
      jest.spyOn(commentRepository, 'create').mockReturnValue(fakeComment);
      jest.spyOn(commentRepository, 'save').mockReturnValue(fakeComment);

      await service.createComment(fakePostId, fakeComment, fakeReturnUserDTO);

      expect(spy).toBeCalledTimes(1);
    });
    it("throw an error if the post doesn't exist", async () => {
      let expectedError;
      const fakePostId = 1;
      const fakeComment: CreateCommentDTO = {
        content: 'contest',
        title: 'title',
      };
      const fakeReturnUserDTO = {
        username: 'username',
        id: 1,
        email: 'email',
        role: UserRole.Basic,
        joinedOn: 'date',
        avatar: 'avatar',
      };

      jest.spyOn(postRepository, 'findOneOrFail').mockReturnValue(undefined);
      jest.spyOn(userRepository, 'findOneOrFail').mockReturnValue(fakeUserEntity);
      jest.spyOn(commentRepository, 'create').mockReturnValue(fakeCommentEntity);
      jest.spyOn(commentRepository, 'save').mockReturnValue(fakeCommentEntity);

      try {
        await service.createComment(fakePostId, fakeComment, fakeReturnUserDTO);
      } catch (error) {
        expectedError = error;
      } finally {
        expect(expectedError).toBeDefined();
      }
    });
  });
  describe('updateComment() should', () => {
    it('call the findCommentByIdAndPostId() method once', async () => {
      const fakePostId = 1;
      const fakeCommentId = 1;
      const fakeComment: UpdateCommentDTO = {
        content: 'contest',
        title: 'title',
      };
      const fakeReturnUserDTO = {
        username: 'username',
        id: 1,
        email: 'email',
        role: UserRole.Basic,
        joinedOn: 'date',
        avatar: 'avatar',
      };

      const spy = jest.spyOn(service as any, 'findCommentByIdAndPostId').mockReturnValue(fakeCommentEntity);
      jest.spyOn(postRepository, 'findOneOrFail').mockReturnValue(fakePostEntity);
      jest.spyOn(userRepository, 'findOneOrFail').mockReturnValue(fakeUserEntity);
      jest.spyOn(commentRepository, 'save').mockReturnValue(fakeComment);

      await service.updateComment(fakePostId, fakeCommentId, fakeComment, fakeReturnUserDTO);

      expect(spy).toBeCalledTimes(1);
    });
    it(`call the commentRepository save() method once`, async () => {
      const fakePostId = 1;
      const fakeCommentId = 1;
      const fakeComment: UpdateCommentDTO = {
        content: 'contest',
        title: 'title',
      };
      const fakeReturnUserDTO = {
        username: 'username',
        id: 1,
        email: 'email',
        role: UserRole.Basic,
        joinedOn: 'date',
        avatar: 'avatar',
      };

      const spy = jest.spyOn(commentRepository, 'save').mockReturnValue(fakeComment);
      jest.spyOn(postRepository, 'findOneOrFail').mockReturnValue(fakePostEntity);
      jest.spyOn(userRepository, 'findOneOrFail').mockReturnValue(fakeUserEntity);
      jest.spyOn(service as any, 'findCommentByIdAndPostId').mockReturnValue(fakeCommentEntity);

      await service.updateComment(fakePostId, fakeCommentId, fakeComment, fakeReturnUserDTO);

      expect(spy).toBeCalledTimes(1);
    });
    it(`throw an error if the user isn't authorized to update this comment`, async () => {
      let expectedError;
      const fakePostId = 1;
      const fakeCommentId = 1;
      const fakeUserId = 99;
      const fakeComment: UpdateCommentDTO = {
        content: 'contest',
        title: 'title',
      };
      const fakeReturnUserDTO = {
        username: 'username',
        id: fakeUserId,
        email: 'email',
        role: UserRole.Basic,
        joinedOn: 'date',
        avatar: 'avatar',
      };

      jest.spyOn(commentRepository, 'save').mockReturnValue(fakeComment);
      jest.spyOn(postRepository, 'findOneOrFail').mockReturnValue(fakePostEntity);
      jest.spyOn(userRepository, 'findOneOrFail').mockReturnValue(fakeUserEntity);
      jest.spyOn(service as any, 'findCommentByIdAndPostId').mockReturnValue(fakeCommentEntity);

      try {
        await service.updateComment(fakePostId, fakeCommentId, fakeComment, fakeReturnUserDTO);
      } catch (error) {
        expectedError = error;
      } finally {
        expect(expectedError).toBeDefined();
      }
    });
  });
  describe('deleteComment() should', () => {
    it('call the commentRepository save() method once', async () => {
      const fakePostId = 1;
      const fakeCommentId = 1;
      const fakeComment: UpdateCommentDTO = {
        content: 'contest',
        title: 'title',
      };
      const fakeReturnUserDTO = {
        username: 'username',
        id: 1,
        email: 'email',
        role: UserRole.Basic,
        joinedOn: 'date',
        avatar: 'avatar',
      };

      const spy = jest.spyOn(commentRepository, 'save').mockReturnValue(fakeComment);
      jest.spyOn(service as any, 'findCommentByIdAndPostId').mockReturnValue(fakeCommentEntity);

      await service.deleteComment(fakePostId, fakeCommentId, fakeReturnUserDTO);

      expect(spy).toBeCalledTimes(1);
    });
    it(`throw an error if the comment doesn't exist`, async () => {
      let expectedError;
      const fakePostId = 1;
      const fakeCommentId = 99;
      const fakeComment: UpdateCommentDTO = {
        content: 'contest',
        title: 'title',
      };
      const fakeReturnUserDTO = {
        username: 'username',
        id: 1,
        email: 'email',
        role: UserRole.Basic,
        joinedOn: 'date',
        avatar: 'avatar',
      };

      jest.spyOn(commentRepository, 'save').mockReturnValue(fakeComment);
      try {
        await service.deleteComment(fakePostId, fakeCommentId, fakeReturnUserDTO);
      } catch (error) {
        expectedError = error;
      } finally {
        expect(expectedError).toBeDefined();
      }
    });
    it(`throw an error if the user isn't authorized to delete this comment`, async () => {
      let expectedError;
      const fakePostId = 1;
      const fakeCommentId = 1;
      const fakeUserId = 99;
      const fakeComment: UpdateCommentDTO = {
        content: 'contest',
        title: 'title',
      };
      const fakeReturnUserDTO = {
        username: 'username',
        id: fakeUserId,
        email: 'email',
        role: UserRole.Basic,
        joinedOn: 'date',
        avatar: 'avatar',
      };
      jest.spyOn(commentRepository, 'save').mockReturnValue(fakeComment);
      jest.spyOn(service as any, 'findCommentByIdAndPostId').mockReturnValue(fakeCommentEntity);

      try {
        await service.deleteComment(fakePostId, fakeCommentId, fakeReturnUserDTO);
      } catch (error) {
        expectedError = error;
      } finally {
        expect(expectedError).toBeDefined();
      }
    });
  });
});
