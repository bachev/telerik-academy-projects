import { CommentsController } from './comments.controller';
import { TestingModule, Test } from '@nestjs/testing';
import { AuthService } from '../auth/auth.service';
import { CommentsService } from './comments.service';
import { QueryCommentDTO } from './models/query-comment.dts';
import { ReturnUserDTO } from '../users/models/return-user.dto';
import { CreateCommentDTO } from './models/create-comment.dto';
import { UpdateCommentDTO } from './models/update-comment.dto';

describe('CommentsController', () => {
  let controller: CommentsController;
  let commentsService: any;
  let authService: any;

  beforeEach(async () => {
    authService = {
      isTokenBlacklisted: jest.fn(),
      isUserBanned: jest.fn(),
    };

    commentsService = {
      getPostComments: jest.fn(),
      getUserComments: jest.fn(),
      createComment: jest.fn(),
      updateComment: jest.fn(),
      deleteComment: jest.fn(),
      likeComment: jest.fn(),
      unlikeComment: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [CommentsController],
      providers: [
        { provide: CommentsService, useValue: commentsService },
        { provide: AuthService, useValue: authService },
      ],
    }).compile();

    controller = module.get<CommentsController>(CommentsController);

    jest.clearAllMocks();
  });

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(controller).toBeDefined();
  });

  describe('getPostComments() should', () => {
    it('call commentsService getPostComments() once with the passed parameters', async () => {
      // Arrange
      const fakePostId = 1;
      const spy = jest.spyOn(commentsService, 'getPostComments');

      // Act
      await controller.getPostComments(fakePostId);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakePostId, undefined);
    });

    it('return the result from commentsService getPostComments()', async () => {
      // Arrange
      const fakePostId = 1;
      const fakeQuery = new QueryCommentDTO();
      const controlResult = { title: 'comment' };

      jest.spyOn(commentsService, 'getPostComments').mockReturnValue(Promise.resolve(controlResult));

      // Act
      const actualResult = await controller.getPostComments(fakePostId, fakeQuery);

      // Assert
      expect(actualResult).toEqual(controlResult);
    });
  });

  describe('getUserComments() should', () => {
    it('call commentsService getUserComments() once with the passed id', async () => {
      // Arrange
      const fakeId = 1;
      const spy = jest.spyOn(commentsService, 'getUserComments');

      // Act
      await controller.getUserComments(fakeId);

      //Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakeId);
    });

    it('return the result from commentsService getUserComments()', async () => {
      // Arrange
      const fakeId = 1;
      const controlResult = [];

      jest.spyOn(commentsService, 'getUserComments').mockReturnValue(Promise.resolve(controlResult));

      // Act
      const actualResult = await controller.getUserComments(fakeId);

      // Assert
      expect(actualResult).toEqual(controlResult);
    });
  });

  describe('createComment() should', () => {
    it('call commentsService createComment() once with the passed parameters', async () => {
      // Arrange
      const fakeCreateCommentDTO = { title: 'title', content: 'content' };
      const fakeUser = new ReturnUserDTO();
      const fakePostId = 1;
      const spy = jest.spyOn(commentsService, 'createComment');

      // Act
      await controller.createComment(fakePostId, fakeCreateCommentDTO, fakeUser);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakePostId, fakeCreateCommentDTO, fakeUser);
    });

    it('return the result from commentsService createComment()', async () => {
      // Arrange
      const fakePostId = 1;
      const fakeCreateCommentDTO = new CreateCommentDTO();
      const fakeUser = new ReturnUserDTO();
      const control = { title: 'testing' };
      const expected = { msg: 'Comment created', comment: control };

      jest.spyOn(commentsService, 'createComment').mockReturnValue(Promise.resolve(control));

      // Act
      const actualResult = await controller.createComment(fakePostId, fakeCreateCommentDTO, fakeUser);
      // Assert
      expect(actualResult).toEqual(expected);
    });
  });

  describe('updateComment() should', () => {
    it('call commentsService updateComment() once with the passed parameters', async () => {
      // Arrange
      const fakePostId = 1;
      const fakeCommentId = 1;
      const fakeUpdateCommentDTO = new UpdateCommentDTO();
      const fakeUser = new ReturnUserDTO();
      const spy = jest.spyOn(commentsService, 'updateComment');

      // Act
      await controller.updateComment(fakePostId, fakeCommentId, fakeUpdateCommentDTO, fakeUser);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakePostId, fakeCommentId, fakeUpdateCommentDTO, fakeUser);
    });

    it('return the result from commentsService createComment()', async () => {
      // Arrange
      const fakePostId = 1;
      const fakeCommentId = 1;
      const fakeUpdateCommentDTO = new UpdateCommentDTO();
      const fakeUser = new ReturnUserDTO();
      const control = { title: 'testing' };
      const expected = { msg: 'Comment updated', comment: control };

      jest.spyOn(commentsService, 'updateComment').mockReturnValue(Promise.resolve(control));

      // Act
      const actualResult = await controller.updateComment(fakePostId, fakeCommentId, fakeUpdateCommentDTO, fakeUser);
      // Assert
      expect(actualResult).toEqual(expected);
    });
  });

  describe('deleteComment() should', () => {
    it('call commentsService deleteComment() once with the passed parameters', async () => {
      // Arrange
      const fakePostId = 1;
      const fakeCommentId = 1;
      const fakeUser = new ReturnUserDTO();
      const spy = jest.spyOn(commentsService, 'deleteComment');

      // Act
      await controller.deleteComment(fakePostId, fakeCommentId, fakeUser);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakePostId, fakeCommentId, fakeUser);
    });

    it('return the result from commentsService createComment()', async () => {
      // Arrange
      const fakePostId = 1;
      const fakeCommentId = 1;
      const fakeUser = new ReturnUserDTO();
      const control = { title: 'testing' };
      const expected = { msg: 'Comment deleted', comment: control };

      jest.spyOn(commentsService, 'deleteComment').mockReturnValue(Promise.resolve(control));

      // Act
      const actualResult = await controller.deleteComment(fakePostId, fakeCommentId, fakeUser);
      // Assert
      expect(actualResult).toEqual(expected);
    });
  });

  describe('likeComment() should', () => {
    it('call commentsService likeComment() once with the passed parameters', async () => {
      // Arrange
      const fakePostId = 1;
      const fakeCommentId = 1;
      const fakeUser = new ReturnUserDTO();
      const spy = jest.spyOn(commentsService, 'likeComment');

      // Act
      await controller.likeComment(fakePostId, fakeCommentId, fakeUser);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakePostId, fakeCommentId, fakeUser);
    });

    it('return the result from commentsService likeComment()', async () => {
      // Arrange
      const fakePostId = 1;
      const fakeCommentId = 1;
      const fakeUser = new ReturnUserDTO();
      const controlResult = { title: 'comment' };

      jest.spyOn(commentsService, 'likeComment').mockReturnValue(Promise.resolve(controlResult));

      // Act
      const actualResult = await controller.likeComment(fakePostId, fakeCommentId, fakeUser);

      // Assert
      expect(actualResult).toEqual(controlResult);
    });
  });

  describe('unlikeComment() should', () => {
    it('call commentsService unlikeComment() once with the passed parameters', async () => {
      // Arrange
      const fakePostId = 1;
      const fakeCommentId = 1;
      const fakeUser = new ReturnUserDTO();
      const spy = jest.spyOn(commentsService, 'unlikeComment');

      // Act
      await controller.unlikeComment(fakePostId, fakeCommentId, fakeUser);

      // Assert
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith(fakePostId, fakeCommentId, fakeUser);
    });

    it('return the result from commentsService unlikeComment()', async () => {
      // Arrange
      const fakePostId = 1;
      const fakeCommentId = 1;
      const fakeUser = new ReturnUserDTO();
      const controlResult = { title: 'comment' };

      jest.spyOn(commentsService, 'unlikeComment').mockReturnValue(Promise.resolve(controlResult));

      // Act
      const actualResult = await controller.unlikeComment(fakePostId, fakeCommentId, fakeUser);

      // Assert
      expect(actualResult).toEqual(controlResult);
    });
  });
});
