import { IsBoolean } from 'class-validator';

export class LikeCommentDTO {
    @IsBoolean()
    public isLiked: boolean;
}