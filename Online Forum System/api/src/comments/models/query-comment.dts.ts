import { IsOptional } from 'class-validator';

export class QueryCommentDTO {
  @IsOptional()
  public title?: string;

  @IsOptional()
  public content?: string;

  @IsOptional()
  public username?: string;
}
