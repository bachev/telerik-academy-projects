import { Length, IsString } from 'class-validator';

export class CreateCommentDTO {
  @Length(2, 40)
  public title: string;
  @Length(2, 5000)
  public content: string;
}
