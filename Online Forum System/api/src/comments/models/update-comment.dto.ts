import { Length, IsString } from 'class-validator';

export class UpdateCommentDTO {
  @IsString()
  @Length(2, 40)
  public title: string;
  @IsString()
  @Length(2, 5000)
  public content: string;
}
