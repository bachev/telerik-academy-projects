import { Expose } from 'class-transformer';

export class ReturnCommentDTO {
  @Expose()
  public id: number;
  @Expose()
  public postId: number;
  @Expose()
  public userId: number;
  @Expose()
  public addedOn: string;
  @Expose()
  public title: string;
  @Expose()
  public content: string;
}
