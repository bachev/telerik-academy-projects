import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { Comment } from '../database/entities/comment.entity';
import { Post } from '../database/entities/post.entity';
import { User } from '../database/entities/user.entity';
import { Vote } from '../database/entities/vote.entity';
import { CreateCommentDTO } from './models/create-comment.dto';
import { LikeCommentDTO } from './models/like-comment.dto';
import { ReturnCommentDTO } from './models/return-comment.dto';
import { UpdateCommentDTO } from './models/update-comment.dto';
import { ReturnUserDTO } from '../users/models/return-user.dto';
import { UserRole } from '../users/enums/user-role.enum';
import { QueryCommentDTO } from './models/query-comment.dts';

@Injectable()
export class CommentsService {
  public constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Post) private readonly postRepository: Repository<Post>,
    @InjectRepository(Comment) private readonly commentRepository: Repository<Comment>,
    @InjectRepository(Vote) private readonly voteRepository: Repository<Vote>,
  ) {}

  public async getAllComments(): Promise<ReturnCommentDTO[]> {
    const comments = await this.commentRepository.find();
    return comments;
  }

  public async getPostComments(postId: number, query?: QueryCommentDTO): Promise<ReturnCommentDTO[]> {
    let comments: Comment[] = await this.commentRepository.find({ where: { postId, isDeleted: false } });

    if (query?.title) {
      comments = comments.filter(post => post.title.toLowerCase().includes(query.title.toLowerCase()));
    }
    if (query?.content) {
      comments = comments.filter(post => post.content.toLowerCase().includes(query.content.toLowerCase()));
    }
    if (query?.username) {
      comments = comments.filter(async post =>
        (await post.user).username.toLowerCase().includes(query.username.toLowerCase()),
      );
    }

    return plainToClass(ReturnCommentDTO, comments, { excludeExtraneousValues: true });
  }

  public async getUserComments(userId: number): Promise<ReturnCommentDTO[]> {
    const user = await this.userRepository.findOneOrFail({ where: { id: userId, isDeleted: false } });
    const userComments = await user.comments;
    return plainToClass(ReturnCommentDTO, userComments, { excludeExtraneousValues: true });
  }

  public async createComment(
    postId: number,
    comment: CreateCommentDTO,
    user: ReturnUserDTO,
  ): Promise<ReturnCommentDTO> {
    const newComment = this.commentRepository.create(comment);
    newComment.post = Promise.resolve(await this.postRepository.findOneOrFail({ id: postId }));
    newComment.user = Promise.resolve(await this.userRepository.findOneOrFail({ id: user.id }));
    if ((await newComment.post) === undefined || (await newComment.post) === null) {
      throw new ForumSystemError(`Post with ID ${postId} not found!`, 404);
    }
    if ((await newComment.user) === undefined || (await newComment.user) === null) {
      throw new ForumSystemError(`User with ID ${user.id} not found!`, 404);
    }
    if ((await newComment.post).isLocked && (await newComment.user).role !== UserRole.Admin) {
      throw new ForumSystemError('This post is locked!', 403);
    }
    const savedComment: Comment = await this.commentRepository.save(newComment);
    return plainToClass(ReturnCommentDTO, savedComment, { excludeExtraneousValues: true });
  }

  public async updateComment(
    postId: number,
    commentId: number,
    comment: Partial<UpdateCommentDTO>,
    user: ReturnUserDTO,
  ) {
    const foundComment = await this.findCommentByIdAndPostId(postId, commentId);
    if ((await foundComment.user).id !== user.id && user.role !== UserRole.Admin) {
      throw new ForumSystemError('You are not authorized to edit this comment!', 401);
    }
    if (comment.title) {
      foundComment.title = comment.title;
    }
    if (comment.content) {
      foundComment.content = comment.content;
    }
    const updatedComment = await this.commentRepository.save(foundComment);
    return plainToClass(ReturnCommentDTO, updatedComment, { excludeExtraneousValues: true });
  }

  public async deleteComment(postId: number, commentId: number, user: ReturnUserDTO): Promise<ReturnCommentDTO> {
    const foundComment = await this.findCommentByIdAndPostId(postId, commentId);
    if ((await foundComment.user).id !== user.id && user.role !== UserRole.Admin) {
      throw new ForumSystemError('You are not authorized to delete this comment!', 401);
    }
    foundComment.isDeleted = true;
    const deletedComment = await this.commentRepository.save(foundComment);
    return plainToClass(ReturnCommentDTO, deletedComment, { excludeExtraneousValues: true });
  }

  public async unlikeComment(postId: number, commentId: number, user: ReturnUserDTO): Promise<ReturnCommentDTO> {
    const foundComment: Comment = await this.findCommentByIdAndPostId(postId, commentId);
    const foundUser: User = await this.userRepository.findOneOrFail({ where: { id: user.id, isDeleted: false } });
    const foundVote: Vote = await this.voteRepository.findOneOrFail({
      where: { comment: foundComment, user: foundUser },
    });
    await this.voteRepository.delete(foundVote);

    return plainToClass(ReturnCommentDTO, foundComment, { excludeExtraneousValues: true });
  }

  public async likeComment(postId: number, commentId: number, user: ReturnUserDTO): Promise<ReturnCommentDTO> {
    const foundComment: Comment = await this.findCommentByIdAndPostId(postId, commentId);
    const foundUser: User = await this.userRepository.findOneOrFail({ where: { id: user.id, isDeleted: false } });
    const foundVote: Vote = await this.voteRepository.findOne({ where: { comment: foundComment, user: foundUser } });

    if (foundVote === undefined) {
      const newVote: Vote = this.voteRepository.create({ isLiked: true });
      newVote.user = Promise.resolve(foundUser);
      newVote.comment = Promise.resolve(foundComment);

      await this.voteRepository.save(newVote);
    } else {
      foundVote.isLiked = true;

      await this.voteRepository.save(foundVote);
    }

    return plainToClass(ReturnCommentDTO, foundComment, { excludeExtraneousValues: true });
  }

  public async getCommentLikes(commentId: number): Promise<ReturnUserDTO[]> {
    const comment: Comment = await this.commentRepository.findOneOrFail({ where: { id: commentId, isDeleted: false } });
    const votes: Vote[] = await comment.votes;
    const users = await Promise.all(votes.map(vote => vote.user));
    return plainToClass(ReturnUserDTO, users, { excludeExtraneousValues: true });
  }

  private async findCommentById(commentId: number): Promise<Comment> {
    const comment: Comment = await this.commentRepository.findOneOrFail({ where: { id: commentId, isDeleted: false } });

    if (comment === undefined) {
      throw new ForumSystemError(`Comment with ID: ${commentId} does not exist`, 404);
    }

    if (comment.isDeleted) {
      throw new ForumSystemError(`Comment with ID: ${commentId} has already been deleted`, 410);
    }
    return comment;
  }
  private async findCommentByIdAndPostId(postId: number, commentId: number) {
    const comment = await this.findCommentById(commentId);
    if (comment.postId !== postId) {
      throw new ForumSystemError(`Comment with ID: ${commentId} doesn't belong to post with ID: ${postId}`, 400);
    }
    return comment;
  }
}
