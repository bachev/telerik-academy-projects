import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Comment } from '../database/entities/comment.entity';
import { Post } from '../database/entities/post.entity';
import { User } from '../database/entities/user.entity';
import { Vote } from '../database/entities/vote.entity';
import { CommentsController } from './comments.controller';
import { CommentsService } from './comments.service';

@Module({
  imports: [TypeOrmModule.forFeature([Post, User, Comment, Vote])],
  controllers: [CommentsController],
  providers: [CommentsService],
})
export class CommentsModule {}
