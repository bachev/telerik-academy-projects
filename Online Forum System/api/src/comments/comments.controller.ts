import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Put,
  UseGuards,
  ValidationPipe,
  Query,
} from '@nestjs/common';
import { User } from '../common/decorators/user.decorator';
import { AuthenticationGuard } from '../common/guards/auth.guard';
import { BanGuard } from '../common/guards/ban.guard';
import { ReturnUserDTO } from '../users/models/return-user.dto';
import { CommentsService } from './comments.service';
import { CreateCommentDTO } from './models/create-comment.dto';
import { ReturnCommentDTO } from './models/return-comment.dto';
import { UpdateCommentDTO } from './models/update-comment.dto';
import { QueryCommentDTO } from './models/query-comment.dts';

@Controller()
export class CommentsController {
  constructor(private readonly commentsService: CommentsService) {}

  @Get('comments/all')
  @UseGuards(AuthenticationGuard)
  @HttpCode(HttpStatus.OK)
  public async getAllComments(): Promise<ReturnCommentDTO[]> {
    return await this.commentsService.getAllComments();
  }

  @Get('posts/:postId/comments')
  @UseGuards(AuthenticationGuard)
  @HttpCode(HttpStatus.OK)
  public async getPostComments(
    @Param('postId', ParseIntPipe) postId: number,
    @Query(ValidationPipe) queryCommentDTO?: QueryCommentDTO,
  ): Promise<ReturnCommentDTO[]> {
    return await this.commentsService.getPostComments(postId, queryCommentDTO);
  }

  @Get('users/:userId/comments')
  @UseGuards(AuthenticationGuard)
  @HttpCode(HttpStatus.OK)
  public async getUserComments(@Param('userId', ParseIntPipe) id: number): Promise<ReturnCommentDTO[]> {
    return await this.commentsService.getUserComments(id);
  }

  @Post('posts/:postId/comments')
  @UseGuards(AuthenticationGuard, BanGuard)
  @HttpCode(HttpStatus.CREATED)
  public async createComment(
    @Param('postId', ParseIntPipe) postId: number,
    @Body(ValidationPipe) body: CreateCommentDTO,
    @User(ValidationPipe) user: ReturnUserDTO,
  ): Promise<any> {
    const result = await this.commentsService.createComment(postId, body, user);
    return { msg: 'Comment created', comment: result };
  }

  @Put('posts/:postId/comments/:commentId')
  @UseGuards(AuthenticationGuard, BanGuard)
  @HttpCode(HttpStatus.OK)
  public async updateComment(
    @Param('postId', ParseIntPipe) postId: number,
    @Param('commentId', ParseIntPipe) commentId: number,
    @Body(ValidationPipe) body: Partial<UpdateCommentDTO>,
    @User(ValidationPipe) user: ReturnUserDTO,
  ): Promise<any> {
    const result = await this.commentsService.updateComment(postId, commentId, body, user);
    return { msg: 'Comment updated', comment: result };
  }

  @Delete('posts/:postId/comments/:commentId')
  @UseGuards(AuthenticationGuard, BanGuard)
  @HttpCode(HttpStatus.OK)
  public async deleteComment(
    @Param('postId', ParseIntPipe) postId: number,
    @Param('commentId', ParseIntPipe) commentId: number,
    @User(ValidationPipe) user: ReturnUserDTO,
  ): Promise<any> {
    const result = await this.commentsService.deleteComment(postId, commentId, user);
    return { msg: 'Comment deleted', comment: result };
  }

  @Get('posts/:postId/comments/:commentId/votes')
  @UseGuards(AuthenticationGuard, BanGuard)
  @HttpCode(HttpStatus.OK)
  public async getCommentLikes(
    @Param('commentId', ParseIntPipe) commentId: number,
    @User(ValidationPipe) user: ReturnUserDTO,
  ): Promise<ReturnUserDTO[]> {
    return await this.commentsService.getCommentLikes(commentId);
  }

  @Post('posts/:postId/comments/:commentId/votes')
  @UseGuards(AuthenticationGuard, BanGuard)
  @HttpCode(HttpStatus.OK)
  public async likeComment(
    @Param('postId', ParseIntPipe) postId: number,
    @Param('commentId', ParseIntPipe) commentId: number,
    @User(ValidationPipe) user: ReturnUserDTO,
  ): Promise<ReturnCommentDTO> {
    return await this.commentsService.likeComment(postId, commentId, user);
  }

  @Delete('posts/:postId/comments/:commentId/votes')
  @UseGuards(AuthenticationGuard, BanGuard)
  @HttpCode(HttpStatus.OK)
  public async unlikeComment(
    @Param('postId', ParseIntPipe) postId: number,
    @Param('commentId', ParseIntPipe) commentId: number,
    @User(ValidationPipe) user: ReturnUserDTO,
  ): Promise<ReturnCommentDTO> {
    return await this.commentsService.unlikeComment(postId, commentId, user);
  }
}
