/* eslint-disable @typescript-eslint/no-unused-vars */
import { Comment } from "./comment.entity";
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from "typeorm";
import { User } from "./user.entity";
import { Vote } from "./vote.entity";

@Entity('posts')
export class Post {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column('timestamp')
  public addedOn: string;

  @Column('nvarchar')
  public title: string;

  @Column('nvarchar')
  public content: string;

  @Column({ default: false })
  public isFlagged: boolean;

  @Column({ default: false })
  public isLocked: boolean;

  @Column({ type: 'boolean', default: false })
  public isDeleted: boolean;

  @Column({ type: 'int', default: 0 })
  public likesCount: number;

  @Column({ type: 'int', default: 0 })
  public dislikesCount: number;

  @Column('int')
  public authorId: number;

  @ManyToOne(type => User, user => user.posts)
  public user: Promise<User>;

  @OneToMany(type => Comment, comment => comment.post)
  public comments: Promise<Comment[]>;

  @OneToMany(type => Vote, vote => vote.post)
  public votes: Promise<Vote[]>;
}