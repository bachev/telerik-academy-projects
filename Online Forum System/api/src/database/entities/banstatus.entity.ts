/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class Banstatus {
    @PrimaryGeneratedColumn('increment')
    public id: number;

    @Column('boolean')
    public isBanned: boolean;

    @Column('nvarchar')
    public admin: string;

    @Column('nvarchar')
    public description: string;

    @Column({ type: 'datetime', nullable: true })
    public expiresOn: Date;

    @Column('timestamp')
    public bannedOn: string;

    @OneToMany(
        type => User,
        user => user.banstatus
    )
    public users: Promise<User[]>
}