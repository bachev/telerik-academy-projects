/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { User } from './user.entity';

@Entity('users_friends')
export class FriendRequest {
  @PrimaryGeneratedColumn('increment')
  public id: number;
  @ManyToOne(
    type => User,
    user => user.friends_requests,
    { eager: true },
  )
  public requester: User;
  @ManyToOne(
    type => User,
    user => user.friends_received,
    { eager: true },
  )
  public receiver: User;

  @Column({ nullable: false, default: false })
  public isFriend: boolean;
}
