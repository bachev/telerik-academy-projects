/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { Post } from './post.entity';
import { User } from './user.entity';
import { Vote } from './vote.entity';

@Entity('comments')
export class Comment {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column('timestamp')
  public addedOn: string;

  @Column('nvarchar')
  public title: string;

  @Column('longtext')
  public content: string;

  @Column()
  public postId: number;

  @Column()
  public userId: number;

  @Column({ type: 'boolean', default: false })
  public isDeleted: boolean;

  @ManyToOne(
    type => Post,
    post => post.comments,
  )
  public post: Promise<Post>;

  @ManyToOne(
    type => User,
    user => user.comments,
  )
  public user: Promise<User>;

  @OneToMany(
    type => Vote,
    vote => vote.comment,
  )
  public votes: Promise<Vote[]>;
}
