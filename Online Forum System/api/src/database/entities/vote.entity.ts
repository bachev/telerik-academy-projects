/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Post } from './post.entity';
import { Comment } from './comment.entity';
import { User } from './user.entity';

@Entity('votes')
export class Vote {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column('boolean')
  public isLiked: boolean;

  @Column('timestamp')
  public addedOn: string;

  @ManyToOne(
    type => User,
    user => user.votes,
  )
  public user: Promise<User>;

  @ManyToOne(
    type => Post,
    post => post.votes,
  )
  public post: Promise<Post>;

  @ManyToOne(
    type => Comment,
    comment => comment.votes,
  )
  public comment: Promise<Comment>;
}
