/* eslint-disable @typescript-eslint/no-unused-vars */
import { UserRole } from '../../users/enums/user-role.enum';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Comment } from './comment.entity';
import { FriendRequest } from './friend.entity';
import { Post } from './post.entity';
import { Banstatus } from './banstatus.entity';
import { Vote } from './vote.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column('timestamp')
  public joinedOn: string;

  @Column({ type: 'nvarchar', unique: true })
  public username: string;

  @Column({ type: 'nvarchar', unique: true })
  public email: string;

  @Column('nvarchar')
  public password: string;

  @Column({ type: 'enum', enum: UserRole })
  public role: UserRole;

  @Column({ nullable: true })
  public avatar: string;

  @Column({ type: 'boolean', default: false })
  public isBanned: boolean;

  @Column({ type: 'boolean', default: false })
  public isDeleted: boolean;

  @OneToMany(
    type => Post,
    post => post.user,
  )
  public posts: Promise<Post[]>;
  @OneToMany(
    type => Vote,
    vote => vote.user,
  )
  public votes: Promise<Vote[]>;

  @OneToMany(
    type => Comment,
    comment => comment.user,
  )
  public comments: Promise<Comment[]>;

  @ManyToOne(
    type => Banstatus,
    banstatus => banstatus.users,
  )
  public banstatus: Promise<Banstatus>;

  @OneToMany(
    type => FriendRequest,
    requests => requests.requester,
  )
  public friends_requests: Promise<FriendRequest[]>;

  @OneToMany(
    type => FriendRequest,
    requests => requests.receiver,
  )
  public friends_received: Promise<FriendRequest[]>;
}
