import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { FriendRequest } from '../database/entities/friend.entity';
import { FriendsController } from './friends.controller';
import { FriendsService } from './friends.service';
import { MulterModule } from '@nestjs/platform-express';
import { extname } from 'path';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { diskStorage } from 'multer';
import { Banstatus } from '../database/entities/banstatus.entity';
import { AdminController } from './admin.controller';
import { PostsService } from '../posts/posts.service';
import { Post } from '../database/entities/post.entity';
import { Vote } from '../database/entities/vote.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, FriendRequest, Banstatus, Post, Vote]),
    MulterModule.register({
      fileFilter(_, file, cb) {
        const ext = extname(file.originalname);
        const allowedExtensions = ['.png', '.jpg', '.gif', '.jpeg'];

        if (!allowedExtensions.includes(ext)) {
          return cb(new ForumSystemError('Only images are allowed!', 400), false);
        }
        cb(null, true);
      },
      storage: diskStorage({
        destination: './avatars',
        filename: (_, file, cb) => {
          const randomName = Array.from({ length: 32 })
            .map(() => Math.round(Math.random() * 10))
            .join('');

          return cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  ],
  controllers: [UsersController, FriendsController, AdminController],
  providers: [UsersService, FriendsService, PostsService],
  exports: [UsersService],
})
export class UsersModule { }
