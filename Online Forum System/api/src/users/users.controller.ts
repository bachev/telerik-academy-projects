import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { CreateUserDTO } from './models/create-user.dto';
import { QueryUserDTO } from './models/query-user.dto';
import { ReturnUserDTO } from './models/return-user.dto';
import { UpdateUserDTO } from './models/update-user.dto';
import { UsersService } from './users.service';
import { AuthenticationGuard } from '../common/guards/auth.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { User } from '../common/decorators/user.decorator';
import { BanGuard } from '../common/guards/ban.guard';

@Controller()
export class UsersController {
  public constructor(private readonly usersService: UsersService) {}

  @Get('users')
  @UseGuards(AuthenticationGuard)
  public async getUsers(@Query() query: QueryUserDTO): Promise<ReturnUserDTO[]> {
    return await this.usersService.getUsers(query);
  }

  @Get('users/:userId')
  @UseGuards(AuthenticationGuard)
  public async getUserById(@Param('userId', ParseIntPipe) id: number): Promise<ReturnUserDTO> {
    return await this.usersService.getUserById(id);
  }

  @Post('users')
  public async createUser(@Body() body: CreateUserDTO): Promise<ReturnUserDTO> {
    return await this.usersService.createUser(body);
  }

  @Put('users/:userId')
  @UseGuards(AuthenticationGuard, BanGuard)
  public async updateUser(
    @Param('userId', ParseIntPipe) id: number,
    @Body() body: UpdateUserDTO,
    @User() user: ReturnUserDTO,
  ): Promise<ReturnUserDTO> {
    return await this.usersService.updateUser(id, body, user);
  }

  @Post('users/:userId/avatar')
  @UseInterceptors(FileInterceptor('file'))
  @UseGuards(AuthenticationGuard, BanGuard)
  public async uploadUserAvatar(
    @Param('userId', ParseIntPipe) id: number,
    @UploadedFile() file: any,
    @User() user: ReturnUserDTO,
  ): Promise<{ token: string }> {
    return await this.usersService.uploadUserAvatar(id, file, user);
  }

  @Get('users/:userId/activity')
  @UseGuards(AuthenticationGuard, BanGuard)
  public async getUserActivity(@Param('userId', ParseIntPipe) userId: number, @User() viewer: ReturnUserDTO) {
    return await this.usersService.getUserActivity(userId, viewer);
  }
}
