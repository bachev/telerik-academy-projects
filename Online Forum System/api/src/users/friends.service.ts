import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FriendRequest } from '../database/entities/friend.entity';
import { User } from '../database/entities/user.entity';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { UserRole } from './enums/user-role.enum';
import { ReturnUserDTO } from './models/return-user.dto';
import { UsersService } from './users.service';
import { plainToClass } from 'class-transformer';

@Injectable()
export class FriendsService {
  public constructor(
    private readonly usersService: UsersService,
    @InjectRepository(User) private readonly userRepository: Repository<User>,

    @InjectRepository(FriendRequest) private readonly friendsRepository: Repository<FriendRequest>,
  ) {}

  public async getUserFriends(userId: number) {
    const user = await this.usersService.findUserById(userId);
    const userFriends = await this.friendsRepository.find({
      where: [
        { requester: user, isFriend: true },
        { receiver: user, isFriend: true },
      ],
    });

    const filteredFriends = userFriends.map(request => {
      if (request.receiver.id !== userId) {
        return request.receiver;
      } else if (request.requester.id !== userId) {
        return request.requester;
      }
    });

    return plainToClass(ReturnUserDTO, filteredFriends, { excludeExtraneousValues: true });
  }

  public async getFriendRequests(userId: number, viewer: ReturnUserDTO) {
    const sentRequests = await this.friendsRepository.find({
      where: { requester: userId, isFriend: false },
    });
    const receivedRequests = await this.friendsRepository.find({
      where: { receiver: userId, isFriend: false },
    });

    let sent: ReturnUserDTO[];
    let received: ReturnUserDTO[];

    if (viewer.id !== userId && viewer.role !== UserRole.Admin) {
      const filteredSent = sentRequests.filter(friendRequest => friendRequest.requester.id === viewer.id);
      const filteredReceived = sentRequests.filter(friendRequest => friendRequest.receiver.id === viewer.id);
      sent = filteredSent.map(request =>
        plainToClass(ReturnUserDTO, request.receiver, { excludeExtraneousValues: true }),
      );
      received = filteredReceived.map(request =>
        plainToClass(ReturnUserDTO, request.requester, { excludeExtraneousValues: true }),
      );
    } else {
      sent = sentRequests.map(request =>
        plainToClass(ReturnUserDTO, request.receiver, { excludeExtraneousValues: true }),
      );
      received = receivedRequests.map(request =>
        plainToClass(ReturnUserDTO, request.requester, { excludeExtraneousValues: true }),
      );
    }
    return { sent, received };
  }

  public async requestFriend(userId: number, friendId: number): Promise<ReturnUserDTO> {
    const request = await this.findFriendRequest(userId, friendId);
    if (request !== undefined) {
      throw new ForumSystemError(`You've already requested to be friends with this user`, 401);
    }
    if (userId === friendId) {
      throw new ForumSystemError(`You can't be friends with yourself!`, 401);
    }
    const requester = await this.userRepository.findOne({ id: userId });
    const receiver = await this.userRepository.findOne({ id: friendId });
    const friendReq = this.friendsRepository.create();
    friendReq.requester = requester;
    friendReq.receiver = receiver;
    const savedRequest = await this.friendsRepository.save(friendReq);
    return plainToClass(ReturnUserDTO, savedRequest.receiver, { excludeExtraneousValues: true });
  }

  public async acceptFriendRequest(userId: number, friendId: number) {
    await this.usersService.findUserById(friendId);
    const request = await this.validateFriendRequest(userId, friendId);
    request.isFriend = true;
    const savedRequest = await this.friendsRepository.save(request);
    return plainToClass(ReturnUserDTO, savedRequest.receiver, { excludeExtraneousValues: true });
  }

  public async removeFriend(userId: number, friendId: number): Promise<ReturnUserDTO> {
    const removedUser = await this.usersService.findUserById(friendId);
    const request = await this.validateFriendRequest(userId, friendId);
    await this.friendsRepository.delete(request);
    return plainToClass(ReturnUserDTO, removedUser, { excludeExtraneousValues: true });
  }

  private async findFriendRequest(userId: number, friendId: number) {
    const friendRequest = await this.friendsRepository.findOne({
      where: [
        { requester: userId, receiver: friendId },
        { requester: friendId, receiver: userId },
      ],
    });
    return friendRequest;
  }

  private async validateFriendRequest(userId: number, friendId: number) {
    const request = await this.findFriendRequest(userId, friendId);
    if (request === undefined || request === null) {
      throw new ForumSystemError(`Friend request not found`, 404);
    }
    return request;
  }
}
