import { IsBoolean, Length, Allow } from 'class-validator';
import { Type } from 'class-transformer';

export class UpdateUserBanstatusDTO {
    @IsBoolean()
    public isBanned: boolean;

    @Length(10, 100)
    public description: string;

    @Allow()
    @Type(() => Date)
    public expiresOn: Date;
}