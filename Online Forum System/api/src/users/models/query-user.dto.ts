import { IsOptional } from 'class-validator';

export class QueryUserDTO {
    @IsOptional()
    public username?: string;

    @IsOptional()
    public email?: string;
}