import { Expose } from 'class-transformer';
import { UserRole } from '../enums/user-role.enum';

export class ReturnUserDTO {
    @Expose()
    public username: string;

    @Expose()
    public id: number;

    @Expose()
    public email: string;

    @Expose()
    public role: UserRole;

    @Expose()
    public joinedOn: string;

    @Expose()
    public avatar: string;

    @Expose()
    public isBanned: boolean;
}