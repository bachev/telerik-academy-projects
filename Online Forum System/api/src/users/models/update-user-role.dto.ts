import { UserRole } from "../enums/user-role.enum";
import { IsEnum } from 'class-validator';

export class UpdateUserRoleDTO {
    @IsEnum(UserRole)
    public role: UserRole;
}