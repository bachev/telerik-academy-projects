import { Length, Matches, IsEmail } from 'class-validator';

export class CreateUserDTO {
    @Length(2, 20)
    public username: string;

    @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/, {
        message:
            'The password must be minimum six characters long and contain at least one letter and one number!',
    })
    public password: string;

    @IsEmail()
    public email: string;
}