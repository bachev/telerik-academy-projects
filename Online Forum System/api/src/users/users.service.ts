import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { plainToClass } from 'class-transformer';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { ResponseMessageDTO } from '../common/models/response-message.dto';
import { User } from '../database/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateUserDTO } from './models/create-user.dto';
import { QueryUserDTO } from './models/query-user.dto';
import { ReturnUserDTO } from './models/return-user.dto';
import { UpdateUserDTO } from './models/update-user.dto';
import { UpdateUserRoleDTO } from './models/update-user-role.dto';
import { UpdateUserBanstatusDTO } from './models/update-user-banstatus.dto';
import { Banstatus } from '../database/entities/banstatus.entity';
import { Post } from '../database/entities/post.entity';
import { Comment } from '../database/entities/comment.entity';
import { Vote } from '../database/entities/vote.entity';
import { Cron } from '@nestjs/schedule';
import { ReturnCommentDTO } from '../comments/models/return-comment.dto';
import { ReturnPostDTO } from '../posts/models/return-post.dto';
import { UserRole } from './enums/user-role.enum';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class UsersService {
  public constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Banstatus) private readonly banstatusRepository: Repository<Banstatus>,
    private readonly jwtService: JwtService
  ) { }

  public async getUsers(query: QueryUserDTO): Promise<ReturnUserDTO[]> {
    let users: User[] = await this.userRepository.find({ isDeleted: false });

    if (query.username) {
      users = users.filter(user => user.username.toLowerCase().includes(query.username.toLowerCase()));
    }
    if (query.email) {
      users = users.filter(user => user.email.toLowerCase().includes(query.email.toLowerCase()));
    }

    return plainToClass(ReturnUserDTO, users, { excludeExtraneousValues: true });
  }

  public async getUserById(userId: number): Promise<ReturnUserDTO> {
    const user: User = await this.findUserById(userId);

    return plainToClass(ReturnUserDTO, user, { excludeExtraneousValues: true });
  }

  public async getUserByUsername(username: string): Promise<ReturnUserDTO> {
    const user: User = await this.userRepository.findOne({ where: { username, isDeleted: false } });

    if (user === undefined) {
      throw new ForumSystemError(`User named '${username}' does not exist!`, 404);
    }

    return plainToClass(ReturnUserDTO, user, { excludeExtraneousValues: true });
  }

  public async createUser(user: CreateUserDTO): Promise<ReturnUserDTO> {
    const newUser: User = this.userRepository.create(user);

    newUser.password = await bcrypt.hash(newUser.password, 10);
    newUser.posts = Promise.resolve([]);
    newUser.comments = Promise.resolve([]);

    try {
      await this.userRepository.save(newUser);
    } catch (error) {
      throw new ForumSystemError('Username or email already in use!', 400);
    }

    return plainToClass(ReturnUserDTO, newUser, { excludeExtraneousValues: true });
  }

  public async updateUser(userId: number, update: UpdateUserDTO, authUser: ReturnUserDTO): Promise<ReturnUserDTO> {
    let user: User = await this.findUserById(userId);

    if (userId !== authUser.id) {
      throw new ForumSystemError('You are not authorized to edit this user!', 401);
    }
    if (update.password) {
      update.password = await bcrypt.hash(update.password, 10);
    }
    user = { ...user, ...update };

    try {
      await this.userRepository.save(user);
    } catch (error) {
      throw new ForumSystemError('Username or email already in use!', 400);
    }

    return plainToClass(ReturnUserDTO, user, { excludeExtraneousValues: true });
  }

  public async deleteUser(userId: number): Promise<ResponseMessageDTO> {
    const user: User = await this.findUserById(userId);

    user.isDeleted = true;

    await this.userRepository.save(user);

    return { msg: `User ${user.username} deleted!` };
  }

  public async updateUserRole(userId: number, newRole: UpdateUserRoleDTO): Promise<ReturnUserDTO> {
    const user: User = await this.findUserById(userId);

    user.role = newRole.role;

    await this.userRepository.save(user);

    return plainToClass(ReturnUserDTO, user, { excludeExtraneousValues: true });
  }

  public async updateUserBanstatus(
    userId: number,
    ban: UpdateUserBanstatusDTO,
    admin: ReturnUserDTO,
  ): Promise<ReturnUserDTO> {
    const user: User = await this.findUserById(userId);

    if (!(await user.banstatus)) {
      const newBanstatus: Banstatus = this.banstatusRepository.create(ban);
      newBanstatus.admin = admin.username;
      await this.banstatusRepository.save(newBanstatus);

      user.banstatus = Promise.resolve(newBanstatus);
    } else {
      let banstatus: Banstatus = await this.banstatusRepository.findOne((await user.banstatus).id);
      banstatus = { ...banstatus, ...ban };
      banstatus.admin = admin.username;
      await this.banstatusRepository.save(banstatus);
    }
    user.isBanned = ban.isBanned;
    await this.userRepository.save(user);

    return plainToClass(ReturnUserDTO, user, { excludeExtraneousValues: true });
  }

  @Cron('10 * * * * *')
  private async removeExpiredBans(): Promise<void> {
    const activeBans: Banstatus[] = await this.banstatusRepository.find({ where: { isBanned: true } });
    activeBans.forEach(async ban => {
      if (ban.expiresOn <= new Date()) {
        ban.isBanned = false;
        ban.description = 'ban expired';
        ban.admin = 'auto';
        ban.expiresOn = null;
        await this.banstatusRepository.save(ban);

        const bannedUsers: User[] = await ban.users;
        const bannedUser = bannedUsers[0];
        bannedUser.isBanned = false;
        await this.userRepository.save(bannedUser);
      }
    });
  }

  public async uploadUserAvatar(userId: number, file: any, uploader: ReturnUserDTO): Promise<{ token: string }> {
    const user: User = await this.findUserById(userId);

    if (userId !== uploader.id) {
      throw new ForumSystemError('You are not authorized to edit this user!', 401);
    }
    user.avatar = file.filename;

    await this.userRepository.save(user);

    const returnUserDTO = plainToClass(ReturnUserDTO, user, { excludeExtraneousValues: true });

    const payload: ReturnUserDTO = { ...returnUserDTO };

    return { token: await this.jwtService.signAsync(payload) };
  }

  public async getUserActivity(userId: number, viewer: ReturnUserDTO) {
    const user = await this.findUserById(userId);
    const foundPosts: Post[] = (await user.posts).filter(post => post.isDeleted === false);
    const foundComments: Comment[] = (await user.comments).filter(comment => comment.isDeleted === false);

    const deletedPosts: ReturnPostDTO[] = plainToClass(
      ReturnPostDTO,
      (await user.posts).filter(post => post.isDeleted === true),
      { excludeExtraneousValues: true },
    );
    const deletedComments: ReturnCommentDTO[] = plainToClass(
      ReturnCommentDTO,
      (await user.comments).filter(comment => comment.isDeleted === true),
      { excludeExtraneousValues: true },
    );

    const userPosts: ReturnPostDTO[] = plainToClass(ReturnPostDTO, foundPosts, { excludeExtraneousValues: true });
    const userComments: ReturnCommentDTO[] = plainToClass(ReturnCommentDTO, foundComments, {
      excludeExtraneousValues: true,
    });
    const userVotes: Vote[] = await user.votes;

    const activity = { posts: userPosts, comments: userComments, votes: userVotes };

    const activityWithDeleted = {
      posts: userPosts,
      comments: userComments,
      votes: userVotes,
      deleted: { posts: deletedPosts, comments: deletedComments },
    };

    if (viewer.role === UserRole.Admin) {
      return activityWithDeleted;
    } else {
      return activity;
    }
  }

  public async findUserById(userId: number): Promise<User> {
    const user: User = await this.userRepository.findOne(userId, { where: { isDeleted: false } });

    if (user === undefined || user === null) {
      throw new ForumSystemError(`User with ID: ${userId} does not exist!`, 404);
    }

    return user;
  }
}
