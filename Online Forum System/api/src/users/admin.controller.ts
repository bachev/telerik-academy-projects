import {
  Controller,
  UseGuards,
  Put,
  Param,
  ParseIntPipe,
  Body,
  Delete
} from '@nestjs/common';
import { UsersService } from './users.service';
import { PostsService } from '../posts/posts.service';
import { AuthenticationGuard } from '../common/guards/auth.guard';
import { AdminGuard } from '../common/guards/admin.guard';
import { LockPostDTO } from '../posts/models/lock-post.dto';
import { ReturnPostDTO } from '../posts/models/return-post.dto';
import { UpdateUserRoleDTO } from './models/update-user-role.dto';
import { ReturnUserDTO } from './models/return-user.dto';
import { UpdateUserBanstatusDTO } from './models/update-user-banstatus.dto';
import { User } from '../common/decorators/user.decorator';
import { ResponseMessageDTO } from 'src/common/models/response-message.dto';

@Controller('admin')
@UseGuards(AuthenticationGuard, AdminGuard)
export class AdminController {
  public constructor(
    private readonly usersService: UsersService,
    private readonly postsService: PostsService,
  ) { }

  @Put('users/:userId')
  public async updateUserRole(
    @Param('userId', ParseIntPipe) id: number,
    @Body() role: UpdateUserRoleDTO,
  ): Promise<ReturnUserDTO> {
    return await this.usersService.updateUserRole(id, role);
  }

  @Put('users/:userId/banstatus')
  public async updateUserBanstatus(
    @Param('userId', ParseIntPipe) id: number,
    @Body() ban: UpdateUserBanstatusDTO,
    @User() user: ReturnUserDTO,
  ): Promise<ReturnUserDTO> {
    return await this.usersService.updateUserBanstatus(id, ban, user);
  }

  @Delete('users/:userId')
  public async deleteUser(@Param('userId', ParseIntPipe) id: number): Promise<ResponseMessageDTO> {
    return await this.usersService.deleteUser(id);
  }

  @Put('posts/:postId')
  public async togglePostLock(
    @Param('postId', ParseIntPipe) id: number,
    @Body() body: LockPostDTO,
  ): Promise<ReturnPostDTO> {
    return await this.postsService.togglePostLock(id, body);
  }
}
