import {
  Controller,
  Param,
  ParseIntPipe,
  Body,
  Post,
  UseGuards,
  HttpCode,
  HttpStatus,
  Get,
  Delete,
  Put,
} from '@nestjs/common';
import { FriendsService } from './friends.service';
import { AuthenticationGuard } from '../common/guards/auth.guard';
import { BanGuard } from '../common/guards/ban.guard';
import { User } from '../common/decorators/user.decorator';
import { ReturnUserDTO } from './models/return-user.dto';
import { UsersService } from './users.service';

@Controller()
export class FriendsController {
  public constructor(private readonly friendsService: FriendsService) {}

  @Get('users/:userId/friends')
  @UseGuards(AuthenticationGuard)
  @HttpCode(HttpStatus.OK)
  public async getUserFriends(@Param('userId', ParseIntPipe) userId: number) {
    return await this.friendsService.getUserFriends(userId);
  }

  @Get('users/:userId/friends/requests')
  @UseGuards(AuthenticationGuard, BanGuard)
  @HttpCode(HttpStatus.OK)
  public async getUserFriendRequests(@User() viewer: ReturnUserDTO, @Param('userId', ParseIntPipe) userId: number) {
    return await this.friendsService.getFriendRequests(userId, viewer);
  }

  @Post('users/:userId/friends')
  @UseGuards(AuthenticationGuard, BanGuard)
  @HttpCode(HttpStatus.CREATED)
  public async addFriend(@User() user: ReturnUserDTO, @Param('userId', ParseIntPipe) friendId: number) {
    return await this.friendsService.requestFriend(user.id, friendId);
  }

  @Put('users/:userId/friends')
  @UseGuards(AuthenticationGuard, BanGuard)
  @HttpCode(HttpStatus.CREATED)
  public async acceptFriend(@User() user: ReturnUserDTO, @Param('userId', ParseIntPipe) friendId: number) {
    return await this.friendsService.acceptFriendRequest(user.id, friendId);
  }

  @Delete('users/:userId/friends')
  @UseGuards(AuthenticationGuard, BanGuard)
  @HttpCode(HttpStatus.CREATED)
  public async removeFriend(@User() user: ReturnUserDTO, @Param('userId', ParseIntPipe) friendId: number) {
    return await this.friendsService.removeFriend(user.id, friendId);
  }
}
