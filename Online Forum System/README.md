﻿## How to setup and run the project:

**1. Create a local copy of the project on your computer**

Open *Git Bash* shell in a folder of your choice and run the following command:
```git clone https://gitlab.com/bachev/telerik-academy-projects.git```

**2. Setup the database**
-   Install MySQL and MySQL Workbench
-   Using MySQL Workbench create a new schema and name it ***forumdb***

**3. Create the following *.env* file in the *api* folder (where _package.json_ and the rest of the config files are):**

    PORT=3000
    DB_TYPE=mysql
    DB_HOST=localhost
    DB_PORT=3306
    DB_USERNAME=root
    DB_PASSWORD=1234
    DB_DATABASE_NAME=forumdb
    JWT_SECRET = forumsystem
    JWT_EXPIRE_TIME = 3600

**4.  Install all dependencies**

Open the project with *Visual Studio Code* and run ```npm install``` both in *api* and *client* folders (where *package.json* and the rest of the config files are)

**5. Run the application**
-   Server side - navigate to the *api* folder and run:
    -   `npm run start:dev` - to start the server in watch mode
    -   `npm run test` - to run the unit tests
-   Client side - navigate to the *client* folder and run:
    -   `ng s -o` - to launch the application and open it in the browser
    -   `npm run test` - to run the unit tests

