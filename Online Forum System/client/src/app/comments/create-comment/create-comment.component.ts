import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommentsService } from '../comments.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { UserDTO } from 'src/app/users/models/user.dto';

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: ['./create-comment.component.css'],
})
export class CreateCommentComponent implements OnInit {
  public registerForm: FormGroup;
  public loggedUser: UserDTO;

  constructor(
    private readonly commentsService: CommentsService,
    private readonly notificator: NotificationService,
    private readonly formBuilder: FormBuilder,
    private readonly activatedRoute: ActivatedRoute,
    private readonly authService: AuthService
  ) {}

  @Output()
  public onCreateCommentEvent = new EventEmitter<string>();

  @Input()
  public postTitle: string;

  ngOnInit(): void {
    this.loggedUser = this.authService.getLoggedUserData();
    this.registerForm = this.formBuilder.group({
      title: [
        `Re: ${this.postTitle}`,
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(5000),
        ],
      ],
      content: ['', [Validators.required, Validators.minLength(10)]],
    });
  }

  public validateComment(comment): void {
    if (
      !comment.title ||
      comment.title === undefined ||
      comment.title.length > 50
    ) {
      return this.notificator.error(
        'Title should be min 2 and max 50 characters long!'
      );
    } else if (
      !comment.content ||
      comment.content === undefined ||
      comment.content.length > 50
    ) {
      return this.notificator.error('Content should be min 2 characters long!');
    } else {
      return this.notificator.error('Comment could not be created');
    }
  }

  public createComment(): void {
    const title = this.registerForm.get('title').value;
    const content = this.registerForm.get('content').value;
    const newComment: any = { title, content };
    const postId: number = +this.activatedRoute.snapshot.paramMap.get('postId');

    this.commentsService.createComment(postId, newComment).subscribe(
      () => {
        this.notificator.success('Comment created!');
        this.registerForm.reset();
      },
      () => this.validateComment(newComment)
    );

    this.onCreateCommentEvent.next('commentCreated');
  }
}
