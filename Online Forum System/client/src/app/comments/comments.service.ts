import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../config/config';

@Injectable()
export class CommentsService {
  public constructor(private readonly httpClient: HttpClient) {}

  public getAllComments(): Observable<any[]> {
    return this.httpClient.get<any[]>(`${CONFIG.DOMAIN_NAME}/comments/all`);
  }

  public getPostComments(postId: number): Observable<any[]> {
    return this.httpClient.get<any[]>(
      `${CONFIG.DOMAIN_NAME}/posts/${postId}/comments`
    );
  }

  public createComment(postId: number, comment: any): Observable<any> {
    return this.httpClient.post<any>(
      `${CONFIG.DOMAIN_NAME}/posts/${postId}/comments`,
      comment
    );
  }

  public updateComment(
    postId: number,
    commentId: number,
    comment: any
  ): Observable<any> {
    return this.httpClient.put<any>(
      `${CONFIG.DOMAIN_NAME}/posts/${postId}/comments/${commentId}`,
      comment
    );
  }

  public deleteComment(postId: number, commentId: number): Observable<void> {
    return this.httpClient.delete<void>(
      `${CONFIG.DOMAIN_NAME}/posts/${postId}/comments/${commentId}`
    );
  }

  public getCommentLikes(postId: number, commentId: number): Observable<any> {
    return this.httpClient.get(
      `${CONFIG.DOMAIN_NAME}/posts/${postId}/comments/${commentId}/votes`
    );
  }

  public likeComment(postId: number, commentId: number): Observable<any> {
    return this.httpClient.post<any>(
      `${CONFIG.DOMAIN_NAME}/posts/${postId}/comments/${commentId}/votes`,
      null
    );
  }

  public unlikeComment(postId: number, commentId: number): Observable<any> {
    return this.httpClient.delete<any>(
      `${CONFIG.DOMAIN_NAME}/posts/${postId}/comments/${commentId}/votes`
    );
  }
}
