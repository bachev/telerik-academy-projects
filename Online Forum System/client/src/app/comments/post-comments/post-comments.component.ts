import { Component, OnInit, Input } from '@angular/core';
import { CommentsService } from '../comments.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-post-comments',
  templateUrl: './post-comments.component.html',
  styleUrls: ['./post-comments.component.css'],
})
export class PostCommentsComponent implements OnInit {
  public postComments: any[] = [];
  @Input()
  public postTitle: string;
  @Input()
  public postLocked: string;

  constructor(
    private readonly commentsService: CommentsService,
    private readonly activatedRoute: ActivatedRoute
  ) {}

  public handleChange(): void {
    setTimeout(() => {
      this.ngOnInit();
    }, 200);
  }

  public getLock(): boolean {
    if (this.postLocked === 'true') {
      return true;
    }
  }

  ngOnInit(): void {
    const postId: number = +this.activatedRoute.snapshot.paramMap.get('postId');
    this.commentsService.getPostComments(postId).subscribe({
      next: (data) => (this.postComments = data),
      error: (err) => console.log(err),
    });
  }
  ngOnDestroy(): void {}
}
