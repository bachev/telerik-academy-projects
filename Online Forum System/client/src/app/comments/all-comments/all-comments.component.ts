import { Component, OnInit } from '@angular/core';
import { CommentsService } from '../comments.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-all-comments',
  templateUrl: './all-comments.component.html',
  styleUrls: ['./all-comments.component.css']
})
export class AllCommentsComponent implements OnInit {
  public postComments: any[] = [];

  constructor(
    private readonly commentsService: CommentsService,
    private readonly activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.commentsService.getAllComments().subscribe({
      next: data => (this.postComments = data),
      error: err => console.log(err)
    });
  }
}
