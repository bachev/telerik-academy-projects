import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { CONFIG } from 'src/app/config/config';
import { UserDTO } from 'src/app/users/models/user.dto';
import { UsersService } from 'src/app/users/services/users.service';
import { NotificationService } from '../../core/services/notification.service';
import { Router } from '@angular/router';
import { CommentsService } from '../comments.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../../core/services/auth.service';
import { UpdateCommentDTO } from '../models/update-comment.dto';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-single-comment',
  templateUrl: './single-comment.component.html',
  styleUrls: ['./single-comment.component.css'],
})
export class SingleCommentComponent implements OnInit, OnDestroy {
  @Input()
  public postLocked: string;
  public userAvatarSrcPrefix: string = CONFIG.USER_AVATAR_SRC_PREFIX;
  public defaultUserAvatarImagePath: string =
    CONFIG.DEFAULT_USER_AVATAR_IMAGE_PATH;

  public authorization: boolean;
  public commentAuthor: UserDTO;
  public loggedUserData: UserDTO;

  public likes: UserDTO[] = [];
  public likedByUser: boolean;
  public commentInEditMode = false;

  public registerForm: FormGroup;

  public subscriptions: Subscription[] = [];
  @Input() public singleComment: any;

  constructor(
    private readonly usersService: UsersService,
    private readonly commentsService: CommentsService,
    private readonly authService: AuthService,
    private readonly notificator: NotificationService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = (): boolean => false;
  }

  ngOnInit(): void {
    this.subscriptions.push(
      this.usersService
        .getUserById(this.singleComment.userId)
        .subscribe((foundUser: UserDTO) => (this.commentAuthor = foundUser))
    );
    this.subscriptions.push(
      this.authService.loggedUserData$.subscribe((data: UserDTO) => {
        this.loggedUserData = data;
        this.authorizeFn();
      })
    );

    this.registerForm = this.formBuilder.group({
      title: [
        this.singleComment.title,
        [Validators.minLength(2), Validators.maxLength(5000)],
      ],
      content: [this.singleComment.content, [Validators.minLength(2)]],
    });

    this.getCommentLikes();
  }

  ngOnDestroy(): void {
    this.subscriptions.map((sub) => sub.unsubscribe());
  }

  public getLock(): boolean {
    if (this.postLocked === 'true') {
      return true;
    }
  }

  public showEditForm(): void {
    this.commentInEditMode = !this.commentInEditMode;
  }

  public authorizeFn(): void {
    if (
      this.loggedUserData.role === 'Admin' ||
      this.loggedUserData.id === this.singleComment.userId
    ) {
      this.authorization = true;
    } else {
      this.authorization = false;
    }
  }

  public updateComment(title?: string, content?: string): void {
    const editedComment = new UpdateCommentDTO();

    if (title) {
      editedComment.title = title;
    }
    if (content) {
      editedComment.content = content;
    }
    this.subscriptions.push(
      this.commentsService
        .updateComment(
          this.singleComment.postId,
          this.singleComment.id,
          editedComment
        )
        .subscribe(
          () => {
            this.notificator.success('Comment updated!');
            this.singleComment.title = title;
            this.singleComment.content = content;
            this.showEditForm();
          },
          () => this.notificator.error('Unable to update comment!')
        )
    );
  }

  public commentLikedUsernames(): string {
    return this.likes.map((user) => user.username).join(', ');
  }

  public deleteComment(): void {
    this.subscriptions.push(
      this.commentsService
        .deleteComment(this.singleComment.postId, this.singleComment.id)
        .subscribe(() => {
          this.notificator.success('Post deleted!');
          this.router.navigate(['/posts', this.singleComment.postId]);
        })
    );
  }
  public likeComment(): void {
    this.subscriptions.push(
      this.commentsService
        .likeComment(this.singleComment.postId, this.singleComment.id)
        .subscribe(() => {
          this.notificator.success('Post liked!');
          this.likedByUser = true;
          this.likes.push(this.loggedUserData);
        })
    );
  }
  public unlikeComment(): void {
    this.subscriptions.push(
      this.commentsService
        .unlikeComment(this.singleComment.postId, this.singleComment.id)
        .subscribe(() => {
          this.notificator.success('Post unliked!');
          this.likes = this.likes.filter(
            (user) => user.id !== this.loggedUserData.id
          );
          this.likedByUser = false;
        })
    );
  }

  public getCommentLikes(): void {
    this.subscriptions.push(
      this.commentsService
        .getCommentLikes(this.singleComment.postId, this.singleComment.id)
        .subscribe({
          next: (data) => {
            this.likes = data;
            if (this.likes) {
              this.likedByUser = this.likes
                .map((user) => user.id)
                .includes(this.loggedUserData.id);
            }
          },
          error: (err) => {
            this.notificator.error(`Unable to fetch likes: ${err}`);
          },
        })
    );
  }
}
