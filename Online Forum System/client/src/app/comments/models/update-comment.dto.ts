export class UpdateCommentDTO {
  public title?: string;
  public content?: string;
}
