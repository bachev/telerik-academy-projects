import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentsService } from './comments.service';
import { AllCommentsComponent } from './all-comments/all-comments.component';
import { SingleCommentComponent } from './single-comment/single-comment.component';
import { SharedModule } from '../shared/shared.module';
import { PostCommentsComponent } from './post-comments/post-comments.component';
import { CreateCommentComponent } from './create-comment/create-comment.component';
import { RouterModule } from '@angular/router';
import { HomeCommentComponent } from './home-comment/home-comment.component';
import { PostsService } from '../posts/services/posts.service';
import { UsersService } from '../users/services/users.service';
import { AuthService } from '../core/services/auth.service';

@NgModule({
  declarations: [
    AllCommentsComponent,
    SingleCommentComponent,
    PostCommentsComponent,
    CreateCommentComponent,
    HomeCommentComponent,
  ],
  imports: [CommonModule, SharedModule, RouterModule],
  providers: [CommentsService, PostsService, UsersService, AuthService],
  exports: [
    AllCommentsComponent,
    PostCommentsComponent,
    SingleCommentComponent,
    CreateCommentComponent,
    HomeCommentComponent,
  ],
})
export class CommentsModule {}
