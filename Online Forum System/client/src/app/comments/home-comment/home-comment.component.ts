import { Component, OnInit, Input } from '@angular/core';
import { CONFIG } from 'src/app/config/config';
import { UserDTO } from 'src/app/users/models/user.dto';
import { UsersService } from 'src/app/users/services/users.service';

@Component({
  selector: 'app-home-comment',
  templateUrl: './home-comment.component.html',
  styleUrls: ['./home-comment.component.css'],
})
export class HomeCommentComponent implements OnInit {
  public userAvatarSrcPrefix: string = CONFIG.USER_AVATAR_SRC_PREFIX;
  public defaultUserAvatarImagePath: string =
    CONFIG.DEFAULT_USER_AVATAR_IMAGE_PATH;

  public postAuthor: UserDTO;
  @Input() public singleComment: any;

  constructor(private readonly usersService: UsersService) {}

  ngOnInit(): void {
    this.usersService
      .getUserById(this.singleComment.userId)
      .subscribe((foundUser: UserDTO) => (this.postAuthor = foundUser));
  }
}
