import { PostsService } from './posts.service';
import { async, TestBed } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CONFIG } from 'src/app/config/config';
import { of } from 'rxjs';
import { PostDTO } from '../models/post.dto';

describe('PostsService', () => {
    let httpClient;

    let service: PostsService;

    beforeEach(async(() => {
        jest.clearAllMocks();

        httpClient = {
            get() { },
            post() { },
            put() { },
            delete() { }
        };

        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [PostsService]
        }).overrideProvider(HttpClient, { useValue: httpClient });

        service = TestBed.get(PostsService);
    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(service).toBeDefined();
    });

    describe('getAllPosts() should', () => {
        it('call the httpClient.get() method once with correct parameters', done => {
            // Arrange
            const url = `${CONFIG.DOMAIN_NAME}/posts`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            // Act & Assert
            service.getAllPosts().subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url);

                done();
            });
        });

        it('should return the result from the httpClient.get() method', () => {
            // Arrange
            const returnValue = of('return value');

            jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            // Act
            const result = service.getAllPosts();

            // Assert
            expect(result).toEqual(returnValue);
        });
    });

    describe('getPostById() should', () => {
        it('should call the httpClient.get() method once with correct parameters', done => {
            // Arrange
            const id = 1;
            const url = `${CONFIG.DOMAIN_NAME}/posts/${id}`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            // Act & Assert
            service.getPostById(id).subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url);

                done();
            });
        });

        it('should return the result from the httpClient.get() method', () => {
            // Arrange
            const id = 1;
            const returnValue = of('return value');

            jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);
            // Act
            const result = service.getPostById(id);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });

    describe('createPost() should', () => {
        it('call the httpClient.post() method once with correct parameters', done => {
            // Arrange
            const url = `${CONFIG.DOMAIN_NAME}/posts`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);
            const post = new PostDTO();

            // Act & Assert
            service.createPost(post).subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url, post);

                done();
            });
        });

        it('should return the result from the httpClient.post() method', () => {
            // Arrange
            const returnValue = of('return value');

            jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);
            const post = new PostDTO();

            // Act
            const result = service.createPost(post);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });

    describe('updatePost() should', () => {
        it('should call the httpClient.put() method once with correct parameters', done => {
            // Arrange
            const id = 1;
            const url = `${CONFIG.DOMAIN_NAME}/posts/${id}`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);
            const post = { id };

            // Act & Assert
            service.updatePost(post as any, id).subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url, post);

                done();
            });
        });
        it('should return the result from the httpClient.put() method', () => {
            // Arrange
            const id = 1;
            const returnValue = of('return value');

            jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);
            const post = { id };

            // Act
            const result = service.updatePost(post as any, id);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });

    describe('deletePost() should', () => {
        it('should call the httpClient.delete() method once with correct parameters', done => {
            // Arrange
            const id = 1;
            const url = `${CONFIG.DOMAIN_NAME}/posts/${id}`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(returnValue);

            // Act & Assert
            service.deletePost(id).subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url);

                done();
            });
        });
    });

    describe('flagPost() should', () => {
        it('should call the httpClient.post() method once with correct parameters', done => {
            // Arrange
            const id = 1;
            const url = `${CONFIG.DOMAIN_NAME}/posts/${id}`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);

            // Act & Assert
            service.flagPost(id).subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url, null);

                done();
            });
        });

        it('should return the result from the httpClient.post() method', () => {
            // Arrange
            const returnValue = of('return value');

            jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);
            const id = 1;

            // Act
            const result = service.flagPost(id);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });

    describe('likePost() should', () => {
        it('should call the httpClient.post() method once with correct parameters', done => {
            // Arrange
            const id = 1;
            const url = `${CONFIG.DOMAIN_NAME}/posts/${id}/votes`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);

            // Act & Assert
            service.likePost(id).subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url, null);

                done();
            });
        });

        it('should return the result from the httpClient.post() method', () => {
            // Arrange
            const returnValue = of('return value');

            jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);
            const id = 1;

            // Act
            const result = service.likePost(id);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });

    describe('unlikePost() should', () => {
        it('should call the httpClient.delete() method once with correct parameters', done => {
            // Arrange
            const id = 1;
            const url = `${CONFIG.DOMAIN_NAME}/posts/${id}/votes`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(returnValue);

            // Act & Assert
            service.unlikePost(id).subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url);

                done();
            });
        });

        it('should return the result from the httpClient.delete() method', () => {
            // Arrange
            const returnValue = of('return value');

            jest.spyOn(httpClient, 'delete').mockReturnValue(returnValue);
            const id = 1;

            // Act
            const result = service.unlikePost(id);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });

    describe('togglePostLock() should', () => {
        it('should call the httpClient.put() method once with correct parameters', done => {
            // Arrange
            const id = 1;
            const url = `${CONFIG.DOMAIN_NAME}/admin/posts/${id}`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);
            const post = { id };

            // Act & Assert
            service.togglePostLock(id, post as any).subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url, post);

                done();
            });
        });

        it('should return the result from the httpClient.put() method', () => {
            // Arrange
            const id = 1;
            const returnValue = of('return value');

            jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);
            const post = { id };

            // Act
            const result = service.togglePostLock(id, post as any);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });
});
