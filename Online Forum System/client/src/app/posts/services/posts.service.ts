import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PostDTO } from '../models/post.dto';
import { CONFIG } from '../../config/config';
import { CreatePostDTO } from '../models/create-post.dto';
import { UpdatePostDTO } from '../models/update-post.dto';
import { LockPostDTO } from '../models/lock-post.dto';

@Injectable()
export class PostsService {

  public constructor(private readonly httpClient: HttpClient) { }

  public getAllPosts(): Observable<PostDTO[]> {
    return this.httpClient.get<PostDTO[]>(`${CONFIG.DOMAIN_NAME}/posts`);
  }

  public getPostById(postId: number): Observable<PostDTO> {
    return this.httpClient.get<PostDTO>(`${CONFIG.DOMAIN_NAME}/posts/${postId}`);
  }

  public createPost(post: CreatePostDTO): Observable<PostDTO> {
    return this.httpClient.post<PostDTO>(`${CONFIG.DOMAIN_NAME}/posts`, post);
  }

  public updatePost(post: UpdatePostDTO, postId: number): Observable<PostDTO> {
    return this.httpClient.put<PostDTO>(`${CONFIG.DOMAIN_NAME}/posts/${postId}`, post);
  }

  public deletePost(postId: number): Observable<void> {
    return this.httpClient.delete<void>(`${CONFIG.DOMAIN_NAME}/posts/${postId}`);
  }

  public flagPost(postId: number): Observable<PostDTO> {
    return this.httpClient.post<PostDTO>(`${CONFIG.DOMAIN_NAME}/posts/${postId}`, null);
  }

  public likePost(postId: number): Observable<PostDTO> {
    return this.httpClient.post<PostDTO>(`${CONFIG.DOMAIN_NAME}/posts/${postId}/votes`, null);
  }

  public unlikePost(postId: number): Observable<PostDTO> {
    return this.httpClient.delete<PostDTO>(`${CONFIG.DOMAIN_NAME}/posts/${postId}/votes`);
  }

  public togglePostLock(postId: number, postLock: LockPostDTO): Observable<PostDTO> {
    return this.httpClient.put<PostDTO>(`${CONFIG.DOMAIN_NAME}/admin/posts/${postId}`, postLock);
  }
}
