export class CreatePostDTO {
    public title: string;
    public content: string;
}
