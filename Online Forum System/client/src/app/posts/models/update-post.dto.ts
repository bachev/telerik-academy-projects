export class UpdatePostDTO {
    public title?: string;
    public content?: string;
}
