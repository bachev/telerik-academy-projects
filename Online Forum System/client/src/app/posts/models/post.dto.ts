export class PostDTO {
    public title: string;
    public id: number;
    public content: string;
    public addedOn: string;
    public likesCount: number;
    public dislikesCount: number;
    public authorId: number;
    public isFlagged: boolean;
    public isLocked: boolean;
}
