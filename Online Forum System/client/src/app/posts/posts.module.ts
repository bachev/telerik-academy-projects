import { NgModule } from '@angular/core';

import { AllPostsComponent } from './all-posts/all-posts.component';
import { SharedModule } from '../shared/shared.module';
import { PostsService } from './services/posts.service';
import { PostsRoutingModule } from './posts-routing.module';
import { SinglePostComponent } from './single-post/single-post.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { CommentsModule } from '../comments/comments.module';
import { CommentsService } from '../comments/comments.service';
import { SortByDatePipe } from '../core/pipes/sort.pipe';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AllPostsComponent,
    SinglePostComponent,
    PostDetailsComponent,
    CreatePostComponent,
  ],
  imports: [SharedModule, RouterModule, PostsRoutingModule, CommentsModule],
  providers: [PostsService, CommentsService],
  exports: [AllPostsComponent, SinglePostComponent, PostsRoutingModule],
})
export class PostsModule {}
