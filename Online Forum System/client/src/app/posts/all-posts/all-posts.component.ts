import { Component, OnInit } from '@angular/core';
import { PostDTO } from '../models/post.dto';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-all-posts',
  templateUrl: './all-posts.component.html',
  styleUrls: ['./all-posts.component.css'],
})
export class AllPostsComponent implements OnInit {
  public posts: PostDTO[] = [];
  public sort = 'DESC';

  constructor(private readonly postsService: PostsService) {}

  ngOnInit(): void {
    this.postsService.getAllPosts().subscribe({
      next: (data) => (this.posts = data),
      error: (err) => console.log(err),
    });
  }
}
