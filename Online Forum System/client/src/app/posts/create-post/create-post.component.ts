import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { PostsService } from '../services/posts.service';
import { CreatePostDTO } from '../models/create-post.dto';
import { NotificationService } from '../../core/services/notification.service';
import { AuthService } from '../../core/services/auth.service';
import { UserDTO } from '../../users/models/user.dto';

@Component({
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css'],
})
export class CreatePostComponent implements OnInit {
  public postForm: FormGroup;
  public loggedUser: UserDTO;

  constructor(
    private readonly postsService: PostsService,
    private readonly notificator: NotificationService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService
  ) { }

  ngOnInit(): void {
    this.loggedUser = this.authService.getLoggedUserData();

    this.postForm = this.formBuilder.group({
      title: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50),
        ],
      ],
      content: ['', [Validators.required, Validators.minLength(10)]],
    });
  }

  public createPost(title: string, content: string): void {
    const newPost: CreatePostDTO = { title, content };

    this.postsService.createPost(newPost).subscribe(
      () => {
        this.notificator.success('Post created!');
        this.router.navigate(['home']);
      },
      () => this.notificator.error('Unable to create post!')
    );
  }
}
