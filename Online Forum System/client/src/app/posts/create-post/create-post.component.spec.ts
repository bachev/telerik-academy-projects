import { CreatePostComponent } from './create-post.component';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { NotificationService } from '../../core/services/notification.service';
import { ReactiveFormsModule, FormBuilder, Validators } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { PostsService } from '../services/posts.service';
import { Router } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { PostDTO } from '../models/post.dto';
import { of, throwError } from 'rxjs';
import { CreatePostDTO } from '../models/create-post.dto';


describe('CreatePostComponent', () => {
    let authService;
    let postsService;
    let notificator;
    let router;
    let formBuilder;

    let fixture: ComponentFixture<CreatePostComponent>;
    let component: CreatePostComponent;

    beforeEach(async(() => {
        jest.clearAllMocks();

        authService = {
            getLoggedUserData() { }
        };

        postsService = {
            createPost() { }
        };

        notificator = {
            success() { },
            error() { }
        };

        router = {
            navigate() { }
        };

        formBuilder = {
            group() { }
        };

        TestBed.configureTestingModule({
            imports: [RouterTestingModule.withRoutes([]), ReactiveFormsModule, SharedModule],
            declarations: [CreatePostComponent],
            providers: [AuthService, PostsService, NotificationService]
        })
            .overrideProvider(AuthService, { useValue: authService })
            .overrideProvider(PostsService, { useValue: postsService })
            .overrideProvider(NotificationService, { useValue: notificator })
            .overrideProvider(FormBuilder, { useValue: formBuilder })
            .compileComponents()
            .then(() => {
                fixture = TestBed.createComponent(CreatePostComponent);
                router = TestBed.get(Router);
                component = fixture.componentInstance;
            });
    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(component).toBeDefined();
    });

    describe('ngOnInit() should', () => {
        it('should call formBuilder.group() once', () => {
            // Arrange
            const mockedPostForm = 'post form';

            const spy = jest
                .spyOn(formBuilder, 'group')
                .mockReturnValue(mockedPostForm);

            // Act
            component.ngOnInit();

            // Assert
            expect(spy).toBeCalledTimes(1);
        });

        it('should set the postForm filed value', () => {
            // Arrange
            const mockedLoginForm = 'login form';

            const spy = jest
                .spyOn(formBuilder, 'group')
                .mockReturnValue(mockedLoginForm);

            // Act
            component.ngOnInit();

            // Assert
            expect(component.postForm).toEqual(mockedLoginForm);
        });
    });

    describe('createPost() should', () => {
        it('should call the postsService.createPost() once with correct parameters', () => {
            // Arrange
            const fakePost: PostDTO = {
                title: 'title',
                id: 1,
                content: 'content',
                addedOn: 'date',
                likesCount: 0,
                dislikesCount: 0,
                authorId: 1,
                isFlagged: false,
                isLocked: false,
            };

            const fakeCreatePost: CreatePostDTO = { title: 'title', content: 'content' };

            const spy = jest.spyOn(postsService, 'createPost').mockReturnValue(of(fakePost));

            // Act
            component.createPost('title', 'content');

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(fakeCreatePost);
        });

        it('should call notificator.success() once with the correct message when the response is successful', () => {
            // Arrange
            const title = 'title';
            const content = 'content';

            jest.spyOn(postsService, 'createPost').mockReturnValue(of('mocked success return value'));
            const spy = jest.spyOn(notificator, 'success');
            jest.spyOn(router, 'navigate');

            // Act
            component.createPost(title, content);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith('Post created!');
        });

        it('should call router.navigate() once with the correct route when the response is successful', () => {
            // Arrange
            const title = 'title';
            const content = 'content';

            jest.spyOn(postsService, 'createPost').mockReturnValue(of('mocked success return value'));
            jest.spyOn(notificator, 'success');
            const spy = jest.spyOn(router, 'navigate');

            // Act
            const reuslt = component.createPost(title, content);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(['home']);
        });

        it('should call notificator.error() once with the correct message when the response is unsuccessful', () => {
            // Arrange
            const title = 'title';
            const content = 'content';

            jest.spyOn(postsService, 'createPost').mockReturnValue(throwError('mocked fail return value'));
            const spy = jest.spyOn(notificator, 'error');

            // Act
            component.createPost(title, content);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith('Unable to create post!');
        });
    });
});
