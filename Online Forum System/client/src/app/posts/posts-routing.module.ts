import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllPostsComponent } from './all-posts/all-posts.component';
import { PostDetailsComponent } from './post-details/post-details.component';
import { AuthGuard } from '../core/guards/auth.guard';
import { CreatePostComponent } from './create-post/create-post.component';

const routes: Routes = [
  {
    path: '',
    component: AllPostsComponent,
    canActivate: [AuthGuard],
    pathMatch: 'full'
  },
  { path: 'create', component: CreatePostComponent, canActivate: [AuthGuard] },
  { path: ':postId', component: PostDetailsComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule {}
