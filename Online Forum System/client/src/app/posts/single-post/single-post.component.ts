import { Component, OnInit, Input } from '@angular/core';
import { PostDTO } from '../models/post.dto';
import { CONFIG } from '../../config/config';
import { UsersService } from '../../users/services/users.service';
import { UserDTO } from '../../users/models/user.dto';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.css']
})
export class SinglePostComponent implements OnInit {
  public userAvatarSrcPrefix: string = CONFIG.USER_AVATAR_SRC_PREFIX;
  public defaultUserAvatarImagePath: string = CONFIG.DEFAULT_USER_AVATAR_IMAGE_PATH;

  public postAuthor: UserDTO;
  @Input() public singlePost: PostDTO;

  constructor(private readonly usersService: UsersService) { }

  ngOnInit(): void {
    this.usersService.getUserById(this.singlePost.authorId).subscribe((foundUser: UserDTO) => this.postAuthor = foundUser);
  }
}
