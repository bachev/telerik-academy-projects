import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { PostDTO } from '../models/post.dto';
import { PostsService } from '../services/posts.service';
import { CommentsService } from 'src/app/comments/comments.service';
import { UserDTO } from '../../users/models/user.dto';
import { CONFIG } from '../../config/config';
import { UsersService } from '../../users/services/users.service';
import { AuthService } from '../../core/services/auth.service';
import { NotificationService } from '../../core/services/notification.service';
import { UpdatePostDTO } from '../models/update-post.dto';
import { LockPostDTO } from '../models/lock-post.dto';

@Component({
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css'],
})
export class PostDetailsComponent implements OnInit, OnDestroy {
  public userAvatarSrcPrefix: string = CONFIG.USER_AVATAR_SRC_PREFIX;
  public defaultUserAvatarImagePath: string =
    CONFIG.DEFAULT_USER_AVATAR_IMAGE_PATH;

  private loggedUserSubscription: Subscription;
  public loggedUserData: UserDTO;

  public author: UserDTO;
  public detailedPost: PostDTO;

  public registerForm: FormGroup;
  public postInEditMode = false;
  public postLocked = false;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly postsService: PostsService,
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
    private readonly notificator: NotificationService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = (): boolean => false;
  }

  ngOnInit(): void {
    const postId: number = +this.activatedRoute.snapshot.paramMap.get('postId');

    this.postsService.getPostById(postId).subscribe((foundPost) => {
      this.detailedPost = foundPost;
      this.registerForm = this.formBuilder.group({
        title: [
          this.detailedPost.title,
          [Validators.minLength(2), Validators.maxLength(50)],
        ],
        content: [this.detailedPost.content, [Validators.minLength(10)]],
      });
      this.usersService
        .getUserById(this.detailedPost.authorId)
        .subscribe((foundUser: UserDTO) => (this.author = foundUser));
    });

    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => (this.loggedUserData = data)
    );
  }

  ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  public showEditForm(): void {
    this.postInEditMode = !this.postInEditMode;
  }

  public updatePost(title?: string, content?: string): void {
    const editedPost = new UpdatePostDTO();

    if (title) {
      editedPost.title = title;
    }
    if (content) {
      editedPost.content = content;
    }
    this.postsService.updatePost(editedPost, this.detailedPost.id).subscribe(
      () => {
        this.notificator.success('Post updated!');
        this.router.navigate(['/posts', this.detailedPost.id]);
      },
      () => this.notificator.error('Unable to update post!')
    );
  }

  public deletePost(): void {
    this.postsService.deletePost(this.detailedPost.id).subscribe(() => {
      this.notificator.success('Post deleted!');
      this.router.navigate(['home']);
    });
  }

  public flagPost(): void {
    this.postsService.flagPost(this.detailedPost.id).subscribe(() => {
      this.notificator.warning('Post flagged!');
    });
  }

  public likePost(): void {
    this.postsService.likePost(this.detailedPost.id).subscribe(() => {
      this.notificator.success('Post liked!');
    });
  }

  public unlikePost(): void {
    this.postsService.unlikePost(this.detailedPost.id).subscribe(() => {
      this.notificator.warning('Post disliked!');
    });
  }

  public togglePostLock(postLocked: boolean): void {
    const postLock: LockPostDTO = { isLocked: postLocked };
    this.postsService
      .togglePostLock(this.detailedPost.id, postLock)
      .subscribe(() =>
        postLocked
          ? this.notificator.warning('Post locked!')
          : this.notificator.success('Post unlocked!')
      );
  }
}
