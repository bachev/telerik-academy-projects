import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
  name: 'SortByDate',
})
export class SortByDatePipe implements PipeTransform {
  transform(values: { addedOn: string }[], args?: string): any {
    if (args === 'DESC')
      values = values
        .sort(
          (a, b) =>
            new Date(a.addedOn).getTime() - new Date(b.addedOn).getTime()
        )
        .reverse();
    else
      values = values.sort(
        (a, b) => new Date(a.addedOn).getTime() - new Date(b.addedOn).getTime()
      );

    return values;
  }
}
