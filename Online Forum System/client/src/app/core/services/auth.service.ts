import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

import { CreateUserDTO } from '../../users/models/create-user.dto';
import { UserDTO } from '../../users/models/user.dto';
import { CONFIG } from '../../config/config';
import { UserCredentialsDTO } from '../../users/models/user-credentials.dto';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedUserDataSubject$ = new BehaviorSubject<UserDTO>(this.getLoggedUserData());

  constructor(
    private readonly httpClient: HttpClient,
    private readonly storage: StorageService,
    private readonly jwtService: JwtHelperService
  ) { }

  public get loggedUserData$(): Observable<UserDTO> {
    return this.loggedUserDataSubject$.asObservable();
  }

  public register(newUser: CreateUserDTO): Observable<UserDTO> {
    return this.httpClient.post<UserDTO>(`${CONFIG.DOMAIN_NAME}/users`, newUser);
  }

  public login(credentials: UserCredentialsDTO): Observable<any> {
    return this.httpClient.post<UserCredentialsDTO>(`${CONFIG.DOMAIN_NAME}/session`, credentials).pipe(
      tap(({ token }) => {
        this.storage.setItem('token', token);
        const user: UserDTO = this.jwtService.decodeToken(token);
        this.emitUserData(user);
      })
    );
  }

  public logout(): Observable<any> {
    return this.httpClient.delete(`${CONFIG.DOMAIN_NAME}/session`).pipe(
      tap(() => {
        this.storage.removeItem('token');
        this.emitUserData(null);
      })
    );
  }

  public getLoggedUserData(): UserDTO {
    const token = this.storage.getItem('token');

    if (!token) {
      return null;
    } else if (token && this.jwtService.isTokenExpired(token)) {
      this.storage.removeItem('token');
      return null;
    }

    return this.jwtService.decodeToken(token);
  }

  public emitUserData(user: UserDTO): void {
    this.loggedUserDataSubject$.next(user);
  }
}
