import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { NotificationService } from '../services/notification.service';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  public constructor(
    private readonly router: Router,
    private readonly notificationService: NotificationService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: any) => {
        if (error.status === 404) {
          this.router.navigate(['not-found']);
          this.notificationService.error('The resource you are looking for is not found!');
        } else if (error.status >= 500) {
          this.router.navigate(['server-error']);
          this.notificationService.error('Sorry... there seems to be a problem with our server!');
        }

        return throwError(error);
      })
    );
  }
}
