import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { NotificationService } from '../services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  public constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificator: NotificationService
  ) { }

  canActivate(): boolean {
    if (!this.authService.getLoggedUserData()) {
      this.notificator.error('You must be logged in in order to see this page!');
      this.router.navigate(['users/login']);

      return false;
    }

    return true;
  }
}
