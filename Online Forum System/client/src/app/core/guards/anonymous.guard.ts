import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AnonymousGuard implements CanActivate {

  public constructor(
    private readonly authService: AuthService,
    private readonly router: Router

  ) { }

  canActivate(): boolean {
    if (this.authService.getLoggedUserData()) {
      this.router.navigate(['home']);

      return false;
    }

    return true;
  }
}
