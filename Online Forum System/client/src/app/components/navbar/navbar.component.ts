import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { AuthService } from '../../core/services/auth.service';
import { NotificationService } from '../../core/services/notification.service';
import { UserDTO } from '../../users/models/user.dto';
import { CONFIG } from '../../config/config';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {
  public userAvatarSrcPrefix: string = CONFIG.USER_AVATAR_SRC_PREFIX;
  public defaultUserAvatarImagePath: string = CONFIG.DEFAULT_USER_AVATAR_IMAGE_PATH;

  private loggedUserSubscription: Subscription;
  public loggedUserData: UserDTO;

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificationService,
    private readonly router: Router
  ) { }

  ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => this.loggedUserData = data
    );
  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  public logoutUser(): void {
    this.authService.logout().subscribe(
      () => {
        this.notificator.success('Logout successful!');
        this.router.navigate(['users/login']);
      },
      () => {
        this.notificator.error('You are not logged in!');
        this.router.navigate(['users/login']);
      }
    );
  }
}
