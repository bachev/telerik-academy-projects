import { Component, OnInit } from '@angular/core';
import { UserDTO } from 'src/app/users/models/user.dto';
import { AuthService } from 'src/app/core/services/auth.service';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public loggedUserData: UserDTO;
  constructor(private readonly authService: AuthService) {}

  ngOnInit(): void {
    this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => (this.loggedUserData = data)
    );
  }
}
