import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SortByDatePipe } from '../core/pipes/sort.pipe';

@NgModule({
  declarations: [SortByDatePipe],
  imports: [CommonModule],
  exports: [CommonModule, FormsModule, ReactiveFormsModule, SortByDatePipe]
})
export class SharedModule {}
