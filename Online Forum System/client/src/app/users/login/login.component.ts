import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AuthService } from '../../core/services/auth.service';
import { UserCredentialsDTO } from '../models/user-credentials.dto';
import { NotificationService } from '../../core/services/notification.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificationService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = (): boolean => false;
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      user: ['', [Validators.required, Validators.minLength(2)]],
      password: [
        '',
        [
          Validators.required,
          Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/),
        ],
      ],
    });
  }

  public loginUser(user: string, password: string): void {
    let credentials: UserCredentialsDTO;

    if (user.includes('@')) {
      credentials = { email: user, password };
    } else {
      credentials = { username: user, password };
    }

    this.authService.login(credentials).subscribe(
      () => {
        this.notificator.success('Login successful!');
        window.location.reload();
      },
      () => this.notificator.error('Login unsuccessful!')
    );
  }
}
