export class UserCredentialsDTO {
    public username?: string;
    public email?: string;
    public password: string;
}
