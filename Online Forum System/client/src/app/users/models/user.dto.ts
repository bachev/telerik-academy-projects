export class UserDTO {
    public username: string;
    public id: number;
    public email: string;
    public role: string;
    public joinedOn: string;
    public avatar: string;
    public isBanned: boolean;
}
