export class BanUserDTO {
    public isBanned: boolean;
    public description: string;
    public expiresOn: Date;
}
