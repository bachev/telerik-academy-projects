import { UserRole } from '../enums/user-role.enum';

export class UpdateUserRoleDTO {
    public role: UserRole;
}
