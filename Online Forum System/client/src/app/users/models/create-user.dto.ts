export class CreateUserDTO {
    public email: string;
    public username: string;
    public password: string;
}
