import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserDTO } from '../models/user.dto';
import { AuthService } from '../../core/services/auth.service';
import { FriendsService } from '../services/friends.service';
import { NotificationService } from '../../core/services/notification.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css'],
})
export class NotificationsComponent implements OnInit {
  private subscriptions: Subscription[] = [];
  public loggedUserData: UserDTO;
  public loggedUserFriendRequests;
  public receivedRequests;

  constructor(
    private readonly authService: AuthService,
    private readonly friendsService: FriendsService,
    private readonly notificator: NotificationService
  ) {}

  ngOnInit(): void {
    this.subscriptions.push(
      this.authService.loggedUserData$.subscribe((data: UserDTO) => {
        this.loggedUserData = data;
        this.getLoggedUserFriendRequests(this.loggedUserData.id);
      })
    );
  }

  public getLoggedUserFriendRequests(userId): void {
    this.subscriptions.push(
      this.friendsService.getUserFriendRequests(userId).subscribe((data) => {
        this.loggedUserFriendRequests = data;
        this.receivedRequests = this.loggedUserFriendRequests.received;
      })
    );
  }

  public removeFriend(userId): void {
    this.subscriptions.push(
      this.friendsService.deleteFriend(userId).subscribe(
        () => {
          this.notificator.success('Frend removed!');
          // this.isFriend = true;
          this.ngOnInit();
        },
        () => this.notificator.error('Unable to remove friend!')
      )
    );
  }

  public acceptFriend(userId): void {
    this.subscriptions.push(
      this.friendsService.acceptFriend(userId).subscribe(
        () => {
          this.notificator.success('Frend accepted!');
          // this.friendRequested = true;
          this.ngOnInit();
        },
        () => this.notificator.error('Unable to accept friend!')
      )
    );
  }
}
