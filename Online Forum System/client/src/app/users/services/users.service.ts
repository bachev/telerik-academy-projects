import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { CONFIG } from '../../config/config';
import { UserDTO } from '../models/user.dto';
import { BanUserDTO } from '../models/ban-user.dto';
import { UpdateUserRoleDTO } from '../models/update-user-role.dto';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private readonly httpClient: HttpClient) {}

  public getUserById(userId: number): Observable<UserDTO> {
    return this.httpClient.get<UserDTO>(
      `${CONFIG.DOMAIN_NAME}/users/${userId}`
    );
  }

  public getUserActivity(userId: number): Observable<UserDTO> {
    return this.httpClient.get<any>(
      `${CONFIG.DOMAIN_NAME}/users/${userId}/activity`
    );
  }

  public uploadUserAvatar(userId: number, file: File): Observable<any> {
    const formData = new FormData();
    formData.append('file', file);

    return this.httpClient.post(
      `${CONFIG.DOMAIN_NAME}/users/${userId}/avatar`,
      formData
    );
  }

  public updateUserBanstatus(userId: number, ban: BanUserDTO): Observable<UserDTO> {
    return this.httpClient.put<UserDTO>(`${CONFIG.DOMAIN_NAME}/admin/users/${userId}/banstatus`, ban);
  }

  public deleteUser(userId: number): Observable<void> {
    return this.httpClient.delete<void>(`${CONFIG.DOMAIN_NAME}/admin/users/${userId}`);
  }

  public updateUserRole(userId: number, role: UpdateUserRoleDTO): Observable<UserDTO> {
    return this.httpClient.put<UserDTO>(`${CONFIG.DOMAIN_NAME}/admin/users/${userId}`, role);
  }
}
