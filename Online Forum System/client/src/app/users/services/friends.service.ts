import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from 'src/app/config/config';

@Injectable({
  providedIn: 'root',
})
export class FriendsService {
  constructor(private readonly httpClient: HttpClient) {}

  public getUserFriends(userId: number): Observable<any> {
    return this.httpClient.get<any>(
      `${CONFIG.DOMAIN_NAME}/users/${userId}/friends`
    );
  }

  public getUserFriendRequests(userId: number): Observable<any> {
    return this.httpClient.get<any>(
      `${CONFIG.DOMAIN_NAME}/users/${userId}/friends/requests`
    );
  }

  public addFriend(friendId: number): Observable<any> {
    return this.httpClient.post(
      `${CONFIG.DOMAIN_NAME}/users/${friendId}/friends?userId=${friendId}`,
      undefined
    );
  }

  public acceptFriend(friendId: number): Observable<any> {
    return this.httpClient.put(
      `${CONFIG.DOMAIN_NAME}/users/${friendId}/friends`,
      undefined
    );
  }

  public deleteFriend(friendId: number): Observable<void> {
    return this.httpClient.delete<void>(
      `${CONFIG.DOMAIN_NAME}/users/${friendId}/friends?userId=${friendId}`
    );
  }
}
