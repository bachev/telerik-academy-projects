import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Subscription } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { UserDTO } from '../models/user.dto';
import { AuthService } from '../../core/services/auth.service';
import { CONFIG } from '../../config/config';
import { UsersService } from '../services/users.service';
import { NotificationService } from '../../core/services/notification.service';
import { StorageService } from '../../core/services/storage.service';
import { FriendsService } from '../services/friends.service';
import { BanUserDTO } from '../models/ban-user.dto';
import { UserRole } from '../enums/user-role.enum';
import { UpdateUserRoleDTO } from '../models/update-user-role.dto';

@Component({
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit, OnDestroy {
  public userAvatarSrcPrefix: string = CONFIG.USER_AVATAR_SRC_PREFIX;
  public defaultUserAvatarImagePath: string =
    CONFIG.DEFAULT_USER_AVATAR_IMAGE_PATH;

  private subscriptions: Subscription[] = [];
  public userProfileViewed: UserDTO;
  public loggedUserData: UserDTO;
  public userFriends = [];
  public loggedUserFriendRequests;
  public userActivity;
  public friendRequested: boolean;
  public banFormOpened = false;
  public banForm: FormGroup;

  public friendRequestReceivedFromUser: boolean;
  public isFriend: boolean;
  public viewedUserSendRequests;

  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
    private readonly friendsService: FriendsService,
    private readonly notificator: NotificationService,
    private readonly storage: StorageService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly jwtService: JwtHelperService,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = (): boolean => false;
  }

  ngOnInit(): void {
    this.banForm = this.formBuilder.group({
      description: [
        '',
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(100),
        ],
      ],
      expiresOn: ['', [Validators.required]],
    });

    this.subscriptions.push(
      this.authService.loggedUserData$.subscribe(
        (data: UserDTO) => (this.loggedUserData = data)
      )
    );

    const userId: number = +this.activatedRoute.snapshot.paramMap.get('userId')
      ? +this.activatedRoute.snapshot.paramMap.get('userId')
      : +this.loggedUserData.id;

    if (this.activatedRoute.snapshot.paramMap.get('userId')) {
      this.subscriptions.push(
        this.usersService
          .getUserById(userId)
          .subscribe((data) => (this.userProfileViewed = data))
      );
      this.getFriendRequests(userId);
    } else {
      this.userProfileViewed = this.loggedUserData;
    }
    this.subscriptions.push(
      this.usersService.getUserActivity(userId).subscribe((data) => {
        this.userActivity = data;
      })
    );

    this.subscriptions.push(
      this.friendsService.getUserFriends(userId).subscribe((data) => {
        this.userFriends = data;
        this.isFriendFn();
      })
    );
    this.getLoggedUserFriendRequests(this.loggedUserData.id);
  }

  public getFriendRequests(userId): void {
    this.subscriptions.push(
      this.friendsService.getUserFriendRequests(userId).subscribe((data) => {
        this.viewedUserSendRequests = data;
        this.friendRequestReceivedFn();
      })
    );
  }

  public getLoggedUserFriendRequests(userId): void {
    this.subscriptions.push(
      this.friendsService.getUserFriendRequests(userId).subscribe((data) => {
        this.loggedUserFriendRequests = data;
        this.friendRequestedFn();
      })
    );
  }

  public sameUser(): boolean {
    if (this.loggedUserData.id !== this.userProfileViewed.id) {
      return false;
    } else {
      return true;
    }
  }

  public addFriend(): void {
    if (!this.sameUser()) {
      this.subscriptions.push(
        this.friendsService.addFriend(this.userProfileViewed.id).subscribe(
          () => {
            this.notificator.success('Frend request sent!');
            this.friendRequested = true;
            this.ngOnInit();
          },
          () => this.notificator.error('Unable to add friend!')
        )
      );
    }
  }
  public removeFriend(): void {
    if (!this.sameUser()) {
      this.subscriptions.push(
        this.friendsService.deleteFriend(this.userProfileViewed.id).subscribe(
          () => {
            this.notificator.success('Frend removed!');
            this.isFriend = true;
            this.ngOnInit();
          },
          () => this.notificator.error('Unable to remove friend!')
        )
      );
    }
  }

  public acceptFriend(): void {
    if (!this.sameUser()) {
      this.subscriptions.push(
        this.friendsService.acceptFriend(this.userProfileViewed.id).subscribe(
          () => {
            this.notificator.success('Frend accepted!');
            this.friendRequested = true;
            this.ngOnInit();
          },
          () => this.notificator.error('Unable to accept friend!')
        )
      );
    }
  }

  public isFriendFn(): void {
    const friends = this.userFriends.map((user) => user.id);
    this.isFriend = friends.includes(this.loggedUserData.id);
  }

  public friendRequestedFn(): void {
    const sentRequestIds: number[] = this.loggedUserFriendRequests.sent.map(
      (friendReq) => friendReq.id
    );
    this.friendRequested = sentRequestIds.includes(this.userProfileViewed.id);
  }

  public friendRequestReceivedFn(): void {
    const sentRequestIds: number[] = this.viewedUserSendRequests.received.map(
      (friendReq) => friendReq.id
    );
    this.friendRequestReceivedFromUser = sentRequestIds.includes(
      this.loggedUserData.id
    );
  }

  public onFileSelected(file: File): void {
    this.subscriptions.push(
      this.usersService
        .uploadUserAvatar(this.loggedUserData.id, file)
        .subscribe(
          ({ token }) => {
            this.storage.setItem('token', token);
            const updatedUser: UserDTO = this.jwtService.decodeToken(token);
            this.authService.emitUserData(updatedUser);
          },
          () => this.notificator.error('Only images are allowed!')
        )
    );
  }
  public ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => {
      sub.unsubscribe();
    });
  }

  public banUser(description: string, expiresOn: Date): void {
    const ban: BanUserDTO = { isBanned: true, description, expiresOn };

    this.usersService
      .updateUserBanstatus(this.userProfileViewed.id, ban)
      .subscribe(
        () => this.notificator.warning('User banned!'),
        () => this.notificator.error('Invalid input!')
      );
  }

  public unbanUser(): void {
    const ban: BanUserDTO = {
      isBanned: false,
      description: 'ban removed',
      expiresOn: null,
    };

    this.usersService
      .updateUserBanstatus(this.userProfileViewed.id, ban)
      .subscribe(() => this.notificator.success('Ban removed!'));
  }

  public openBanForm(): void {
    this.banFormOpened = !this.banFormOpened;
  }

  public deleteUser(): void {
    this.usersService.deleteUser(this.userProfileViewed.id).subscribe(() => {
      this.notificator.success('User deleted!');
      this.router.navigate(['home']);
    });
  }

  public makeUserAdmin(): void {
    const newRole: UpdateUserRoleDTO = { role: UserRole.Admin };
    this.usersService.updateUserRole(this.userProfileViewed.id, newRole).subscribe(
      () => this.notificator.success('User promoted to Admn!')
    );
  }
  public makeUserBasic(): void {
    const newRole: UpdateUserRoleDTO = { role: UserRole.Basic };
    this.usersService.updateUserRole(this.userProfileViewed.id, newRole).subscribe(
      () => this.notificator.success('User role set to Basic!')
     );
  }
}
