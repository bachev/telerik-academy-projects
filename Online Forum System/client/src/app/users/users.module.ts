import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { UsersRoutingModule } from './users-routing.module';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { UsersService } from './services/users.service';
import { CommentsModule } from '../comments/comments.module';
import { PostsModule } from '../posts/posts.module';
import { FriendsService } from './services/friends.service';
import { NotificationsComponent } from './notifications/notifications.component';

@NgModule({
  declarations: [
    RegisterComponent,
    LoginComponent,
    ProfileComponent,
    NotificationsComponent,
  ],
  imports: [SharedModule, UsersRoutingModule, CommentsModule, PostsModule],
  providers: [UsersService, FriendsService],
  exports: [NotificationsComponent],
})
export class UsersModule {}
