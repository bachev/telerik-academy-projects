export const CONFIG = {
  DOMAIN_NAME: 'http://localhost:3000',
  USER_AVATAR_SRC_PREFIX: 'http://localhost:3000/avatars/',
  DEFAULT_USER_AVATAR_IMAGE_PATH: '/assets/default-user.png'
};
