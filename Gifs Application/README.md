﻿## How to setup and run the project:

**1. Create a local copy of the project on your computer**

Open *Git Bash* shell in a folder of your choice and run the following command:
```git clone https://gitlab.com/bachev/telerik-academy-projects.git```

**2.  Install all dependencies**

Open the project with *Visual Studio Code* and run ```npm install``` in the root folder (where *package.json* and the rest of the config files are)

**3. Run the application**
- Add the *Live Server* extension to *Visual Studio Code*
- Open *index.html*, hit the right mouse button and select *Open with Live Server*

**4. Have fun!**
