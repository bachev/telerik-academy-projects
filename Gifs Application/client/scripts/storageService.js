const storeUploaded = (id) => {
  // eslint-disable-next-line max-len
  const storedIds = localStorage.getItem('id') ? JSON.parse(localStorage.getItem('id')) : [];

  storedIds.push(id);

  localStorage.setItem('id', JSON.stringify(storedIds));
};

const storeFavorites = (id) => {
  localStorage.setItem('favourite', id);
};

export {
  storeUploaded,
  storeFavorites,
};
