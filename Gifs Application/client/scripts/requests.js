/* eslint-disable camelcase */
/* eslint-disable max-len */
import { bestUrl, key, limit, rating } from './constants.js';

const requestTrending = () => fetch(`${bestUrl}/trending?api_key=${key}&limit=${limit}&rating=${rating}`, {
  method: 'GET',
});

const requestUploaded = (ids) => fetch(`${bestUrl}?ids=${ids}&api_key=${key}`, {
  method: 'GET',
});

const requestFav = (id) => fetch(`${bestUrl}?ids=${id}&api_key=${key}`, {
  method: 'GET',
});

const requestSearch = (value) => fetch(`${bestUrl}/search?q=${value}&api_key=${key}&limit=${limit}`, {
  method: 'GET',
});

const requestDetails = (gif_id) => fetch(`${bestUrl}/${gif_id}?api_key=${key}`, {
  method: 'GET',
});

const uploadGiphy = (file) => fetch(`http://upload.giphy.com/v1/gifs?api_key=${key}`, {
  method: 'POST',
  body: file,
});

const requestRandom = () => fetch(`${bestUrl}/random?api_key=${key}`, {
  method: 'GET',
});

export {
  requestTrending,
  requestSearch,
  requestDetails,
  uploadGiphy,
  requestUploaded,
  requestFav,
  requestRandom,
};
