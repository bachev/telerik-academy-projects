/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable max-len */
import { requestTrending, requestSearch, uploadGiphy, requestUploaded, requestFav, requestRandom } from './requests.js';
import { createImgTag, createFavTag } from './views.js';
import { storeFavorites, storeUploaded } from './storageService.js';

export const populateGifs = async () => {
  try {
    $('.main').empty();
    $('.heading span').text('Trending');
    const response = await requestTrending();
    const result = await response.json();
    result.data.forEach((gif) => createImgTag(gif));
  } catch (e) {
    // eslint-disable-next-line no-undef
    console.log(e.message);
  }
};

export const populateFavouriteGif = async () => {
  if (localStorage.favourite) {
    const id = localStorage.getItem('favourite');
    try {
      const response = await requestFav(id);
      const result = await response.json();
      $('.main').empty();
      $('.main').append(createFavTag(result.data[0]));
      $('.heading span').text('Favourite GIF');
    } catch (e) {
      console.log(e.message);
    }
  } else {
    toastr.error('Favourite GIF not selected! A random GIF will be shown.');
    try {
      const response = await requestRandom();
      const result = await response.json();
      $('.main').empty();
      $('.main').append(createFavTag(result.data));
      $('.heading span').text('A random GIF');
    } catch (e) {
      console.log(e.message);
    }
  }
};

export const gifsUploaded = async () => {
  if (!localStorage.id) {
    // eslint-disable-next-line no-undef
    toastr.error('No GIFs uploaded yet!');
    return;
  }
  const ids = (JSON.parse(localStorage.getItem('id'))).join();
  try {
    const response = await requestUploaded(ids);
    const result = await response.json();
    $('.main').empty();
    $('.heading span').text('Uploaded GIFs');
    result.data.forEach((gif) => $('.main').append(createImgTag(gif)));
  } catch (e) {
    console.log(e.message);
  }
};

export const gifsSearch = async () => {
  const $searchValue = $('input.search');
  if ($searchValue.val() === '') {
    toastr.error('Please enter a search string!');
    return;
  }
  try {
    const response = await requestSearch($searchValue.val());
    const result = await response.json();
    $('.main').empty();
    $('.heading span').text('GIFs found');
    result.data.forEach((gif) => $('.main').append(createImgTag(gif)));
  } catch (e) {
    console.log(e.message);
  }
  $searchValue.val('');
};

export const updateGif = async (evt) => {
  evt.preventDefault();
  const files = $('#myFile').prop('files')[0];
  const formData = new FormData();
  formData.append('file', files);
  try {
    const response = await uploadGiphy(formData);
    const result = await response.json();
    const ids = result.data.id;
    storeUploaded(ids);
    $('#myFile').val('');
    toastr.success('Upload successiful');
  } catch (error) {
    console.log(error.message);
  }
};

export const favouriteAdd = async (evt) => {
  const id = $(evt.target).attr('favourite-gif-id');
  storeFavorites(id);
  toastr.success('Favourite GIF added!');
};
