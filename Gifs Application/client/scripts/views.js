/* eslint-disable no-alert */
/* eslint-disable max-len */
import { requestDetails } from './requests.js';

const createImgTag = (gif) => {
  return $('.main')
      .removeClass('display-flex')
      .append(
          $('<div>')
              .addClass('card card-preview')
              .append(
                  $('<img>')
                      .addClass('gifs')
                      .attr('src', `${gif.images.fixed_width_downsampled.url}`)
                      .attr('details-shown', false))
              .append(
                  $('<div>')
                      .addClass('bar-bottom')
                      .append(
                          $('<span>')
                              .addClass('favourite-button')
                              .attr('favourite-gif-id', gif.id)
                              .text('Make Favourite')
                      )
                      .append(
                          $('<span>')
                              .addClass('show-details')
                              .attr('details-id', gif.id)
                              .attr('details-shown', false)
                              .text('Show Details')
                      )
              ));
};

const createFavTag = (gif) => {
  return $('.main')
      .addClass('display-flex')
      .append(
          $('<div>')
              .addClass('card card-full-view')
              .append(
                  $('<img>')
                      .addClass('gifs')
                      .attr('src', `${gif.images.original.url}`)
                      .attr('details-shown', false))
              .append(
                  $('<div>')
                      .addClass('bar-bottom')
                      .append(
                          $('<span>')
                              .addClass('show-details')
                              .attr('details-id', gif.id)
                              .attr('details-shown', false)
                              .text('Show Details')
                      )
              ));
};

const createUploadedTag = (gif) => {
  return $('.main')
      .addClass('display-flex')
      .append(
          $('<div>')
              .addClass('card card-fav-view')
              .append(
                  $('<img>')
                      .addClass('gifs')
                      .attr('src', `${gif.images.original.url}`)
                      .attr('details-shown', false))
              .append(
                  $('<div>')
                      .addClass('bar-bottom')
                      .append(
                          $('<span>')
                              .addClass('favourite-button')
                              .attr('favourite-gif-id', gif.id)
                              .text('Make Favourite')
                      )
                      .append(
                          $('<span>')
                              .addClass('show-details')
                              .attr('details-id', gif.id)
                              .attr('details-shown', false)
                              .text('Show Details')
                      )
              ));
};

const gifDetails = async (event) => {
  const gifId = $(event.target).attr('details-id');
  const detailsShown = $(event.target).attr('details-shown');

  try {
    const response = await requestDetails(gifId);
    const result = await response.json();
    if (detailsShown === 'false') {
      $(event.target)
          .attr('details-shown', true)
          .text('Hide Details')
          .after(
              $('<div>')
                  .addClass('details-info-wrapper')
                  .append(
                      $('<p>')
                          .text(`Name: ${result.data.title || 'No Name'}`)
                          .addClass('details-info')
                  )
                  .append(
                      $('<p>')
                          .text(`Author: ${result.data.username || 'Unknown'}`)
                          .addClass('details-info')
                  )
                  .append(
                      $('<p>')
                          .text(`Added: ${result.data.import_datetime}`)
                          .addClass('details-info')
                  )
          );
    } else {
      $(event.target).next('div').remove();
      $(event.target)
          .attr('details-shown', false)
          .text('Show Details');
    }
  } catch (e) {
    alert(e.message);
  }
};

export {
  createImgTag,
  gifDetails,
  createFavTag,
  createUploadedTag,
};
