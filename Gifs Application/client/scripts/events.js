/* eslint-disable max-len */
// eslint-disable-next-line max-len
const searchGifs = (callback) => $(document).on('keyup', '#input-search', function() {
  if ($('#input-search').is(':focus') && event.key === 'Enter') {
    callback();
  }
});

// eslint-disable-next-line max-len
const detailsGif = (callback) => $(document).on('click', '[details-id]', callback);

const upGifi = (callback) => $(document).on('submit', '#upFile', callback);

// eslint-disable-next-line max-len
const uploadedGifs = (callback) => $(document).on('click', '#uploaded', callback);

// eslint-disable-next-line max-len
const trendingGifs = (callback) => $(document).on('click', '#trending', callback);

const favourite = (callback) => $(document).on('click', '[favourite-gif-id]', callback);

const favouriteShow = (callback) => $(document).on('click', '#your-fav-gif', callback);

const uploadButtonClick = () => $(document).on('click', '#myUploadButton', () => {
  $('#myFile').click();
});

const submitForm = () => $(document).on('change', '#myFile', () => {
  $('#myFile').submit();
});

export {
  searchGifs,
  detailsGif,
  upGifi,
  uploadedGifs,
  trendingGifs,
  uploadButtonClick,
  submitForm,
  favourite,
  favouriteShow,
};
