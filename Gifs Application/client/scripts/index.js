/* eslint-disable max-len */

import { populateGifs, gifsSearch, updateGif, gifsUploaded, favouriteAdd, populateFavouriteGif } from './callbacks.js';
import { searchGifs, detailsGif, upGifi, uploadedGifs, trendingGifs, uploadButtonClick, submitForm, favourite, favouriteShow } from './events.js';
import { gifDetails } from './views.js';

$(async () => {
  try {
    await populateGifs();

    searchGifs(gifsSearch);

    detailsGif(gifDetails);

    upGifi(updateGif);

    trendingGifs(populateGifs);

    uploadedGifs(gifsUploaded);

    uploadButtonClick();

    submitForm();

    favourite(favouriteAdd);

    favouriteShow(populateFavouriteGif);
  } catch (error) {
    console.log(error.message);
  }
});


